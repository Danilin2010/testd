<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 08.09.2019
 * Time: 21:47
 */

define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);
define("EXTRANET_NO_REDIRECT", true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Application;

Bitrix\Main\Loader::includeModule("iblock");
$request = Application::getInstance()->getContext()->getRequest();

if($request->isPost()){

    $questioning = (int)$request->getPost("questioning");
    $relevance = (int)$request->getPost("relevance");
    $quality = (int)$request->getPost("quality");
    $complexity = (int)$request->getPost("complexity");
    $detail_text = $request->getPost("detail_text");
    $questioning_name = $request->getPost("questioning_name");
    if($questioning>0){
        $el = new CIBlockElement;
        $arLoadProductArray = Array(
            "IBLOCK_ID"      => IBLOCK_FEEDBACK,
            'DATE_ACTIVE_FROM'=>date('d.m.Y'),
            "PROPERTY_VALUES"=> [
                'RELEVANCE'=>$relevance,
                'QUALITY'=>$quality,
                'COMPLEXITY'=>$complexity,
                'QUESTIONING'=>$questioning,
            ],
            "NAME"           => $questioning_name,
            "ACTIVE"         => "Y",
            "DETAIL_TEXT"    => $detail_text,
        );
        $el->Add($arLoadProductArray);
        /*if($PRODUCT_ID = $el->Add($arLoadProductArray))
            echo "New ID: ".$PRODUCT_ID;
        else
            echo "Error: ".$el->LAST_ERROR;*/
    }

}