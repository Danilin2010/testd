<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\JavaScript;

/**
 * @var string $sIdTab
 */

?>
<script>
    (function ($, api) {
        $(document).ready(function () {
            var root = $(<?= JavaScript::toObject('#'.$sIdTab) ?>);
            var elements = $('.widget-element .widget-element-question', root);
            var answers = $('.widget-element .widget-element-answer', root);
            var answersText = $('.widget-element-answer-text', answers);
            var icons = $('.widget-element-question-icon-wrapper', elements);

            elements.on('click', function () {
                var self = $(this);
                var answer = self.next();
                var answerText = $('.widget-element-answer-text', answer);
                var icon = self.find('.widget-element-question-icon-wrapper');

                if (answer.is(':hidden')) {
                    icons.removeClass('active');
                    icon.addClass('active');
                    answers.slideUp(500);
                    answersText.fadeOut(300);
                    answerText.fadeIn(800);
                    answer.slideDown(500);
                } else {
                    icon.removeClass('active');
                    answer.slideUp(500);
                    answerText.fadeOut(300);
                }
            });
        });
    })(jQuery, intec);
</script>
