<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Настройки отображения */
$bBySection = ArrayHelper::getValue($arParams, 'BY_SECTION') == 'Y';

$arResult['VISUAL'] = [
    'BY_SECTION' => $bBySection,
    'TEXT_ALIGN' => ArrayHelper::getValue($arParams, 'ELEMENT_TEXT_ALIGN'),
    'TABS_POSITION' => ArrayHelper::getValue($arParams, 'TABS_POSITION')
];

/** Настройки кнопки "Показать все" */
$sFooterText = ArrayHelper::getValue($arParams, 'FOOTER_TEXT');
$sFooterText = trim($sFooterText);
$sListPage = ArrayHelper::getValue($arParams, 'LIST_PAGE');
$sListPage = trim($sListPage);
$sListPage = StringHelper::replaceMacros($sListPage, $arMacros);
$bFooterShow = ArrayHelper::getValue($arParams, 'FOOTER_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sFooterText) && !empty($sListPage);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'FOOTER_POSITION'),
    'TEXT' => Html::encode($sFooterText),
    'LIST_PAGE' => $sListPage
];

foreach ($arResult['ITEMS'] as $iKey => $arItem) {
    /** Обработка текста */
    $sPreviewText = ArrayHelper::getValue($arItem, 'PREVIEW_TEXT');
    $sDetailText = ArrayHelper::getValue($arItem, 'DETAIL_TEXT');

    if (empty($sPreviewText) && !empty($sDetailText)) {
        $sPreviewText = $sDetailText;
    }

    $arItem['PREVIEW_TEXT'] = $sPreviewText;

    $arResult['ITEMS'][$iKey] = $arItem;
}

/** Получение информации о разделах при выводе с группировкой */
if ($bBySection) {
    $sSortBy = ArrayHelper::getValue($arParams, 'SORT_BY');
    $sOrderBy = ArrayHelper::getValue($arParams, 'ORDER_BY');

    $arSort = [
        $sSortBy => $sOrderBy
    ];
    $arSections = [];

    $rsSections = CIBlockSection::GetList(
        $arSort,
        [
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'ID' => $arResult['SECTIONS']
        ]
    );

    while ($arSection = $rsSections->Fetch()) {
        $arSections[$arSection['ID']] = $arSection;
    }
    unset($rsSections);

    $arResult['SECTIONS'] = $arSections;
    unset($arSections);

    /** Присвоение элементов соотвествующим разделам */
    foreach ($arResult['ITEMS'] as $iItemKey => $arItem) {
        foreach ($arResult['SECTIONS'] as $iSectionKey => $arSection) {
            if ($arItem['IBLOCK_SECTION_ID'] == $arSection['ID']) {
                $arResult['SECTIONS'][$arSection['ID']]['ITEMS'][$arItem['ID']] = $arItem;
            }
        }
    }
}