<?php

$MESS['FAQ_TEMP1_BY_SECTION'] = 'Группировать элементы по разделам';
$MESS['FAQ_TEMP1_TABS_POSITION'] = 'Расположение вкладок разделов';
$MESS['FAQ_TEMP1_ELEMENT_TEXT_ALIGN'] = 'Выравнивание текста в элементах';
$MESS['FAQ_TEMP1_FOOTER_SHOW'] = 'Показывать кнопку "Показать все"';
$MESS['FAQ_TEMP1_FOOTER_POSITION'] = 'Расположение кнопки "Показать все"';
$MESS['FAQ_TEMP1_FOOTER_TEXT'] = 'Текст кнопки "Показать все"';
$MESS['FAQ_TEMP1_LIST_PAGE'] = 'Ссылка на страницу раздела';

$MESS['FAQ_TEMP1_POSITION_LEFT'] = 'Слева';
$MESS['FAQ_TEMP1_POSITION_CENTER'] = 'По центру';
$MESS['FAQ_TEMP1_POSITION_RIGHT'] = 'Справа';
$MESS['FAQ_TEMP1_POSITION_TOP_RIGHT'] = 'Сверху справа';

$MESS['FAQ_TEMP1_FOOTER_TEXT_DEFAULT'] = 'Показать все';