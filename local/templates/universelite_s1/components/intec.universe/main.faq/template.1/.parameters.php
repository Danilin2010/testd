<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

if (!empty($arCurrentValues['IBLOCK_ID'])) {
    $arTemplateParameters['BY_SECTION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('FAQ_TEMP1_BY_SECTION'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N',
        'REFRESH' => 'Y'
    );

    if ($arCurrentValues['BY_SECTION'] == 'Y') {
        $arTemplateParameters['TABS_POSITION'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('FAQ_TEMP1_TABS_POSITION'),
            'TYPE' => 'LIST',
            'VALUES' => array(
                'left' => Loc::getMessage('FAQ_TEMP1_POSITION_LEFT'),
                'center' => Loc::getMessage('FAQ_TEMP1_POSITION_CENTER'),
                'right' => Loc::getMessage('FAQ_TEMP1_POSITION_RIGHT')
            ),
            'DEFAULT' => 'center'
        );
    }
}

$arTemplateParameters['ELEMENT_TEXT_ALIGN'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('FAQ_TEMP1_ELEMENT_TEXT_ALIGN'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        'left' => Loc::getMessage('FAQ_TEMP1_POSITION_LEFT'),
        'center' => Loc::getMessage('FAQ_TEMP1_POSITION_CENTER')
    ),
    'DEFAULT' => 'center'
);

$arTemplateParameters['FOOTER_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('FAQ_TEMP1_FOOTER_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['FOOTER_SHOW'] == 'Y') {
    $arTemplateParameters['FOOTER_POSITION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('FAQ_TEMP1_FOOTER_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'left' => Loc::getMessage('FAQ_TEMP1_POSITION_LEFT'),
            'center' => Loc::getMessage('FAQ_TEMP1_POSITION_CENTER'),
            'right' => Loc::getMessage('FAQ_TEMP1_POSITION_RIGHT')
        ),
        'DEFAULT' => 'center'
    );
    $arTemplateParameters['FOOTER_TEXT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('FAQ_TEMP1_FOOTER_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('FAQ_TEMP1_FOOTER_TEXT_DEFAULT')
    );
    $arTemplateParameters['LIST_PAGE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('FAQ_TEMP1_LIST_PAGE'),
        'TYPE' => 'STRING'
    );
}