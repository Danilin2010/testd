<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\UnsetArrayValue;

$arReturn = [];

$arReturn['ADDRESS_SHOW_MOBILE'] = new UnsetArrayValue();
$arReturn['PHONES_SHOW_MOBILE'] = new UnsetArrayValue();
$arReturn['EMAIL_SHOW_MOBILE'] = new UnsetArrayValue();
$arReturn['TAGLINE_SHOW_MOBILE'] = new UnsetArrayValue();

$arReturn['MOBILE_FILLED'] = [
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_HEADER_TEMP1_MOBILE_TEMP1_FILLED'),
    'TYPE' => 'CHECKBOX'
];

return $arReturn;