<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use intec\core\bitrix\component\InnerTemplate;
use intec\core\helpers\Html;

/**
 * @var array $arParams
 * @var array $arResult
 * @var array $arData
 * @var InnerTemplate $this
 */

$sTemplateId = $arData['id'];

?>
<?= Html::beginTag('div', [
    'class' => Html::cssClassFromArray([
        'widget-view-mobile-1' => [
            '' => true,
            'filled' => $arResult['MOBILE']['FILLED']
        ],
        'intec-cl-background' => $arResult['MOBILE']['FILLED'],
        'intec-content-wrap' => true
    ], true)
]) ?>
    <div class="widget-wrapper intec-content intec-content-visible">
        <div class="widget-wrapper-2 intec-content-wrapper">
            <div class="widget-wrapper-3 intec-grid intec-grid-nowrap intec-grid-i-h-10 intec-grid-a-v-center">
                <?php if ($arResult['MENU']['MAIN']['SHOW']['MOBILE']) { ?>
                    <div class="widget-menu-wrap intec-grid-item-auto">
                        <div class="widget-item widget-menu">
                            <?php include(__DIR__.'/../../../parts/menu/main.mobile.1.php') ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($arResult['LOGOTYPE']['SHOW']['MOBILE']) { ?>
                    <div class="widget-logotype-wrap intec-grid-item intec-grid-item-shrink-1">
                        <a href="<?= SITE_DIR ?>" class="widget-item widget-logotype intec-image">
                            <div class="intec-aligner"></div>
                            <?php include(__DIR__.'/../../../parts/logotype.php') ?>
                        </a>
                    </div>
                <?php } else { ?>
                    <div class="intec-grid-item intec-grid-item-shrink-1"></div>
                <?php } ?>
                <?php if ($arResult['SEARCH']['SHOW']['MOBILE']) { ?>
                    <div class="widget-search-wrap intec-grid-item-auto">
                        <a href="<?= $arResult['URL']['SEARCH'] ?>" class="widget-item widget-search intec-cl-text-hover">
                            <i class="glyph-icon-loop"></i>
                        </a>
                    </div>
                <?php } ?>
                <?php if ($arResult['BASKET']['SHOW']['MOBILE']) { ?>
                    <div class="widget-basket-wrap intec-grid-item-auto">
                        <div class="widget-item widget-basket">
                            <?php $arBasketParams = [
                                'DELAY_SHOW' => 'N',
                                'COMPARE_SHOW' => 'N'
                            ] ?>
                            <?php include(__DIR__.'/../../../parts/basket.php') ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($arResult['AUTHORIZATION']['SHOW']['MOBILE']) { ?>
                    <div class="widget-authorization-wrap intec-grid-item-auto">
                        <div class="widget-item widget-authorization">
                            <?php include(__DIR__.'/../../../parts/auth/icons.php') ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?= Html::endTag('div') ?>
