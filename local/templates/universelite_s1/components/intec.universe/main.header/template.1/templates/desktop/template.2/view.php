<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use intec\core\bitrix\component\InnerTemplate;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\JavaScript;

/**
 * @var array $arParams
 * @var array $arResult
 * @var array $arData
 * @var InnerTemplate $this
 */

$sTemplateId = $arData['id'];

$bContainerShow = false;

foreach ([
    'LOGOTYPE',
    'PHONES',
    'AUTHORIZATION',
    'ADDRESS',
    'EMAIL',
    'SEARCH',
    'BASKET',
    'DELAY',
    'COMPARE',
    'MENU'
] as $sBlock) {
    $bContainerShow = $bContainerShow || $arResult[$sBlock]['SHOW']['DESKTOP'];
}

$bBasketShow =
    $arResult['BASKET']['SHOW']['DESKTOP'] ||
    $arResult['DELAY']['SHOW']['DESKTOP'] ||
    $arResult['COMPARE']['SHOW']['DESKTOP'];

?>
<div class="widget-view-desktop-2">
    <?php if ($bContainerShow) { ?>
        <div class="widget-container">
            <div class="intec-content intec-content-visible">
                <div class="intec-content-wrapper">
                    <?= Html::beginTag('div', [
                        'class' => [
                            'widget-container-wrapper',
                            'intec-grid' => [
                                '',
                                'nowrap',
                                'a-h-start',
                                'a-v-center',
                                'i-h-10'
                            ]
                        ]
                    ]) ?>
                        <?php if ($arResult['MENU']['MAIN']['SHOW']['DESKTOP']) { ?>
                            <div class="widget-menu-wrap intec-grid-item-auto">
                                <div class="widget-item widget-menu">
                                    <?php include(__DIR__.'/../../../parts/menu/main.popup.1.php') ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($arResult['LOGOTYPE']['SHOW']['DESKTOP']) { ?>
                            <div class="widget-logotype-wrap intec-grid-item-auto">
                                <a href="<?= SITE_DIR ?>" class="widget-item widget-logotype intec-image">
                                    <div class="intec-aligner"></div>
                                    <?php include(__DIR__.'/../../../parts/logotype.php') ?>
                                </a>
                            </div>
                        <?php } ?>
                        <?php if ($arResult['ADDRESS']['SHOW']['DESKTOP']) { ?>
                            <div class="widget-address-wrap intec-grid-item-auto">
                                <div class="widget-item widget-address">
                                    <div class="widget-address-icon glyph-icon-location intec-cl-text"></div>
                                    <div class="widget-address-text">
                                        <?= $arResult['ADDRESS']['VALUE'] ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="intec-grid-item"></div>
                        <?php if ($arResult['PHONES']['SHOW']['DESKTOP'] || $arResult['EMAIL']['SHOW']['DESKTOP']) { ?>
                            <div class="widget-contacts-wrap intec-grid-item-auto">
                                <div class="widget-contacts widget-item">
                                    <?php if ($arResult['PHONES']['SHOW']['DESKTOP']) { ?>
                                        <?php
                                            $arPhones = $arResult['PHONES']['VALUES'];
                                            $arPhone = ArrayHelper::shift($arPhones);
                                        ?>
                                        <div class="widget-phone-wrap">
                                            <div class="widget-phone">
                                                <div class="widget-phone-content" data-block="phone">
                                                    <div class="widget-phone-content-text intec-cl-text-hover" data-block-action="popup.open">
                                                        <div class="widget-phone-content-text-icon glyph-icon-phone intec-cl-text"></div>
                                                        <a href="tel:<?= $arPhone['VALUE'] ?>" class="widget-phone-content-text-value intec-cl-text-hover">
                                                            <?= $arPhone['DISPLAY'] ?>
                                                        </a>
                                                    </div>
                                                    <?php if (!empty($arPhones)) { ?>
                                                        <div class="widget-phone-content-popup" data-block-element="popup">
                                                            <div class="widget-phone-content-popup-wrapper">
                                                                <?php foreach ($arPhones as $arPhone) { ?>
                                                                    <a href="tel:<?= $arPhone['VALUE'] ?>" class="widget-phone-content-popup-item intec-cl-text-hover">
                                                                        <?= $arPhone['DISPLAY'] ?>
                                                                    </a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php if (!defined('EDITOR')) { ?>
                                                <script type="text/javascript">
                                                    (function ($) {
                                                        $(document).on('ready', function () {
                                                            var root = $(<?= JavaScript::toObject('#'.$sTemplateId) ?>);
                                                            var block = $('[data-block="phone"]', root);
                                                            var popup = $('[data-block-element="popup"]', block);

                                                            popup.open = $('[data-block-action="popup.open"]', block);
                                                            popup.open.on('mouseenter', function () {
                                                                popup.css({
                                                                    'display': 'block'
                                                                }).stop().animate({
                                                                    'opacity': 1
                                                                }, 500);
                                                            });

                                                            block.on('mouseleave', function () {
                                                                popup.stop().animate({
                                                                    'opacity': 0
                                                                }, 500, function () {
                                                                    popup.css({
                                                                        'display': 'none'
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    })(jQuery)
                                                </script>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <?php if ($arResult['EMAIL']['SHOW']['DESKTOP']) { ?>
                                        <div class="widget-email-wrap">
                                            <div class="widget-email">
                                                <div class="widget-email-icon glyph-icon-mail intec-cl-text"></div>
                                                <a href="mailto:<?= $arResult['EMAIL']['VALUE'] ?>" class="widget-email-text">
                                                    <?= $arResult['EMAIL']['VALUE'] ?>
                                                </a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($arResult['FORMS']['CALL']['SHOW']) { ?>
                            <div class="widget-button-wrap intec-grid-item-auto">
                                <div class="widget-button intec-button intec-button-s-6 intec-button-r-3 intec-button-cl-common intec-button-transparent" data-action="forms.call.open">
                                    <?= Loc::getMessage('C_HEADER_TEMP1_DESKTOP_TEMP2_BUTTON') ?>
                                </div>
                                <?php include(__DIR__.'/../../../parts/forms/call.php') ?>
                            </div>
                        <?php } ?>
                        <div class="intec-grid-item"></div>
                        <?php if ($bBasketShow) { ?>
                            <div class="widget-basket-wrap intec-grid-item-auto">
                                <div class="widget-basket widget-item">
                                    <?php include(__DIR__.'/../../../parts/basket.php') ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($arResult['SEARCH']['SHOW']['DESKTOP']) { ?>
                            <div class="widget-search-wrap intec-grid-item-auto">
                                <div class="widget-search widget-item">
                                    <?php $arSearchParams = [
                                        'INPUT_ID' => $arParams['SEARCH_INPUT_ID'].'-desktop'
                                    ] ?>
                                    <?php include(__DIR__.'/../../../parts/search/popup.1.php') ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($arResult['AUTHORIZATION']['SHOW']['DESKTOP']) { ?>
                            <div class="widget-authorize-wrap intec-grid-item-auto">
                                <div class="widget-authorize widget-item">
                                    <?php include(__DIR__.'/../../../parts/auth/panel.php') ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?= Html::endTag('div') ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
