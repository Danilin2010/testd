<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use Bitrix\Main\Localization\Loc;
use intec\core\bitrix\component\InnerTemplate;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\JavaScript;

/**
 * @var array $arParams
 * @var array $arResult
 * @var array $arData
 * @var InnerTemplate $this
 */

$sTemplateId = $arData['id'];

?>
<div class="widget-view-desktop-6">
    <div class="intec-content intec-content-visible">
        <div class="intec-content-wrapper">
            <?= Html::beginTag('div', [
                'class' => [
                    'widget-wrapper',
                    'intec-grid' => [
                        '',
                        'nowrap',
                        'a-v-center',
                        'i-h-15'
                    ]
                ]
            ]) ?>
                <?php if ($arResult['MENU']['MAIN']['SHOW']['DESKTOP']) { ?>
                    <div class="widget-menu-wrap intec-grid-item-auto">
                        <div class="widget-item widget-menu">
                            <?php include(__DIR__.'/../../../parts/menu/main.popup.1.php') ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($arResult['LOGOTYPE']['SHOW']['DESKTOP']) { ?>
                    <div class="widget-logotype-wrap intec-grid-item-auto">
                        <a href="<?= SITE_DIR ?>" class="widget-item widget-logotype intec-image">
                            <div class="intec-aligner"></div>
                            <?php include(__DIR__.'/../../../parts/logotype.php') ?>
                        </a>
                    </div>
                <?php } ?>
                <div class="intec-grid-item"></div>
                <?php if ($arResult['PHONES']['SHOW']['DESKTOP'] || $arResult['EMAIL']['SHOW']['DESKTOP']) { ?>
                <?php
                    $arPhones = $arResult['PHONES']['VALUES'];
                    $arPhone = ArrayHelper::shift($arPhones);

                    if (!$arResult['PHONES']['SHOW']['DESKTOP']) {
                        $arPhones = [];
                        $arPhone = null;
                    }
                ?>
                    <div class="widget-contacts-wrap intec-grid-item-auto">
                        <?= Html::beginTag('div', [
                            'class' => [
                                'widget-item',
                                'widget-contacts'
                            ],
                            'data' => [
                                'multiple' => !empty($arPhones) ? 'true' : 'false',
                                'expanded' => 'false',
                                'block' => 'phone'
                            ]
                        ]) ?>
                            <?php if ($arResult['PHONES']['SHOW']['DESKTOP']) { ?>
                                <div class="widget-phone">
                                    <div class="widget-phone-content">
                                        <a href="tel:<?= $arPhone['VALUE'] ?>" class="widget-phone-text intec-cl-text-hover" data-block-action="popup.open">
                                            <?= $arPhone['DISPLAY'] ?>
                                        </a>
                                        <?php if (!empty($arPhones)) { ?>
                                            <div class="widget-phone-popup" data-block-element="popup">
                                                <div class="widget-phone-popup-wrapper">
                                                    <?php foreach ($arPhones as $arPhone) { ?>
                                                        <a href="tel:<?= $arPhone['VALUE'] ?>" class="widget-phone-popup-item intec-cl-text-hover">
                                                            <?= $arPhone['DISPLAY'] ?>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php if (!empty($arPhones)) { ?>
                                        <div class="widget-phone-arrow far fa-chevron-down" data-block-action="popup.open"></div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if ($arResult['EMAIL']['SHOW']['DESKTOP']) { ?>
                                <div class="widget-email-wrap">
                                    <a href="mailto:<?= $arResult['EMAIL']['VALUE'] ?>" class="widget-email intec-cl-text-hover">
                                        <?= $arResult['EMAIL']['VALUE'] ?>
                                    </a>
                                </div>
                            <?php } ?>
                        <?= Html::endTag('div') ?>
                        <?php if (!empty($arPhones)) { ?>
                            <script type="text/javascript">
                                (function ($) {
                                    $(document).on('ready', function () {
                                        var root = $(<?= JavaScript::toObject('#'.$sTemplateId) ?>);
                                        var block = $('[data-block="phone"]', root);
                                        var popup = $('[data-block-element="popup"]', block);

                                        popup.open = $('[data-block-action="popup.open"]', block);
                                        popup.open.on('mouseenter', function () {
                                            block.attr('data-expanded', 'true');
                                            popup.css({
                                                'display': 'block'
                                            }).stop().animate({
                                                'opacity': 1
                                            }, 500);
                                        });

                                        block.on('mouseleave', function () {
                                            block.attr('data-expanded', 'false');
                                            popup.stop().animate({
                                                'opacity': 0
                                            }, 500, function () {
                                                popup.css({
                                                    'display': 'none'
                                                });
                                            });
                                        });
                                    });
                                })(jQuery)
                            </script>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($arResult['FORMS']['CALL']['SHOW']) { ?>
                    <div class="widget-button-wrap intec-grid-item-auto">
                        <div class="widget-item widget-button-wrapper">
                            <div class="widget-button intec-cl-background intec-cl-background-light-hover" data-action="forms.call.open">
                                <?= Loc::getMessage('C_HEADER_TEMP1_DESKTOP_TEMP6_BUTTON') ?>
                            </div>
                            <?php include(__DIR__.'/../../../parts/forms/call.php') ?>
                        </div>
                    </div>
                <?php } ?>
            <?= Html::endTag('div') ?>
        </div>
    </div>
</div>