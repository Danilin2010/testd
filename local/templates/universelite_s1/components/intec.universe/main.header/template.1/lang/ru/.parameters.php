<?php

$MESS['C_HEADER_TEMP1_SETTINGS_USE'] = 'Использовать глобальные настройки';

$MESS['C_HEADER_TEMP1_TRANSPARENCY'] = 'Прозрачность';

$MESS['C_HEADER_TEMP1_LOGOTYPE_SHOW_FIXED'] = '[Фикс. шаблон] Отображать логотип';
$MESS['C_HEADER_TEMP1_LOGOTYPE_SHOW_MOBILE'] = '[Моб. шаблон] Отображать логотип';

$MESS['C_HEADER_TEMP1_PHONES_SHOW_FIXED'] = '[Фикс. шаблон] Отображать телефоны';
$MESS['C_HEADER_TEMP1_PHONES_SHOW_MOBILE'] = '[Моб. шаблон] Отображать телефоны';

$MESS['C_HEADER_TEMP1_AUTHORIZATION_SHOW'] = 'Отображать авторизацию';
$MESS['C_HEADER_TEMP1_AUTHORIZATION_SHOW_FIXED'] = '[Фикс. шаблон] Отображать авторизацию';
$MESS['C_HEADER_TEMP1_AUTHORIZATION_SHOW_MOBILE'] = '[Моб. шаблон] Отображать авторизацию';

$MESS['C_HEADER_TEMP1_EMAIL_SHOW'] = 'Отображать эл. почту';
$MESS['C_HEADER_TEMP1_EMAIL_SHOW_FIXED'] = '[Фикс. шаблон] Отображать эл. почту';
$MESS['C_HEADER_TEMP1_EMAIL_SHOW_MOBILE'] = '[Моб. шаблон] Отображать эл. почту';
$MESS['C_HEADER_TEMP1_EMAIL'] = 'Эл. почта';

$MESS['C_HEADER_TEMP1_ADDRESS_SHOW'] = 'Отображать адрес';
$MESS['C_HEADER_TEMP1_ADDRESS_SHOW_FIXED'] = '[Фикс. шаблон] Отображать адрес';
$MESS['C_HEADER_TEMP1_ADDRESS_SHOW_MOBILE'] = '[Моб. шаблон] Отображать адрес';
$MESS['C_HEADER_TEMP1_ADDRESS'] = 'Адрес';

$MESS['C_HEADER_TEMP1_TAGLINE_SHOW'] = 'Отображать слоган';
$MESS['C_HEADER_TEMP1_TAGLINE_SHOW_FIXED'] = '[Фикс. шаблон] Отображать слоган';
$MESS['C_HEADER_TEMP1_TAGLINE_SHOW_MOBILE'] = '[Моб. шаблон] Отображать слоган';
$MESS['C_HEADER_TEMP1_TAGLINE'] = 'Слоган';

$MESS['C_HEADER_TEMP1_SEARCH'] = 'Поиск.';
$MESS['C_HEADER_TEMP1_SEARCH_SHOW'] = 'Отображать поиск';
$MESS['C_HEADER_TEMP1_SEARCH_SHOW_FIXED'] = '[Фикс. шаблон] Отображать поиск';
$MESS['C_HEADER_TEMP1_SEARCH_SHOW_MOBILE'] = '[Моб. шаблон] Отображать поиск';

$MESS['C_HEADER_TEMP1_BASKET_SHOW'] = 'Отображать корзину';
$MESS['C_HEADER_TEMP1_BASKET_SHOW_FIXED'] = '[Фикс. шаблон] Отображать корзину';
$MESS['C_HEADER_TEMP1_BASKET_SHOW_MOBILE'] = '[Моб. шаблон] Отображать корзину';
$MESS['C_HEADER_TEMP1_BASKET_URL'] = 'Страница корзины';
$MESS['C_HEADER_TEMP1_DELAY_SHOW'] = 'Отображать отложенные товары';
$MESS['C_HEADER_TEMP1_DELAY_SHOW_FIXED'] = '[Фикс. шаблон] Отображать отложенные товары';
$MESS['C_HEADER_TEMP1_DELAY_SHOW_MOBILE'] = '[Моб. шаблон] Отображать отложенные товары';
$MESS['C_HEADER_TEMP1_COMPARE_SHOW'] = 'Отображать сравнение товаров';
$MESS['C_HEADER_TEMP1_COMPARE_SHOW_FIXED'] = '[Фикс. шаблон] Отображать сравнение товаров';
$MESS['C_HEADER_TEMP1_COMPARE_SHOW_MOBILE'] = '[Моб. шаблон] Отображать сравнение товаров';
$MESS['C_HEADER_TEMP1_COMPARE_IBLOCK_ID'] = 'Сравнение товаров. Инфоблок';
$MESS['C_HEADER_TEMP1_COMPARE_IBLOCK_TYPE'] = 'Сравнение товаров. Тип инфоблока';
$MESS['C_HEADER_TEMP1_COMPARE_CODE'] = 'Сравнение товаров. Код';
$MESS['C_HEADER_TEMP1_COMPARE_URL'] = 'Страница сравнения товаров';

$MESS['C_HEADER_TEMP1_MENU_MAIN'] = 'Главное меню.';
$MESS['C_HEADER_TEMP1_MENU_MAIN_SHOW'] = 'Отображать главное меню';
$MESS['C_HEADER_TEMP1_MENU_MAIN_SHOW_FIXED'] = '[Фикс. шаблон] Отображать главное меню';
$MESS['C_HEADER_TEMP1_MENU_MAIN_SHOW_MOBILE'] = '[Моб. шаблон] Отображать главное меню';
$MESS['C_HEADER_TEMP1_MENU_MAIN_ROOT'] = 'Главное меню. Корневой тип';
$MESS['C_HEADER_TEMP1_MENU_MAIN_LEVEL'] = 'Главное меню. Максимальная вложенность';
$MESS['C_HEADER_TEMP1_MENU_MAIN_CHILD'] = 'Главное меню. Вложенный тип';
$MESS['C_HEADER_TEMP1_MENU_MAIN_DELIMITERS'] = 'Главное меню. Использовать разделители';
$MESS['C_HEADER_TEMP1_MENU_MAIN_CATALOG_LINKS'] = 'Главное меню. Ссылки на каталоги';

$MESS['C_HEADER_TEMP1_SEARCH_URL'] = 'Страница поиска';
$MESS['C_HEADER_TEMP1_LOGIN_URL'] = 'Страница авторизации';
$MESS['C_HEADER_TEMP1_PASSWORD_URL'] = 'Страница восстановления пароля';
$MESS['C_HEADER_TEMP1_PROFILE_URL'] = 'Страница профиля';
$MESS['C_HEADER_TEMP1_REGISTER_URL'] = 'Страница регистрации';
$MESS['C_HEADER_TEMP1_CONSENT_URL'] = 'Ссылка на страницу соглашения на обработку данных';

$MESS['C_HEADER_TEMP1_FORMS_CALL_SHOW'] = 'Показывать кнопку "Заказать звонок"';
$MESS['C_HEADER_TEMP1_FORMS_CALL_ID'] = 'Форма для заказа звонка';
$MESS['C_HEADER_TEMP1_FORMS_CALL_TEMPLATE'] = 'Шаблон отображения формы заказа звонка';
$MESS['C_HEADER_TEMP1_FORMS_CALL_TITLE'] = 'Текст заголовка формы "Заказать звонок"';
$MESS['C_HEADER_TEMP1_FORMS_CALL_TITLE_DEFAULT'] = 'Заказать звонок';

$MESS['C_HEADER_TEMP1_DESKTOP'] = 'Шаблон';
$MESS['C_HEADER_TEMP1_FIXED'] = 'Шаблон (Фиксированный)';
$MESS['C_HEADER_TEMP1_MOBILE'] = 'Шаблон (Мобильный)';
$MESS['C_HEADER_TEMP1_MOBILE_FIXED'] = 'Шаблон (Мобильный) фиксированный';
$MESS['C_HEADER_TEMP1_BANNER'] = 'Шаблон баннера';