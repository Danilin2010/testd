<?php

use intec\core\bitrix\component\InnerTemplates;
use intec\core\bitrix\component\InnerTemplate;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;
use intec\core\helpers\Type;

/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

$fHandleDisplaying = function ($sKey, $fCondition = null) use (&$arParams) {
    $arResult = [
        'DESKTOP' => ArrayHelper::getValue($arParams, $sKey . '_SHOW') == 'Y',
        'FIXED' => ArrayHelper::getValue($arParams, $sKey . '_SHOW_FIXED') == 'Y',
        'MOBILE' => ArrayHelper::getValue($arParams, $sKey . '_SHOW_MOBILE') == 'Y'
    ];

    if ($fCondition instanceof Closure)
        foreach ($arResult as $sKey => $bValue)
            $arResult[$sKey] = $bValue && $fCondition($sKey);

    return $arResult;
};

$arParams['SETTINGS_USE'] = ArrayHelper::getValue($arParams, 'SETTINGS_USE');

if ($arParams['SETTINGS_USE'] == 'Y')
    include(__DIR__.'/parts/settings.php');

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

$arLogotype = $arResult['LOGOTYPE'];

$arLogotype['SHOW'] = $fHandleDisplaying('LOGOTYPE', function () use (&$arLogotype) {
    return !empty($arLogotype['PATH']);
});

$arPhones = $arResult['PHONES'];

$arPhones['SHOW'] = $fHandleDisplaying('PHONES', function () use (&$arPhones) {
    return !empty($arPhones['VALUES']);
});

$arAuthorization = [
    'SHOW' => $fHandleDisplaying('AUTHORIZATION')
];

$arEmail = [
    'SHOW' => null,
    'VALUE' => ArrayHelper::getValue($arParams, 'EMAIL')
];

$arEmail['SHOW'] = $fHandleDisplaying('EMAIL', function () use (&$arEmail) {
    return !empty($arEmail['VALUE']);
});

$arAddress = [
    'SHOW' => null,
    'VALUE' => ArrayHelper::getValue($arParams, 'ADDRESS')
];

$arAddress['SHOW'] = $fHandleDisplaying('ADDRESS', function () use (&$arAddress) {
    return !empty($arAddress['VALUE']);
});

$arTagline = [
    'SHOW' => $fHandleDisplaying('TAGLINE'),
    'VALUE' => ArrayHelper::getValue($arParams, 'TAGLINE')
];

$arTagline['SHOW'] = $fHandleDisplaying('TAGLINE', function () use (&$arTagline) {
    return !empty($arTagline['VALUE']);
});

$arFormsCall = [
    'SHOW' => ArrayHelper::getValue($arParams, 'FORMS_CALL_SHOW') == 'Y',
    'ID' => ArrayHelper::getValue($arParams, 'FORMS_CALL_ID'),
    'TEMPLATE' => ArrayHelper::getValue($arParams, 'FORMS_CALL_TEMPLATE'),
    'TITLE' => ArrayHelper::getValue($arParams, 'FORMS_CALL_TITLE')
];

if ($arFormsCall['SHOW'] && empty($arFormsCall['ID']))
    $arFormsCall['SHOW'] = false;

$arSearch = [
    'SHOW' => $fHandleDisplaying('SEARCH')
];

$arBasket = [
    'SHOW' => $fHandleDisplaying('BASKET')
];

$arDelay = array(
    'SHOW' => $fHandleDisplaying('DELAY')
);

$arCompare = [
    'SHOW' => $fHandleDisplaying('COMPARE'),
    'IBLOCK' => [
        'ID' => ArrayHelper::getValue($arParams, 'COMPARE_IBLOCK_ID'),
        'TYPE' => ArrayHelper::getValue($arParams, 'COMPARE_IBLOCK_TYPE')
    ],
    'CODE' => ArrayHelper::getValue($arParams, 'COMPARE_CODE')
];

$arMenu = [];

foreach (['MAIN'] as $sMenu) {
    $arMenu[$sMenu] = [
        'SHOW' => $fHandleDisplaying('MENU_'.$sMenu),
        'ROOT' => ArrayHelper::getValue($arParams, 'MENU_'.$sMenu.'_ROOT'),
        'CHILD' => ArrayHelper::getValue($arParams, 'MENU_'.$sMenu.'_CHILD'),
        'LEVEL' => ArrayHelper::getValue($arParams, 'MENU_'.$sMenu.'_LEVEL')
    ];
}

$arTemplates = [];
$arTemplates['DESKTOP'] = InnerTemplates::findOne($this, 'templates/desktop', $arParams['DESKTOP']);
$arTemplates['FIXED'] = InnerTemplates::findOne($this, 'templates/fixed', $arParams['FIXED']);
$arTemplates['MOBILE'] = InnerTemplates::findOne($this, 'templates/mobile', $arParams['MOBILE']);
$arTemplates['BANNER'] = InnerTemplates::findOne($this, 'templates/banners', $arParams['BANNER']);

$arMobile = [
    'FIXED' => ArrayHelper::getValue($arParams, 'MOBILE_FIXED') == 'Y'
];

$arUrl = [
    'LOGIN' => ArrayHelper::getValue($arParams, 'LOGIN_URL'),
    'PROFILE' => ArrayHelper::getValue($arParams, 'PROFILE_URL'),
    'PASSWORD' => ArrayHelper::getValue($arParams, 'PASSWORD_URL'),
    'REGISTER' => ArrayHelper::getValue($arParams, 'REGISTER_URL'),
    'SEARCH' => ArrayHelper::getValue($arParams, 'SEARCH_URL'),
    'BASKET' => ArrayHelper::getValue($arParams, 'BASKET_URL'),
    'COMPARE' => ArrayHelper::getValue($arParams, 'COMPARE_URL'),
    'CONSENT' => ArrayHelper::getValue($arParams, 'CONSENT_URL')
];

foreach ($arUrl as $sKey => $sUrl)
    $arUrl[$sKey] = StringHelper::replaceMacros(
        $sUrl,
        $arMacros
    );

$arVisual = [
    'TRANSPARENCY' => ArrayHelper::getValue($arParams, 'TRANSPARENCY') == 'Y'
];

$arResult['LOGOTYPE'] = $arLogotype;
$arResult['PHONES'] = $arPhones;
$arResult['AUTHORIZATION'] = $arAuthorization;
$arResult['ADDRESS'] = $arAddress;
$arResult['EMAIL'] = $arEmail;
$arResult['TAGLINE'] = $arTagline;
$arResult['FORMS'] = [];
$arResult['FORMS']['CALL'] = $arFormsCall;
$arResult['SEARCH'] = $arSearch;
$arResult['BASKET'] = $arBasket;
$arResult['DELAY'] = $arDelay;
$arResult['COMPARE'] = $arCompare;
$arResult['MENU'] = $arMenu;
$arResult['TEMPLATES'] = $arTemplates;
$arResult['MOBILE'] = $arMobile;
$arResult['URL'] = $arUrl;
$arResult['VISUAL'] = $arVisual;

/** @var InnerTemplate $oTemplate */
foreach ($arTemplates as $oTemplate) {
    if (empty($oTemplate))
        continue;

    $oTemplate->modify($arParams, $arResult);
}