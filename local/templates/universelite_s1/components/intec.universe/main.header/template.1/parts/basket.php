<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use intec\core\helpers\ArrayHelper;

$arBasketParams = !empty($arBasketParams) ? $arBasketParams : [];

?>
<?php $APPLICATION->IncludeComponent(
    "intec.universe:system.basket.icons",
    ".default",
    ArrayHelper::merge([
        "BASKET_SHOW" => $arResult['BASKET']['SHOW'] ? 'Y' : 'N',
        "DELAY_SHOW" => $arResult['DELAY']['SHOW'] ? 'Y' : 'N',
        "COMPARE_SHOW" => $arResult['COMPARE']['SHOW'] ? 'Y' : 'N',
        "BASKET_URL" => $arResult['URL']['BASKET'],
        "COMPARE_URL" => $arResult['URL']['COMPARE'],
        "COMPARE_CODE" => $arResult['COMPARE']['CODE'],
        "COMPARE_IBLOCK_TYPE" => $arResult['COMPARE']['IBLOCK']['TYPE'],
        "COMPARE_IBLOCK_ID" => $arResult['COMPARE']['IBLOCK']['ID']
    ], $arBasketParams),
    $this->getComponent()
) ?>
<?php unset($arBasketParams) ?>