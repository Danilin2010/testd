<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$sTemplateId = spl_object_hash($this);

$this->setFrameMode(true);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arVisual = $arResult['VISUAL'];
$arViewParams = ArrayHelper::getValue($arResult, 'VIEW_PARAMETERS');
$arFooter = $arResult['FOOTER_BLOCK'];

$arTextParams = [
    2 => 220,
    3 => 180,
    4 => 120
];

?>
<div class="intec-content">
    <div class="intec-content-wrapper">
        <div class="widget c-services c-services-template-1">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [ /** Контентный блок */
                'class' => Html::cssClassFromArray([
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-center'
                    ]
                ])
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = ArrayHelper::getValue($arItem, 'NAME');
                    $sDescription = ArrayHelper::getValue($arItem, 'PREVIEW_TEXT');
                    $sDescription = TruncateText($sDescription, $arTextParams[$arViewParams['LINE_COUNT']]);
                    $sDetailPage = ArrayHelper::getValue($arItem, 'DETAIL_PAGE_URL');

                ?>
                    <?= Html::beginTag('div', [
                        'class' => Html::cssClassFromArray([ /** Главный блок элемента */
                            'widget-element-wrap' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1100-3' => $arVisual['LINE_COUNT'] >= 4,
                                '850-2' => $arVisual['LINE_COUNT'] >= 3,
                                '600-1' => $arVisual['LINE_COUNT'] >= 2
                            ]
                        ], true)
                    ]) ?>
                        <div class="widget-element" id="<?= $sAreaId ?>">
                            <div class="widget-element-fade intec-cl-background"></div>
                            <div class="widget-element-name">
                                <?= $sName ?>
                            </div>
                            <?php if ($arVisual['DESCRIPTION_SHOW'] && !empty($sDescription)) { ?>
                                <div class="widget-element-description">
                                    <?= $sDescription ?>
                                </div>
                            <?php } ?>
                            <?php if ($arVisual['DETAIL']['SHOW']) { ?>
                                <div class="widget-element-detail-wrap">
                                    <a class="widget-element-detail" href="<?= $sDetailPage ?>">
                                        <span class="widget-element-detail-text">
                                            <?= $arVisual['DETAIL']['TEXT'] ?>
                                        </span>
                                        <i class="typcn typcn-arrow-right"></i>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
            <?php if ($arFooter['SHOW']) { ?>
                <div class="widget-footer align-<?= $arFooter['POSITION'] ?>">
                    <a class="widget-footer-all intec-cl-border intec-cl-background-hover" href="<?= $arFooter['LIST_PAGE'] ?>">
                        <?= $arFooter['TEXT'] ?>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
