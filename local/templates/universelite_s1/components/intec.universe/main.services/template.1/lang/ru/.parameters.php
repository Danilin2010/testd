<?php

$MESS['C_SERV_TEMP1_LINE_COUNT'] = 'Кол-во элементов в строке';
$MESS['C_SERV_TEMP1_ELEMENT_DESCRIPTION_SHOW'] = 'Отображать описание элементов';
$MESS['C_SERV_TEMP1_ELEMENT_DETAIL_SHOW'] = 'Отображать ссылку на детальную страницу';
$MESS['C_SERV_TEMP1_ELEMENT_DETAIL_TEXT'] = 'Текст ссылки на детальную страницу';
$MESS['C_SERV_TEMP1_FOOTER_SHOW'] = 'Отображать кнопку "Показать все"';
$MESS['C_SERV_TEMP1_FOOTER_POSITION'] = 'Расположение кнопки "Показать все"';
$MESS['C_SERV_TEMP1_FOOTER_TEXT'] = 'Текст кнопки "Показать все"';

$MESS['C_SERV_TEMP1_POSITION_LEFT'] = 'Слева';
$MESS['C_SERV_TEMP1_POSITION_CENTER'] = 'По центру';
$MESS['C_SERV_TEMP1_POSITION_RIGHT'] = 'Справа';

$MESS['C_SERV_TEMP1_ELEMENT_DETAIL_TEXT_DEFAULT'] = 'Смотреть';
$MESS['C_SERV_TEMP1_FOOTER_TEXT_DEFAULT'] = 'Показать все';