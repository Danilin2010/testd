<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Обработка настроенных параметров компонента */
$sDetailText = ArrayHelper::getValue($arParams, 'DETAIL_TEXT');
$sDetailText = trim($sDetailText);
$bDetailShow = ArrayHelper::getValue($arParams, 'DETAIL_SHOW') == 'Y';
$bDetailShow = $bDetailShow && !empty($sDetailText);
$iLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($iLineCount <= 2)
    $iLineCount = 2;

if ($iLineCount >= 4)
    $iLineCount = 4;

$arResult['VISUAL'] = [
    'LINE_COUNT' => $iLineCount,
    'DESCRIPTION_SHOW' => ArrayHelper::getValue($arParams, 'DESCRIPTION_SHOW') == 'Y',
    'DETAIL' => [
        'SHOW' => $bDetailShow,
        'TEXT' => Html::encode($sDetailText)
    ]
];

$arResult['VIEW_PARAMETERS'] = [
    'LINE_COUNT' => ArrayHelper::getValue($arParams, 'LINE_COUNT'),
    'ELEMENT_DESCRIPTION_SHOW' => ArrayHelper::getValue($arParams, 'DESCRIPTION_SHOW') == 'Y',
    'ELEMENT_DETAIL_SHOW' => ArrayHelper::getValue($arParams, 'DETAIL_SHOW') == 'Y',
    'ELEMENT_DETAIL_TEXT' => ArrayHelper::getValue($arParams, 'DETAIL_TEXT')
];

$sFooterText = ArrayHelper::getValue($arParams, 'FOOTER_TEXT');
$sFooterText = trim($sFooterText);
$sListPage = ArrayHelper::getValue($arParams, 'LIST_PAGE_URL');

if (!empty($sListPage)) {
    $sListPage = trim($sListPage);
    $sListPage = StringHelper::replaceMacros($sListPage, $arMacros);
} else {
    $sListPage = ArrayHelper::getFirstValue($arResult['ITEMS']);
    $sListPage = $sListPage['LIST_PAGE_URL'];
}

$bFooterShow = ArrayHelper::getValue($arParams, 'FOOTER_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sFooterText) && !empty($sListPage);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'FOOTER_POSITION'),
    'TEXT' => Html::encode($sFooterText),
    'LIST_PAGE' => $sListPage
];