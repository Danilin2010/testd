<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Коды свойств */
$arResult['PROPERTY_CODES'] = [
    'ICON' => ArrayHelper::getValue($arParams, 'PROPERTY_ICON')
];

/** Обработка настроенных параметров компонента */
$sDetailText = ArrayHelper::getValue($arParams, 'DETAIL_TEXT');
$sDetailText = trim($sDetailText);
$bDetailShow = ArrayHelper::getValue($arParams, 'DETAIL_SHOW') == 'Y';
$bDetailShow = $bDetailShow && !empty($sDetailText);
$iParallaxRatio = ArrayHelper::getValue($arParams, 'PARALLAX_RATIO');

if ($iParallaxRatio < 0) $iParallaxRatio = 0;
if ($iParallaxRatio > 100) $iParallaxRatio = 100;

$iParallaxRatio = (100 - $iParallaxRatio) / 100;

$arResult['VISUAL'] = [
    'DESCRIPTION_SHOW' => ArrayHelper::getValue($arParams, 'DESCRIPTION_SHOW') == 'Y',
    'ICON_SHOW' => ArrayHelper::getValue($arParams, 'ICON_SHOW') == 'Y',
    'NUMBER_SHOW' => ArrayHelper::getValue($arParams, 'NUMBER_SHOW') == 'Y',
    'DETAIL' => [
        'SHOW' => $bDetailShow,
        'TEXT' => Html::encode($sDetailText)
    ],
    'PARALLAX' => [
        'USE' => ArrayHelper::getValue($arParams, 'PARALLAX_USE') == 'Y',
        'RATIO' => $iParallaxRatio
    ]
];

/** Параметры кнопки "Показать все" */
$sFooterText = ArrayHelper::getValue($arParams, 'FOOTER_TEXT');
$sFooterText = trim($sFooterText);
$sListPage = ArrayHelper::getValue($arParams, 'LIST_PAGE_URL');

if (!empty($sListPage)) {
    $sListPage = trim($sListPage);
    $sListPage = StringHelper::replaceMacros($sListPage, $arMacros);
} else {
    $sListPage = ArrayHelper::getFirstValue($arResult['ITEMS']);
    $sListPage = $sListPage['LIST_PAGE_URL'];
}

$bFooterShow = ArrayHelper::getValue($arParams, 'FOOTER_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sFooterText) && !empty($sListPage);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'FOOTER_POSITION'),
    'TEXT' => $sFooterText
];

foreach ($arResult['ITEMS'] as $sKey => $arItem) {
    /** Обработка картинок */
    $sPropertyIcon = $arResult['PROPERTY_CODES']['ICON'];

    if (!empty($sPropertyIcon)) {
        $arFile = ArrayHelper::getValue($arItem, ['PROPERTIES', $sPropertyIcon, 'VALUE']);

        $arPicture = CFile::ResizeImageGet(
            $arFile,
            array(
                'width' => 120,
                'height' => 120
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT
        );

        $arPicture = array_change_key_case($arPicture, CASE_UPPER);
        $arItem['PROPERTIES'][$sPropertyIcon]['VALUE'] = $arPicture;
    }

    $arResult['ITEMS'][$sKey] = $arItem;
}