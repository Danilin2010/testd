<?php

$MESS['C_SERV_TEMP4_PROPERTY_ICON'] = 'Свойство "Иконка"';
$MESS['C_SERV_TEMP4_DESCRIPTION_SHOW'] = 'Отображать описание элемента';
$MESS['C_SERV_TEMP4_ICON_SHOW'] = 'Отображать иконку';
$MESS['C_SERV_TEMP4_NUMBER_SHOW'] = 'Отображать счетчик количества';
$MESS['C_SERV_TEMP4_DETAIL_SHOW'] = 'Отображать ссылку на детальную страницу';
$MESS['C_SERV_TEMP4_DETAIL_TEXT'] = 'Текст ссылки на детальную страницу';
$MESS['C_SERV_TEMP4_FOOTER_SHOW'] = 'Отображать кнопку "Показать все"';
$MESS['C_SERV_TEMP4_FOOTER_POSITION'] = 'Расположение кнопки "Показать все"';
$MESS['C_SERV_TEMP4_FOOTER_TEXT'] = 'Текст кнопки "Показать все"';

$MESS['C_SERV_TEMP4_POSITION_LEFT'] = 'Слева';
$MESS['C_SERV_TEMP4_POSITION_CENTER'] = 'По центру';
$MESS['C_SERV_TEMP4_POSITION_RIGHT'] = 'Справа';

$MESS['C_SERV_TEMP4_PARALLAX_USE'] = 'Использовать эффект паралакса';
$MESS['C_SERV_TEMP4_PARALLAX_RATIO'] = 'Сила эффекта паралакса (0 - 100)';

$MESS['C_SERV_TEMP4_DETAIL_TEXT_DEFAULT'] = 'Подробнее';
$MESS['C_SERV_TEMP4_FOOTER_TEXT_DEFAULT'] = 'Показать все';