<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$sTemplateId = spl_object_hash($this);

$this->setFrameMode(true);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arFooter = $arResult['FOOTER_BLOCK'];
$arVisual = $arResult['VISUAL'];
$arCodes = $arResult['PROPERTY_CODES'];

$iCounter = 1;
$sCountElements = count($arResult['ITEMS']);
$sCountNumber = 0;

?>
<div class="widget c-services c-services-template-4">
    <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
        <div class="intec-content intec-content-visible widget-header">
            <div class="intec-content-wrapper">
                <?php if ($arHeader['SHOW']) { ?>
                    <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                        <?= $arHeader['TEXT'] ?>
                    </div>
                <?php } ?>
                <?php if ($arDescription['SHOW']) { ?>
                    <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                        <?= $arDescription['TEXT'] ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div class="widget-content">
        <?php foreach ($arResult['ITEMS'] as $arItem) {

            $sId = $sTemplateId.'_'.$arItem['ID'];
            $sAreaId = $this->GetEditAreaId($sId);
            $this->AddEditAction($sId, $arItem['EDIT_LINK']);
            $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

            $sCountNumber++;

            $sIcon = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['ICON'], 'VALUE', 'SRC']);
            $sName = ArrayHelper::getValue($arItem, 'NAME');
            $sDescription = ArrayHelper::getValue($arItem, 'PREVIEW_TEXT');
            $sDescription = TruncateText($sDescription, 140);
            $sDetailPageUrl = ArrayHelper::getValue($arItem, 'DETAIL_PAGE_URL');
            $sElementBackground = ArrayHelper::getValue($arItem, ['DETAIL_PICTURE', 'SRC']);

        ?>
            <?= Html::beginTag('div', [ /** Главный тег элемента */
                'class' => 'widget-element-wrapper',
                'data-stellar-background-ratio' => $arVisual['PARALLAX']['USE'] ? $arVisual['PARALLAX']['RATIO'] : null,
                'style' => [
                    'background-image' => !empty($sElementBackground) ? 'url('.$sElementBackground.')' : null
                ],
                'id' => $sAreaId
            ]) ?>
                <div class="intec-content intec-content-visible widget-element">
                    <?= Html::beginTag('div', [ /** Блок для позиционирования контента */
                        'class' => Html::cssClassFromArray([
                            'intec' => [
                                'content-wrapper' => true,
                                'grid' => [
                                    '' => true,
                                    'important' => true,
                                    'wrap' => true,
                                    'a-v-stretch' => true,
                                    'a-h' => [
                                        'start' => $iCounter % 2 == 1,
                                        'end' => $iCounter % 2 != 1
                                    ]
                                ]
                            ]
                        ], true)
                    ]) ?>
                        <div class="widget-element-content intec-grid-item intec-grid-item-auto">
                            <?php if (($arVisual['ICON_SHOW'] && !empty($sIcon)) || $arVisual['NUMBER_SHOW']) { ?>
                                <div class="widget-element-icon-wrap">
                                    <?php if ($arVisual['ICON_SHOW'] && !empty($sIcon)) { ?>
                                        <?= Html::tag('div', '', [ /** Иконка элемента */
                                            'class' => 'widget-element-icon',
                                            'style' => [
                                                'background-image' => 'url('.$sIcon.')'
                                            ]
                                        ]) ?>
                                    <?php } ?>
                                    <div class="widget-element-number">
                                        <?php if ($arVisual['NUMBER_SHOW']) { ?>
                                            <?= $sCountNumber.' / '.$sCountElements ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="widget-element-name">
                                <?= $sName ?>
                            </div>
                            <?php if ($arVisual['DESCRIPTION_SHOW'] && !empty($sDescription)) { ?>
                                <div class="widget-element-description">
                                    <?= $sDescription ?>
                                </div>
                            <?php } ?>
                            <?php if ($arVisual['DETAIL']['SHOW']) { ?>
                                <div class="widget-element-detail-wrap">
                                    <a class="widget-element-detail" href="<?= $sDetailPageUrl ?>">
                                        <span class="element-detail-text">
                                            <?= $arVisual['DETAIL']['TEXT'] ?>
                                        </span>
                                        <i class="typcn typcn-arrow-right"></i>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    <?= Html::endTag('div') ?>
                </div>
            <?= Html::endTag('div') ?>
            <?php $iCounter++ ?>
        <?php } ?>
    </div>
    <?php if ($arFooter['SHOW']) { ?>
        <div class="intec-content">
            <div class="intec-content-wrapper widget-footer align-<?= $arFooter['POSITION'] ?>">
                <a class="widget-footer-all intec-cl-border intec-cl-background-hover" href="<?= $arFooter['LIST_PAGE'] ?>">
                    <?= $arFooter['TEXT'] ?>
                </a>
            </div>
        </div>
    <?php } ?>
</div>