<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

/**
 * @var array $arCurrentValues
 */

if (!Loader::includeModule('iblock'))
    return;

/** Получение списка форм и шаблонов для них */
$arForms = array();
$rsTemplates = array();
$arTemplates = array();
$arFormFields = array();

if ($arCurrentValues['BUTTON_SHOW'] == 'Y' && $arCurrentValues['BUTTON_TYPE'] == 'order') {
    if (Loader::includeModule('intec.startshop')) {
        include('parameters/lite.php');
    } elseif (Loader::includeModule('form')) {
        include('parameters/base.php');
    } else
        return;

    foreach ($rsTemplates as $arTemplate) {
        $arTemplates[$arTemplate['NAME']] = $arTemplate['NAME'] . (!empty($arTemplate['TEMPLATE']) ? ' (' . $arTemplate['TEMPLATE'] . ')' : null);
    }
}

/** Получение свойств инфоблока для выбора свойства цены услуги */
$arProperties = [];

if ($arCurrentValues['PRICE_SHOW'] == 'Y') {
    $rsProperties = CIBlockProperty::GetList(
        array('SORT' => 'ASC'),
        array(
            'IBLOCK_ID' => $arCurrentValues['IBLOCK_ID']
        )
    );

    while ($arProperty = $rsProperties->GetNext()) {
        if ($arProperty['PROPERTY_TYPE'] == 'S' && $arProperty['LIST_TYPE'] == 'L') {
            $arProperties[$arProperty['CODE']] = '['.$arProperty['CODE'].'] '.$arProperty['NAME'];
        }
    }
}

/** Параметры шаблона */
$arTemplateParameters['TEMPLATE_VIEW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SERV_TEMP2_TEMPLATE_VIEW'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        'mosaic' => Loc::getMessage('C_SERV_TEMP2_MOSAIC'),
        'blocks' => Loc::getMessage('C_SERV_TEMP2_BLOCKS')
    ),
    'REFRESH' => 'Y'
);

if ($arCurrentValues['TEMPLATE_VIEW'] == 'blocks') {
    $arTemplateParameters['LINE_COUNT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SERV_TEMP2_LINE_COUNT'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            2 => '2',
            3 => '3'
        ),
        'DEFAULT' => 3
    );
}

$arTemplateParameters['PRICE_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SERV_TEMP2_PRICE_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['PRICE_SHOW'] == 'Y') {
    $arTemplateParameters['PROPERTY_PRICE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SERV_TEMP2_PROPERTY_PRICE'),
        'TYPE' => 'LIST',
        'VALUES' => $arProperties,
        'ADDITIONAL_VALUES' => 'Y'
    );
}

$arTemplateParameters['BUTTON_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SERV_TEMP2_BUTTON_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['BUTTON_SHOW'] == 'Y') {
    $arTemplateParameters['BUTTON_TYPE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SERV_TEMP2_BUTTON_TYPE'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'order' => Loc::getMessage('C_SERV_TEMP2_BUTTON_TYPE_ORDER'),
            'detail' => Loc::getMessage('C_SERV_TEMP2_BUTTON_TYPE_DETAIL')
        ),
        'DEFAULT' => 'detail',
        'REFRESH' => 'Y'
    );

    if ($arCurrentValues['BUTTON_TYPE'] == 'order') {
        $arTemplateParameters['BUTTON_FORM_ID'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_SERV_TEMP2_BUTTON_FORM_ID'),
            'TYPE' => 'LIST',
            'VALUES' => $arForms,
            'ADDITIONAL_VALUES' => 'Y',
            'REFRESH' => 'Y'
        );

        if (!empty($arCurrentValues['BUTTON_FORM_ID'])) {
            $arTemplateParameters['FORM_FIELD'] = array(
                'PARENT' => 'VISUAL',
                'NAME' => Loc::getMessage('C_SERV_TEMP2_FORM_FIELD'),
                'TYPE' => 'LIST',
                'VALUES' => $arFormFields
            );
        }

        $arTemplateParameters['FORM_TEMPLATE'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_SERV_TEMP2_FORM_TEMPLATE'),
            'TYPE' => 'LIST',
            'VALUES' => $arTemplates,
            'DEFAULT' => '.default'
        );
        $arTemplateParameters['FORM_TITLE'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_SERV_TEMP2_FORM_TITLE'),
            'TYPE' => 'STRING',
            'DEFAULT' => Loc::getMessage('C_SERV_TEMP2_FORM_TITLE_DEFAULT')
        );
        $arTemplateParameters['CONSENT_URL'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_SERV_TEMP2_CONSENT_URL'),
            'TYPE' => 'STRING',
            'DEFAULT' => '#SITE_DIR#company/consent/'
        );
    }

    $arTemplateParameters['BUTTON_TEXT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SERV_TEMP2_BUTTON_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('C_SERV_TEMP2_BUTTON_TEXT_DEFAULT_DETAIL')
    );

    if ($arCurrentValues['BUTTON_TYPE'] == 'order') {
        $arTemplateParameters['BUTTON_TEXT'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_SERV_TEMP2_BUTTON_TEXT'),
            'TYPE' => 'STRING',
            'DEFAULT' => Loc::getMessage('C_SERV_TEMP2_BUTTON_TEXT_DEFAULT_ORDER')
        );
    }
}

$arTemplateParameters['SEE_ALL_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SERV_TEMP2_SEE_ALL_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['SEE_ALL_SHOW'] == 'Y') {
    $arTemplateParameters['SEE_ALL_POSITION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SERV_TEMP2_SEE_ALL_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'left' => Loc::getMessage('C_SERV_TEMP2_POSITION_LEFT'),
            'center' => Loc::getMessage('C_SERV_TEMP2_POSITION_CENTER'),
            'right' => Loc::getMessage('C_SERV_TEMP2_POSITION_RIGHT')
        ),
        'DEFAULT' => 'center'
    );
    $arTemplateParameters['SEE_ALL_TEXT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SERV_TEMP2_SEE_ALL_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('C_SERV_TEMP2_SEE_ALL_TEXT_DEFAULT')
    );
}