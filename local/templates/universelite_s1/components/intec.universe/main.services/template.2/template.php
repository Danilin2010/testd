<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\JavaScript;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = ArrayHelper::getValue($arResult, 'HEADER_BLOCK');
$arDescription = ArrayHelper::getValue($arResult, 'DESCRIPTION_BLOCK');
$arFooter = ArrayHelper::getValue($arResult, 'FOOTER_BLOCK');
$arVisual = ArrayHelper::getValue($arResult, 'VISUAL');
$arCodes = ArrayHelper::getValue($arResult, 'PROPERTY_CODES');

$arFormParams = [];
$arForm = [];

if ($arVisual['FORM']['USE']) {
    $arForm = $arVisual['FORM'];

    $arFormParams = [
        'id' => $arForm['ID'],
        'template' => $arForm['TEMPLATE'],
        'parameters' => [
            'AJAX_OPTION_ADDITIONAL' => $sTemplateId.'_FORM_ASK',
            'CONSENT_URL' => $arForm['CONSENT']
        ],
        'settings' => [
            'title' => $arForm['TITLE']
        ]
    ];
}

$iElementCount = 1;
$arClasses = [];

?>
<div class="intec-content">
    <div class="intec-content-wrapper">
        <div class="widget c-services c-services-template-2">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [
                'class' => Html::cssClassFromArray([
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-start'
                    ]
                ])
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = ArrayHelper::getValue($arItem, 'NAME');
                    $sPicture = ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']);
                    $sDetailPageUrl = ArrayHelper::getValue($arItem, 'DETAIL_PAGE_URL');
                    $sPrice = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['PRICE'], 'VALUE']);

                    if ($arVisual['FORM']['USE']) {
                        $arFormParams['fields'] = [
                            $arForm['FIELD'] => $sName
                        ];
                    }

                    if ($arVisual['VIEW'] == 'mosaic') {
                        if ($iElementCount <= 2) {
                            $arClasses = [
                                'widget-element-wrap',
                                'intec-grid-item' => [
                                    '2',
                                    '600-1'
                                ]
                            ];
                        } else if ($iElementCount >= 3) {
                            $arClasses = [
                                'widget-element-wrap',
                                'intec-grid-item' => [
                                    '3',
                                    '1000-2',
                                    '600-1'
                                ]
                            ];
                        }
                    } else if ($arVisual['VIEW'] == 'blocks') {
                        $arClasses = [
                            'widget-element-wrap',
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'],
                                '1000-2',
                                '600-1'
                            ]
                        ];
                    }

                ?>
                    <?= Html::beginTag('div', [ /** Главный блок элемента */
                        'class' => Html::cssClassFromArray($arClasses)
                    ]) ?>
                        <div class="widget-element" id="<?= $sAreaId ?>">
                            <?= Html::tag('div', '', [ /** Картинка элемента */
                                'class' => 'widget-element-picture',
                                'style' => [
                                    'background-image' => 'url('.$sPicture.')'
                                ]
                            ]) ?>
                            <div class="widget-element-fade"></div>
                            <div class="widget-element-text">
                                <div class="widget-element-name">
                                    <?= $sName ?>
                                </div>
                                <?php if ($arVisual['PRICE_SHOW'] && !empty($sPrice)) { ?>
                                    <div class="widget-element-price">
                                        <?= $sPrice ?>
                                    </div>
                                <?php } ?>
                                <?php if ($arVisual['BUTTON']['SHOW']) { ?>
                                    <div class="widget-element-button-wrapper">
                                        <?php if ($arVisual['BUTTON']['TYPE'] == 'order' && $arForm['USE']) { ?>
                                            <?= Html::tag('div', $arVisual['BUTTON']['TEXT'], [ /** Кнопка заказа услуги */
                                                'class' => 'widget-element-button intec-cl-background intec-cl-background-light-hover',
                                                'onclick' => 'universe.forms.show('.JavaScript::toObject($arFormParams).')'
                                            ]) ?>
                                        <?php } else if ($arVisual['BUTTON']['TYPE'] == 'detail') { ?>
                                            <?= Html::tag('a', $arVisual['BUTTON']['TEXT'], [ /** Кнопка перехода на детальную страницу */
                                                'class' => 'widget-element-button intec-cl-background intec-cl-background-light-hover',
                                                'href' => $sDetailPageUrl
                                            ]) ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?= Html::endTag('div') ?>
                    <?php if ($arVisual['VIEW'] == 'mosaic') {
                        if ($iElementCount == 5) {
                            $iElementCount = 1;
                        } else {
                            $iElementCount++;
                        }
                    } ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
            <?php if ($arFooter['SHOW']) { ?>
                <div class="widget-footer align-<?= $arFooter['POSITION'] ?>">
                    <a class="widget-footer-all intec-cl-border intec-cl-background-hover" href="<?= $arFooter['LIST_PAGE'] ?>">
                        <?= $arFooter['TEXT'] ?>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>