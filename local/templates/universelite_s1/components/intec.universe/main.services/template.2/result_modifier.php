<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Коды свойств */
$arResult['PROPERTY_CODES'] = [
    'PRICE' => ArrayHelper::getValue($arParams, 'PROPERTY_PRICE')
];

/** Обработка настроенных параметров компонента */
$bPriceShow = ArrayHelper::getValue($arParams, 'PRICE_SHOW') == 'Y';
$bPriceShow = $bPriceShow && !empty($arResult['PROPERTY_CODES']['PRICE']);
$sButtonText = ArrayHelper::getValue($arParams, 'BUTTON_TEXT');
$bButtonShow = ArrayHelper::getValue($arParams, 'BUTTON_SHOW') == 'Y';
$bButtonShow = $bButtonShow && !empty($sButtonText);
$sButtonType = ArrayHelper::getValue($arParams, 'BUTTON_TYPE');
$sFormId = ArrayHelper::getValue($arParams, 'BUTTON_FORM_ID');
$sFormField = ArrayHelper::getValue($arParams, 'FORM_FIELD');
$bFormUse = $bButtonShow && $sButtonType == 'order' && !empty($sFormId) && !empty($sFormField);
$sFormConsent = ArrayHelper::getValue($arParams, 'CONSENT_URL');
$sFormConsent = trim($sFormConsent);
$sFormConsent = StringHelper::replaceMacros($sFormConsent, $arMacros);
$iLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($iLineCount <= 2)
    $iLineCount = 2;

if ($iLineCount >= 3)
    $iLineCount = 3;

$arResult['VISUAL'] = [
    'VIEW' => ArrayHelper::getValue($arParams, 'TEMPLATE_VIEW'),
    'LINE_COUNT' => $iLineCount,
    'PRICE_SHOW' => $bPriceShow,
    'BUTTON' => [
        'SHOW' => $bButtonShow,
        'TEXT' => Html::encode($sButtonText),
        'TYPE' => $sButtonType
    ],
    'FORM' => [
        'USE' => $bFormUse,
        'ID' => $sFormId,
        'FIELD' => $sFormField,
        'TEMPLATE' => ArrayHelper::getValue($arParams, 'FORM_TEMPLATE'),
        'TITLE' => Html::encode(ArrayHelper::getValue($arParams, 'FORM_TITLE')),
        'CONSENT' => $sFormConsent
    ]
];

/** Параметры кнопки "Показать все" */
$sFooterText = ArrayHelper::getValue($arParams, 'SEE_ALL_TEXT');
$sFooterText = trim($sFooterText);
$sListPage = ArrayHelper::getValue($arParams, 'LIST_PAGE_URL');

if (!empty($sListPage)) {
    $sListPage = trim($sListPage);
    $sListPage = StringHelper::replaceMacros($sListPage, $arMacros);
} else {
    $sListPage = ArrayHelper::getFirstValue($arResult['ITEMS']);
    $sListPage = $sListPage['LIST_PAGE_URL'];
}

$bFooterShow = ArrayHelper::getValue($arParams, 'SEE_ALL_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sFooterText) && !empty($sListPage);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'SEE_ALL_POSITION'),
    'TEXT' => $sFooterText,
    'LIST_PAGE' => $sListPage
];