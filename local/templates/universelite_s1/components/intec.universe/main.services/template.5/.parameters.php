<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

$arTemplateParameters = array();

$arTemplateParameters = array(
    'LINE_COUNT' => array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SERV_TEMP5_LINE_COUNT'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            2 => '2',
            3 => '3',
            4 => '4'
        ),
        'DEFAULT' => 3
    )
);

$arTemplateParameters['LINK_USE'] = [
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SERV_TEMP5_LINK_USE'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
];

$arTemplateParameters['FOOTER_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SERV_TEMP5_FOOTER_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['FOOTER_SHOW'] == 'Y') {
    $arTemplateParameters['FOOTER_POSITION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SERV_TEMP5_FOOTER_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'left' => Loc::getMessage('C_SERV_TEMP5_POSITION_LEFT'),
            'center' => Loc::getMessage('C_SERV_TEMP5_POSITION_CENTER'),
            'right' => Loc::getMessage('C_SERV_TEMP5_POSITION_RIGHT')
        ),
        'DEFAULT' => 'center'
    );

    $arTemplateParameters['FOOTER_TEXT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SERV_TEMP5_FOOTER_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('C_SERV_TEMP5_FOOTER_TEXT_DEFAULT')
    );
}