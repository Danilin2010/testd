<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arFooter = $arResult['FOOTER_BLOCK'];
$arVisual = $arResult['VISUAL'];

?>
<div class="intec-content">
    <div class="intec-content-wrapper">
        <div class="widget c-services c-services-template-5">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [
                'class' => Html::cssClassFromArray([
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-center'
                    ]
                ])
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = ArrayHelper::getValue($arItem, 'NAME');
                    $sPicture = ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']);
                    $sDetailPageUrl = ArrayHelper::getValue($arItem, 'DETAIL_PAGE_URL');

                ?>
                    <?= Html::beginTag('div', [
                        'class' => Html::cssClassFromArray([
                            'widget-element-wrap' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1000-3' => $arVisual['LINE_COUNT'] >= 4,
                                '800-2' => $arVisual['LINE_COUNT'] >= 3,
                                '600-1' => $arVisual['LINE_COUNT'] >= 2
                            ]
                        ], true)
                    ]) ?>
                        <div class="widget-element intec-cl-background-hover" id="<?= $sAreaId ?>">
                            <?= Html::tag('div', '', [ /** Блок изображения элемента */
                                'class' => 'widget-element-picture',
                                'style' => [
                                    'background-image' => !empty($sPicture) ? 'url('.$sPicture.')' : null
                                ]
                            ]) ?>
                            <?php if ($arVisual['LINK_USE']) { ?>
                                <a class="widget-element-fade" href="<?= $sDetailPageUrl ?>"></a>
                            <?php } else { ?>
                                <div class="widget-element-fade"></div>
                            <?php } ?>
                            <div class="widget-element-name">
                                <span class="widget-element-name-text intec-cl-background">
                                    <?= $sName ?>
                                </span>
                            </div>
                        </div>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
            <?php if ($arFooter['SHOW']) { ?>
                <div class="widget-footer align-<?= $arFooter['POSITION'] ?>">
                    <a class="widget-footer-all intec-cl-border intec-cl-background-hover" href="<?= $arFooter['LIST_PAGE'] ?>">
                        <?= $arFooter['TEXT'] ?>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
