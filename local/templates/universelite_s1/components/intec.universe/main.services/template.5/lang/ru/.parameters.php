<?php

$MESS['C_SERV_TEMP5_LINE_COUNT'] = 'Кол-во элементов в строке';
$MESS['C_SERV_TEMP5_LINK_USE'] = 'Использовать ссылку на детальную страницу элемента';
$MESS['C_SERV_TEMP5_FOOTER_SHOW'] = 'Отображать кнопку "Показать все"';
$MESS['C_SERV_TEMP5_FOOTER_POSITION'] = 'Расположение кнопки "Показать все"';
$MESS['C_SERV_TEMP5_FOOTER_TEXT'] = 'Текст кнопки "Показать все"';

$MESS['C_SERV_TEMP5_POSITION_LEFT'] = 'Слева';
$MESS['C_SERV_TEMP5_POSITION_CENTER'] = 'По центру';
$MESS['C_SERV_TEMP5_POSITION_RIGHT'] = 'Справа';

$MESS['C_SERV_TEMP5_FOOTER_TEXT_DEFAULT'] = 'Показать все';