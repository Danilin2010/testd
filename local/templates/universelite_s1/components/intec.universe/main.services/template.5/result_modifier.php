<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Обработка настроенных параметров компонента */
$iLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($iLineCount <= 2)
    $iLineCount = 2;

if ($iLineCount >= 4)
    $iLineCount = 4;

$arResult['VISUAL'] = [
    'LINE_COUNT' => $iLineCount,
    'LINK_USE' => ArrayHelper::getValue($arResult, 'LINK_USE') == 'Y'
];

/** Параметры кнопки "Показать все" */
$sFooterText = ArrayHelper::getValue($arParams, 'FOOTER_TEXT');
$sFooterText = trim($sFooterText);
$sListPage = ArrayHelper::getValue($arParams, 'LIST_PAGE_URL');

if (!empty($sListPage)) {
    $sListPage = trim($sListPage);
    $sListPage = StringHelper::replaceMacros($sListPage, $arMacros);
} else {
    $sListPage = ArrayHelper::getFirstValue($arResult['ITEMS']);
    $sListPage = $sListPage['LIST_PAGE_URL'];
}

$bFooterShow = ArrayHelper::getValue($arParams, 'FOOTER_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sFooterText) && !empty($sListPage);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'FOOTER_POSITION'),
    'TEXT' => $sFooterText,
    'LIST_PAGE' => $sListPage
];

/** Обработка элементов */
foreach ($arResult['ITEMS'] as $sKey => $arItem) {
    $arPicture = $arItem['PREVIEW_PICTURE'];

    if (!empty($arPicture)) {
        $arResizePicture = CFile::ResizeImageGet(
            $arPicture,
            array(
                'width' => 700,
                'height' => 700
            ),
            BX_RESIZE_IMAGE_EXACT
        );

        $arPicture = $arResizePicture;
    }

    $arResult['ITEMS'][$sKey]['PREVIEW_PICTURE']['SRC'] = $arPicture['src'];
}