<?php

$MESS['C_SERV_TEMP3_LINE_COUNT'] = 'Кол-во элементов в строке';
$MESS['C_SERV_TEMP3_LINK_USE'] = 'Использовать ссылки на детальную страницу';
$MESS['C_SERV_TEMP3_FOOTER_SHOW'] = 'Отображать кнопку "Показать все"';
$MESS['C_SERV_TEMP3_FOOTER_POSITION'] = 'Расположение кнопки "Показать все"';
$MESS['C_SERV_TEMP3_FOOTER_TEXT'] = 'Текст кнопки "Показать все"';

$MESS['C_SERV_TEMP3_POSITION_LEFT'] = 'Слева';
$MESS['C_SERV_TEMP3_POSITION_CENTER'] = 'По центру';
$MESS['C_SERV_TEMP3_POSITION_RIGHT'] = 'Справа';

$MESS['C_SERV_TEMP3_FOOTER_TEXT_DEFAULT'] = 'Показать все';