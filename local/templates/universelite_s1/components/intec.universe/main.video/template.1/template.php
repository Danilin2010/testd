<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\JavaScript;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$sId = $sTemplateId.'_'.$arResult['ITEM']['ID'];
$sAreaId = $this->GetEditAreaId($sId);
$this->AddEditAction($sId, $arResult['ITEM']['EDIT_LINK']);
$this->AddDeleteAction($sId, $arResult['ITEM']['DELETE_LINK']);

$arHeader = ArrayHelper::getValue($arResult, 'HEADER_BLOCK');
$arDescription = ArrayHelper::getValue($arResult, 'DESCRIPTION_BLOCK');
$arVisual = ArrayHelper::getValue($arResult, 'VISUAL');
$arCodes = ArrayHelper::getValue($arResult, 'PROPERTY_CODES');
$arLink = ArrayHelper::getValue($arResult, ['ITEM', 'PROPERTIES', $arCodes['LINK'], 'VALUE']);

$sImage = ArrayHelper::getValue($arResult, ['ITEM', 'PREVIEW_PICTURE', 'SRC']);

?>
<?php if ($arVisual['TEMPLATE_SHOW']) { ?>
    <div class="widget c-video c-video-template-1" id="<?= $sTemplateId ?>">
        <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
            <div class="widget-header">
                <div class="intec-content">
                    <div class="intec-content-wrapper">
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (!$arVisual['WIDTH']) { ?>
            <div class="intec-content widget-content-wrap">
                <div class="intec-content-wrapper">
        <?php } ?>
        <div class="widget-content">
            <?= Html::beginTag('div', [ /** Главный тег элемента */
                'class' => 'widget-element intec-content-wrap',
                'id' => $sAreaId,
                'data-src' => $arLink,
                'data-stellar-background-ratio' => $arVisual['PARALLAX']['USE'] ? $arVisual['PARALLAX']['RATIO'] : null,
                'style' => [
                    'height' => $arVisual['HEIGHT'].'px',
                    'background-image' => 'url('.$sImage.')'
                ]
            ]) ?>
                <div class="intec-aligner"></div>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="widget-element-icon" style="fill: <?= $arVisual['COLOR_THEME'] ?>">
                    <path d="M216 354.9V157.1c0-10.7 13-16.1 20.5-8.5l98.3 98.9c4.7 4.7 4.7 12.2 0 16.9l-98.3 98.9c-7.5 7.7-20.5 2.3-20.5-8.4zM256 8c137 0 248 111 248 248S393 504 256 504 8 393 8 256 119 8 256 8zm0 48C145.5 56 56 145.5 56 256s89.5 200 200 200 200-89.5 200-200S366.5 56 256 56z"></path>
                </svg>
            <?= Html::endTag('div') ?>
        </div>
        <?php if (!$arVisual['WIDTH']) { ?>
                </div>
            </div>
        <?php } ?>
    </div>
    <script>
        (function ($, api) {
            $(document).ready(function () {
                var root = $(<?= JavaScript::toObject('#'.$sTemplateId) ?>);

                <?php if (!defined('EDITOR')) { ?>
                    $('.widget-content', root).lightGallery();
                <?php } ?>
            });
        })(jQuery, intec);
    </script>
<?php } else { ?>
    <div style="text-align: center; color: red; padding: 30px;">
        <?= Loc::getMessage('C_VIDEO_TEMP1_TEMPLATE_SHOW') ?>
    </div>
<?php } ?>