<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Коды свойств */
$sPropertyLink = ArrayHelper::getValue($arParams, 'PROPERTY_LINK');

$arResult['PROPERTY_CODES'] = [
    'LINK' => ArrayHelper::getValue($arParams, 'PROPERTY_LINK')
];

/** Настройки отображения шаблона */
$iLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($iLineCount <= 2) {
    $iLineCount = 2;
}
if ($iLineCount >= 3)
    $iLineCount = 3;

$arResult['VISUAL'] = [
    'LINE_COUNT' => $iLineCount
];

foreach ($arResult['ITEMS'] as $iKey => $arItem) {
    /** Обработка ссылок */
    $sPropertyLinkValue = ArrayHelper::getValue($arItem, ['PROPERTIES', $sPropertyLink, 'VALUE']);

    if (!empty($sPropertyLinkValue)) {
        $arItem['PROPERTIES'][$sPropertyLink]['VALUE'] = StringHelper::replaceMacros($sPropertyLinkValue, $arMacros);
    }

    /** Обработка картинок */
    $arPreviewPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');
    $arDetailPicture = ArrayHelper::getValue($arItem, 'DETAIL_PICTURE');
    $sNoImage = SITE_TEMPLATE_PATH.'/images/no-img/noimg_original.jpg';

    if (empty($arPreviewPicture) && !empty($arDetailPicture)) {
        $arPreviewPicture = $arDetailPicture;
    }

    if (!empty($arPreviewPicture)) {
        $arPicture = CFile::ResizeImageGet(
            $arPreviewPicture,
            array(
                'width' => 150,
                'height' => 150
            ),
            BX_RESIZE_IMAGE_EXACT
        );

        $arPreviewPicture['SRC'] = $arPicture['src'];
    } else {
        $arPreviewPicture['SRC'] = $sNoImage;
    }

    $arItem['PREVIEW_PICTURE'] = $arPreviewPicture;

    $arResult['ITEMS'][$iKey] = $arItem;
}