<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use Bitrix\Main\Localization\Loc;
use intec\core\helpers\StringHelper;
/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arVisual = $arResult['VISUAL'];
$arCodes = $arResult['PROPERTY_CODES'];

?>
<div class="intec-content">
    <div class="intec-content-wrapper">
        <div class="widget c-categories c-categories-template-2" id="<?=$sTemplateId?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [ /** Контентный тег */
                'class' => [
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-center'
                    ]
                ]
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);
                    $sName = $arItem['NAME'];
                    $sDescription = $arItem['PREVIEW_TEXT'];
                    $sImage = $arItem['PREVIEW_PICTURE']['SRC'];
                    $sLink = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK'], 'VALUE']);
                    $sTag = !empty($sLink) ? 'a' : 'div'

                ?>
                    <?= Html::beginTag('div', [ /** Главный тег элемента */
                        'id' => $sAreaId,
                        'class' => Html::cssClassFromArray([
                            'widget-element' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1100-2' => $arVisual['LINE_COUNT'] >=3,
                                '800-1' => true,
                            ]
                        ], true)
                    ]) ?>
                        <div class="widget-element-wrapper clearfix">
                            <?= Html::tag($sTag, '', [
                                'class' => 'widget-element-image-wrap',
                                'href' => !empty($sLink) ? $sLink : null,
                                'style' => [
                                    'background-image' => 'url('.$sImage.')'
                                ]
                            ]) ?>
                            <div class="widget-element-text">
                                <?= Html::tag($sTag, $sName, [
                                    'href' => !empty($sLink) ? $sLink : null,
                                    'class' => Html::cssClassFromArray([
                                        'widget-element-name' => true,
                                        'intec-cl-text' => [
                                            '' => empty($sLink),
                                            'light-hover' => empty($sLink)
                                        ]
                                    ], true)
                                ]) ?>
                                <?php if (!empty($sDescription)) { ?>
                                    <div class="widget-element-description">
                                        <?= $sDescription ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
        </div>
    </div>
</div>

