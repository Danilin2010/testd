<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !==true) die();

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arCodes = $arResult['PROPERTY_CODES'];
$arVisual = $arResult['VISUAL'];

?>
<div class="intec-content intec-content-visible">
    <div class="intec-content-wrapper">
        <div class="widget c-categories c-categories-template-4" id="<?= $sTemplateId ?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php }?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [ /** Контентный блок */
                'class' => [
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-center'
                    ]
                ]
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = ArrayHelper::getValue($arItem, 'NAME');
                    $sDescription = ArrayHelper::getValue($arItem, 'PREVIEW_TEXT');
                    $sImage = ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']);
                    $sLink = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK'], 'VALUE']);

                    $bAction = !empty($sDescription) || !empty($sLink)

                ?>
                    <?= Html::beginTag('div', [ /** Главный тег элемента */
                        'id' => $sAreaId,
                        'class' => [
                            'widget-element',
                            'intec-grid-item' => [
                                '2',
                                '750-1'
                            ]
                        ]
                    ]) ?>
                        <?= Html::beginTag('div', [ /** Картинка элемента */
                            'class' => 'widget-element-wrapper',
                            'style' => [
                                'background-image' => 'url('.$sImage.')'
                            ]
                        ]) ?>
                            <?php if ($bAction) { ?>
                                <div class="widget-element-fade"></div>
                            <?php } ?>
                            <div class="widget-element-information">
                                <?= Html::tag('div', $sName, [ /** Имя элемента */
                                    'class' => Html::cssClassFromArray([
                                        'widget-element-name' => true,
                                        'action' => $bAction
                                    ], true)
                                ]) ?>
                                <?php if ($bAction) { ?>
                                    <div class="widget-element-description-wrap">
                                        <div class="widget-element-description-name">
                                            <?= $sName ?>
                                        </div>
                                        <?php if (!empty($sDescription)) { ?>
                                            <div class="widget-element-description">
                                                <?= $sDescription ?>
                                            </div>
                                        <?php } ?>
                                        <?php if (!empty($sLink)) { ?>
                                            <a class="widget-element-description-button" href="<?= $sLink ?>">
                                                <span>
                                                    <?= Loc::getMessage('C_CATEGORIES_TEMP_4_INFORMATION') ?>
                                                </span>
                                                <i class="fal fa-chevron-right"></i>
                                            </a>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?= Html::endTag('div') ?>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
        </div>
    </div>
</div>