<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Коды свойств */
$sPropertyLink = ArrayHelper::getValue($arParams, 'PROPERTY_LINK');

$arResult['PROPERTY_CODES'] = [
    'LINK' => $sPropertyLink
];

foreach ($arResult['ITEMS'] as $iKey => $arItem) {
    /** Обработка ссылок */
    $sPropertyLinkValue = ArrayHelper::getValue($arItem, ['PROPERTIES', $sPropertyLink, 'VALUE']);

    if (!empty($sPropertyLinkValue)) {
        $arItem['PROPERTIES'][$sPropertyLink]['VALUE'] = StringHelper::replaceMacros($sPropertyLinkValue, $arMacros);
    }

    /** Обработка картинок */
    $arPreviewPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');
    $arDetailPicture = ArrayHelper::getValue($arItem, 'DETAIL_PICTURE');
    $sNoImage = SITE_TEMPLATE_PATH.'/images/no-img/noimg_original.jpg';

    if (empty($arPreviewPicture) && !empty($arDetailPicture)) {
        $arPreviewPicture = $arDetailPicture;
    }

    if (!empty($arPreviewPicture)) {
        $arPicture = CFile::ResizeImageGet(
            $arPreviewPicture,
            array(
                'width' => 800,
                'height' => 800
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT
        );

        $arPreviewPicture['SRC'] = $arPicture['src'];
    } else {
        $arPreviewPicture['SRC'] = $sNoImage;
    }

    $arItem['PREVIEW_PICTURE'] = $arPreviewPicture;

    $arResult['ITEMS'][$iKey] = $arItem;
}