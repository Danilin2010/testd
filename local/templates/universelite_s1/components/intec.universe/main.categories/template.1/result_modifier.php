<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arResult['PROPERTY_CODES'] = [
    'LINK' => ArrayHelper::getValue($arParams, 'PROPERTY_LINK')
];
