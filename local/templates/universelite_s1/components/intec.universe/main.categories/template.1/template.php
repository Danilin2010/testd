<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use Bitrix\Main\Localization\Loc;
use intec\core\helpers\StringHelper;
/**
 * @var array $arResult
 */

$sTemplateId = spl_object_hash($this);

$this->setFrameMode(true);

$bHeaderShow = ArrayHelper::getValue($arResult, ['HEADER_BLOCK', 'SHOW']);
$sHeaderPosition = ArrayHelper::getValue($arResult, ['HEADER_BLOCK', 'POSITION']);
$sHeaderText = ArrayHelper::getValue($arResult, ['HEADER_BLOCK', 'TEXT']);

$bDescriptionShow = ArrayHelper::getValue($arResult, ['DESCRIPTION_BLOCK', 'SHOW']);
$sDescriptionPosition = ArrayHelper::getValue($arResult, ['DESCRIPTION_BLOCK', 'POSITION']);
$sDescriptionText = ArrayHelper::getValue($arResult, ['DESCRIPTION_BLOCK', 'TEXT']);

$arCodes = ArrayHelper::getValue($arResult, ['PROPERTY_CODES']);
?>
<div class="intec-content intec-content-visible">
    <div class="intec-content-wrapper">
        <div class="widget c-categories c-categories-template-1" id="<?=$sTemplateId?>">
            <?php if ($bHeaderShow || $bDescriptionShow) { ?>
                <div class="widget-header">
                    <?php if ($bHeaderShow && !empty($sHeaderText)) { ?>
                        <div class="widget-title align-<?= $sHeaderPosition?>">
                            <?= Html:: encode ($sHeaderText)?>
                        </div>
                    <?php } ?>
                    <?php if ($bDescriptionShow && !empty($sDescriptionText)) { ?>
                        <div class="widget-description align-<?= $sDescriptionPosition ?>">
                            <?= Html::encode($sDescriptionText) ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="widget-content">
                <div class="widget-elements">
                    <div class="widget-elements-wrapper">
                        <?php foreach ($arResult['ITEMS'] as $arItem){
                            $sId = $sTemplateId.'_'.$arItem['ID'];
                            $sAreaId = $this->GetEditAreaId($sId);
                            $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                            $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);
                            $sName = $arItem['NAME'];
                            $sImage = $arItem['PREVIEW_PICTURE']['SRC'];
                            $sLink = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK'], 'VALUE']);
                            $sLink = StringHelper::replaceMacros($sLink, [
                                'SITE_DIR' => SITE_DIR
                            ]);
                        ?>
                        <div class="widget-element">
                            <div class="widget-element-wrapper intec-cl-border-hover" id="<?= $sAreaId?>">
                                <a href="<?= $sLink ?>" class="widget-element-content-wrapper ">
                                    <div class="widget-element-image-wrap intec-image">
                                        <div class="intec-aligner"></div>
                                        <img class="widget-element-image" src='<?= $sImage ?>'>
                                    </div>
                                    <div class="widget-element-name-wrap">
                                        <div class="widget-element-name">
                                            <?= Html::encode($sName) ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
