<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Параметры кнопки "Показать все" */
$sFooterText = ArrayHelper::getValue($arParams, 'FOOTER_TEXT');
$sFooterText = trim($sFooterText);
$sListPage = ArrayHelper::getValue($arParams, 'LIST_PAGE');
$sListPage = trim($sListPage);
$sListPage = StringHelper::replaceMacros($sListPage, $arMacros);
$bFooterShow = ArrayHelper::getValue($arParams, 'FOOTER_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sFooterText) && !empty($sListPage);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'FOOTER_POSITION'),
    'TEXT' => Html::encode($sFooterText),
    'LIST_PAGE' => $sListPage
];

/** Параметры отображения */
$arResult['VISUAL'] = [
    'VIEW' => ArrayHelper::getValue($arParams, 'VIEW_STYLE'),
    'NAME' => [
        'VERTICAL' => ArrayHelper::getValue($arParams, 'NAME_VERTICAL'),
        'HORIZONTAL' => ArrayHelper::getValue($arParams, 'NAME_HORIZONTAL')
    ]
];

/** Коды свойств */
$sLink = ArrayHelper::getValue($arParams, 'PROPERTY_LINK');

$arResult['PROPERTY_CODES'] = [
    'LINK' => $sLink
];

foreach ($arResult['ITEMS'] as $iKey => $arItem) {
    /** Обработка ссылок */
    $sLinkValue = ArrayHelper::getValue($arItem, ['PROPERTIES', $sLink, 'VALUE']);
    $sLinkValue = trim($sLinkValue);
    $sLinkValue = StringHelper::replaceMacros($sLinkValue, $arMacros);

    $arItem['PROPERTIES'][$sLink]['VALUE'] = $sLinkValue;

    /** Обработка картинок */
    $arPreviewPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');
    $arDetailPicture = ArrayHelper::getValue($arItem, 'DETAIL_PICTURE');
    $sNoImage = SITE_TEMPLATE_PATH.'/images/no-img/noimg_original.jpg';

    if (empty($arPreviewPicture) && !empty($arDetailPicture)) {
        $arPreviewPicture = $arDetailPicture;
    }

    if (!empty($arPreviewPicture)) {
        $arPicture = CFile::ResizeImageGet(
            $arPreviewPicture,
            array(
                'width' => 800,
                'height' => 800
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT
        );

        $arPreviewPicture['SRC'] = $arPicture['src'];
    } else {
        $arPreviewPicture['SRC'] = $sNoImage;
    }

    $arItem['PREVIEW_PICTURE'] = $arPreviewPicture;

    $arResult['ITEMS'][$iKey] = $arItem;
}