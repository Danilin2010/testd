<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

if (!Loader::includeModule('iblock'))
    return;

$arPropertiesText = array();

if (!empty($arCurrentValues['IBLOCK_ID'])) {
    /** Список свойств инфоблока */
    $rsProperties = CIBlockProperty::GetList(
        array(),
        array(
            'IBLOCK_ID' => $arCurrentValues['IBLOCK_ID']
        )
    );

    while ($arProperty = $rsProperties->Fetch()) {
        if ($arProperty['PROPERTY_TYPE'] == 'S' && $arProperty['LIST_TYPE'] == 'L') {
            $arPropertiesText[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        }
    }
}


/** Параметры шаблона */
if (!empty($arCurrentValues['IBLOCK_ID'])) {
    $arTemplateParameters['PROPERTY_LINK'] = array(
        'PARENT' => 'DATA_SOURCE',
        'NAME' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_PROPERTY_LINK'),
        'TYPE' => 'LIST',
        'VALUES' => $arPropertiesText,
        'ADDITIONAL_VALUES' => 'Y'
    );
}

$arTemplateParameters['VIEW_STYLE'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_VIEW_STYLE'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        'standard' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_VIEW_STYLE_STANDARD'),
        'chess' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_VIEW_STYLE_CHESS')
    ),
    'DEFAULT' => 'chess'
);
$arTemplateParameters['NAME_VERTICAL'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_NAME_VERTICAL'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        'top' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_VERTICAL_TOP'),
        'middle' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_VERTICAL_MID'),
        'bottom' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_VERTICAL_BOT')
    ),
    'DEFAULT' => 'center'
);
$arTemplateParameters['NAME_HORIZONTAL'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_NAME_HORIZONTAL'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        'left' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_POSITION_LEFT'),
        'center' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_POSITION_CENTER'),
        'right' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_POSITION_RIGHT')
    ),
    'DEFAULT' => 'center'
);
$arTemplateParameters['FOOTER_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_FOOTER_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['FOOTER_SHOW'] == 'Y') {
    $arTemplateParameters['FOOTER_POSITION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_FOOTER_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'left' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_POSITION_LEFT'),
            'center' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_POSITION_CENTER'),
            'right' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_POSITION_RIGHT')
        ),
        'DEFAULT' => 'center'
    );
    $arTemplateParameters['FOOTER_TEXT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_FOOTER_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_FOOTER_TEXT_DEFAULT')
    );
    $arTemplateParameters['LIST_PAGE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('LANDING_CATEGORIES_TEMP6_LIST_PAGE'),
        'TYPE' => 'STRING'
    );
}