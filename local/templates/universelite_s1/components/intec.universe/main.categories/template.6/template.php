<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$sTemplateId = spl_object_hash($this);

$this->setFrameMode(true);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arFooter = $arResult['FOOTER_BLOCK'];
$arVisual = $arResult['VISUAL'];
$arCodes = $arResult['PROPERTY_CODES'];

$iRepeatBlock = 1;
$iRepeatCounter = 2;
$iSize = null;

$arBlockSize = [
    0 => 'big',
    1 => 'small',
    2 => 'standard'
];

?>
<div class="intec-content intec-content-visible">
    <div class="intec-content-wrapper">
        <div class="widget c-categories c-categories-template-6">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="widget-content">
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sPicture = ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']);
                    $sLink = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK'], 'VALUE']);
                    $sName = ArrayHelper::getValue($arItem, 'NAME');
                    $sTag = !empty($sLink) ? 'a' : 'div';

                    if ($arVisual['VIEW'] == 'chess') {
                        $iSize = $iRepeatBlock % 2;

                        if ($iRepeatCounter == 2) {
                            $iRepeatCounter = 1;
                            $iRepeatBlock++;
                        } else {
                            $iRepeatCounter++;
                        }
                    } else {
                        $iSize = 2;
                    }

                ?>
                    <div class="widget-element-wrap grid-<?= $arBlockSize[$iSize] ?>">
                        <?= Html::beginTag($sTag, [ /** Тег-обертка элемента */
                            'class' => 'widget-element intec-cl-background-hover',
                            'id' => $sAreaId,
                            'href' => $sLink,
                            'style' => [
                                'background-image' => 'url('.$sPicture.')',
                                'text-align' => $arVisual['NAME']['HORIZONTAL']
                            ]
                        ]) ?>
                            <div class="intec-aligner"></div>
                            <?= Html::tag('div', $sName, [ /** Название элемента */
                                'class' => 'widget-element-name',
                                'style' => [
                                    'vertical-align' => $arVisual['NAME']['VERTICAL']
                                ]
                            ]) ?>
                        <?= Html::endTag($sTag) ?>
                    </div>
                <?php } ?>
            </div>
            <?php if ($arFooter['SHOW']) { ?>
                <div class="widget-footer align-<?= $arFooter['POSITION'] ?>">
                    <a class="widget-footer-all intec-cl-border intec-cl-background-hover" href="<?= $arFooter['LIST_PAGE'] ?>">
                        <?= $arFooter['TEXT'] ?>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
