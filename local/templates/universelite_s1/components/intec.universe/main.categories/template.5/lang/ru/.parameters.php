<?php

$MESS['C_CATEGORIES_TEMP5_PROPERTY_LINK'] = 'Свойство "Ссылка"';
$MESS['C_CATEGORIES_TEMP5_PROPERTY_SIZE'] = 'Свойство "Размер картинки"';
$MESS['C_CATEGORIES_TEMP5_NAME_HORIZONTAL'] = 'Расположение текста по горизонтали';
$MESS['C_CATEGORIES_TEMP5_NAME_VERTICAL'] = 'Расположение текста по вертикали';

$MESS['C_CATEGORIES_TEMP5_NAME_LEFT'] = 'Слева';
$MESS['C_CATEGORIES_TEMP5_NAME_CENTER'] = 'По центру';
$MESS['C_CATEGORIES_TEMP5_NAME_RIGHT'] = 'Справа';
$MESS['C_CATEGORIES_TEMP5_NAME_TOP'] = 'Сверху';
$MESS['C_CATEGORIES_TEMP5_NAME_BOTTOM'] = 'Сверху';