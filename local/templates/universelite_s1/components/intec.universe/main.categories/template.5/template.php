<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !==true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION'];
$arVisual = $arResult['VISUAL'];
$arCodes = $arResult['PROPERTY_CODES'];

$sNameHorizontal = ArrayHelper::fromRange(['left', 'center', 'right'], $arVisual['NAME']['HORIZONTAL']);
$sNameVertical = ArrayHelper::fromRange(['bottom', 'center', 'top'], $arVisual['NAME']['VERTICAL']);

?>
<div class="intec-content intec-content-visible">
    <div class="intec-content-wrapper">
        <div class="widget c-categories c-categories-template-5" id="<?= $sTemplateId ?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php }?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [ /** Контентный блок */
                'class' => [
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-start'
                    ]
                ]
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = $arItem['NAME'];
                    $sImage = $arItem['PREVIEW_PICTURE']['SRC'];
                    $sSize = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['SIZE'], 'VALUE_XML_ID']);
                    $sSize = ArrayHelper::fromRange(['small', 'medium'], $sSize);
                    $sLink = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK'], 'VALUE']);
                    $sTag = !empty($sLink) ? 'a' : 'div';

                ?>
                    <?= Html::beginTag('div', [ /** Главный тег элемента */
                        'class' => Html::cssClassFromArray([
                            'widget-element' => true,
                            'intec-grid-item' => [
                                '4' => $sSize == 'small',
                                '2' => $sSize == 'medium',
                                '1000-2' => $sSize == 'small',
                                '700-1' => true
                            ]
                        ], true)
                    ]) ?>
                        <?= Html::beginTag($sTag, [ /** Картинка элемента */
                            'id' => $sAreaId,
                            'href' => $sTag == 'a' ? $sLink : null,
                            'class' => 'widget-element-wrapper',
                            'style' => [
                                'background-image' => 'url('.$sImage.')'
                            ]
                        ]) ?>
                            <?= Html::beginTag('div', [ /** Тег-обертка для выравнивания названия */
                                'class' => 'widget-element-name-wrap',
                                'style' => [
                                    'text-align' => $sNameHorizontal
                                ]
                            ]) ?>
                                <div class="intec-aligner"></div>
                                <?= Html::tag('div', $sName, [ /** Название элемента */
                                    'class' => 'widget-element-name',
                                    'style' => [
                                        'vertical-align' => $sNameVertical
                                    ]
                                ]) ?>
                            <?= Html::endTag('div') ?>
                        <?= Html::endTag($sTag) ?>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
        </div>
    </div>
</div>
