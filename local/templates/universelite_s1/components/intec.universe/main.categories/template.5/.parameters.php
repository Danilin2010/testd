<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

if (!Loader::includeModule('iblock'))
    return;

$arPropertiesText = [];
$arPropertiesList = [];

if (!empty($arCurrentValues['IBLOCK_ID'])) {
    $rsProperties = CIBlockProperty::GetList(
        array(),
        array(
            'IBLOCK_ID' => $arCurrentValues['IBLOCK_ID']
        )
    );

    while ($arProperty = $rsProperties->Fetch()) {
        if ($arProperty['PROPERTY_TYPE'] == 'S' && $arProperty['LIST_TYPE'] == 'L') {
            $arPropertiesText[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        } else if ($arProperty['PROPERTY_TYPE'] == 'L' && $arProperty['LIST_TYPE'] == 'L') {
            $arPropertiesList[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        }
    }


    $arTemplateParameters['PROPERTY_LINK'] = array (
        'PARENT' => 'DATA_SOURCE',
        'NAME' => Loc::getMessage('C_CATEGORIES_TEMP5_PROPERTY_LINK'),
        'TYPE' => 'LIST',
        'VALUES' => $arPropertiesText,
        'ADDITIONAL_VALUES' => 'Y'
    );
    $arTemplateParameters['PROPERTY_SIZE'] = array (
        'PARENT' => 'DATA_SOURCE',
        'NAME' => Loc::getMessage('C_CATEGORIES_TEMP5_PROPERTY_SIZE'),
        'TYPE' => 'LIST',
        'ADDITIONAL_VALUES' => 'Y',
        'VALUES' => $arPropertiesList
    );
}

$arTemplateParameters['NAME_HORIZONTAL'] = array (
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_CATEGORIES_TEMP5_NAME_HORIZONTAL'),
    'TYPE' => 'LIST',
    'VALUES' => [
        'left' => Loc::getMessage('C_CATEGORIES_TEMP5_NAME_LEFT'),
        'center' => Loc::getMessage('C_CATEGORIES_TEMP5_NAME_CENTER'),
        'right' => Loc::getMessage('C_CATEGORIES_TEMP5_NAME_RIGHT')
    ],
    'DEAFULT' => 'left'
);

$arTemplateParameters['NAME_VERTICAL'] = array (
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_CATEGORIES_TEMP5_NAME_VERTICAL'),
    'TYPE' => 'LIST',
    'VALUES' => [
        'top' => Loc::getMessage('C_CATEGORIES_TEMP5_NAME_TOP'),
        'center' => Loc::getMessage('C_CATEGORIES_TEMP5_NAME_CENTER'),
        'bottom' => Loc::getMessage('C_CATEGORIES_TEMP5_NAME_BOTTOM')
    ],
    'DEFAULT' => 'bottom'
);
