<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !==true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
/**
 * @var array $arResult
 */

$sTemplateId = spl_object_hash($this);

$this->setFrameMode(true);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arVisual = $arResult['VISUAL'];
$arCodes = $arResult['PROPERTY_CODES'];

?>
<div class="intec-content intec-content-visible">
    <div class="intec-content-wrapper">
        <div class="widget c-categories c-categories-template-3" id="<?=$sTemplateId?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [ /** Контентный тег */
                'class' => [
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-center'
                    ]
                ]
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);
                    $sName = $arItem['NAME'];
                    $sImage = $arItem['PREVIEW_PICTURE']['SRC'];
                    $sLink = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK'], 'VALUE']);
                    $sTag = !empty($sLink) ? 'a' : 'div';

                ?>
                    <?= Html::beginTag('div', [ /** Главный тег элемента */
                        'id' => $sAreaId,
                        'class' => Html::cssClassFromArray([
                            'widget-element' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1000-3' => $arVisual['LINE_COUNT'] >= 4,
                                '800-2' => $arVisual['LINE_COUNT'] >= 3,
                                '600-1' => $arVisual['LINE_COUNT'] >= 2
                            ]
                        ], true)
                    ]) ?>
                        <?= Html::beginTag($sTag, [ /** Картинка элемента */
                            'class' => 'widget-element-wrapper intec-cl-background-hover',
                            'href' => !empty($sLink) ? $sLink : null,
                            'style' => [
                                'background-image' => 'url('.$sImage.')'
                            ]
                        ]) ?>
                            <div class="widget-element-fade"></div>
                            <div class="widget-element-name">
                                <?= $sName ?>
                            </div>
                        <?= Html::endTag($sTag) ?>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
        </div>
    </div>
</div>
