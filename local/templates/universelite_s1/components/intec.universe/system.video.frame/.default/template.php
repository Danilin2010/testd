<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 * @var array $arParams
 */

$this->setFrameMode(true);
$sTemplateId = spl_object_hash($this);

$arVisual = ArrayHelper::getValue($arResult, 'VISUAL');

$sUrl = ArrayHelper::getValue($arResult, 'URL');
$sAutoPlay = !defined('EDITOR') ? '1' : '0';

$arShadow = [];

if ($arVisual['SHADOW']['USE']) {
    $arShadow = [
        'class' => 'widget-iframe-shadow widget-iframe-shadow-'.$arVisual['SHADOW']['COLOR']['VALUE'],
        'style' => [
            'background-color' => !empty($arVisual['SHADOW']['COLOR']['CUSTOM']) ? $arVisual['SHADOW']['COLOR']['CUSTOM'] : null,
            'opacity' => !empty($arVisual['SHADOW']['OPACITY']) ? $arVisual['SHADOW']['OPACITY'] : null
        ]
    ];
} else {
    $arShadow = [
        'class' => 'widget-iframe-shadow'
    ];
}

?>
<?= Html::beginTag('div', [
    'class' => 'widget c-video-frame c-video-frame-default',
    'style' => [
        'height' => !empty($arVisual['HEIGHT']) ? $arVisual['HEIGHT'].'px' : null
    ]
]) ?>
    <?= Html::tag('div', '', $arShadow) ?>
    <div class="widget-iframe-wrap">
        <?= Html::tag('iframe', '', [
            'src' => $sUrl['iframe'].'?rel=0&mute=1&controls=0&loop=1&showinfo=0&autoplay='.$sAutoPlay.'&playlist='.$sUrl['id'],
            'frameborder' => '0',
            'width' => '100%',
            'height' => '100%'
        ]) ?>
    </div>
<?= Html::endTag('div') ?>