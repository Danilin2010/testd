<?php

$MESS['C_S_VIDEO_FRAME_DEFAULT_SHADOW_USE'] = 'Использовать затемнение блока видео';
$MESS['C_S_VIDEO_FRAME_DEFAULT_SHADOW_COLOR'] = 'Цвет затемнения блока видео';
$MESS['C_S_VIDEO_FRAME_DEFAULT_SHADOW_COLOR_CUSTOM'] = 'Ваш цвет для затемнения блока видео';
$MESS['C_S_VIDEO_FRAME_DEFAULT_SHADOW_OPACITY'] = 'Прозрачность блока для затемнения видео (%)';

$MESS['C_S_VIDEO_FRAME_DEFAULT_SHADOW_COLOR_DEFAULT'] = 'Стандартный (черный)';
$MESS['C_S_VIDEO_FRAME_DEFAULT_SHADOW_COLOR_CUSTOM'] = 'Свой цвет';