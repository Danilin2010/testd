<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var $arCurrentValues
 */

$arTemplateParameters['SHADOW_USE'] = [
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_S_VIDEO_FRAME_DEFAULT_SHADOW_USE'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
];

if ($arCurrentValues['SHADOW_USE'] == 'Y') {
    $arTemplateParameters['SHADOW_COLOR'] = [
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_S_VIDEO_FRAME_DEFAULT_SHADOW_COLOR'),
        'TYPE' => 'LIST',
        'VALUES' => [
            'default' => Loc::getMessage('C_S_VIDEO_FRAME_DEFAULT_SHADOW_COLOR_DEFAULT'),
            'custom' => Loc::getMessage('C_S_VIDEO_FRAME_DEFAULT_SHADOW_COLOR_CUSTOM')
        ],
        'REFRESH' => 'Y'
    ];

    if ($arCurrentValues['SHADOW_COLOR'] == 'custom') {
        $arTemplateParameters['SHADOW_COLOR_CUSTOM'] = [
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_S_VIDEO_FRAME_DEFAULT_SHADOW_COLOR_CUSTOM'),
            'TYPE' => 'STRING'
        ];
    }

    $arTemplateParameters['SHADOW_OPACITY'] = [
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_S_VIDEO_FRAME_DEFAULT_SHADOW_OPACITY'),
        'TYPE' => 'STRING',
        'DEFAULT' => '50'
    ];
}