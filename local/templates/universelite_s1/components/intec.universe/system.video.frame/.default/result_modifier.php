<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arShadow = [
    'USE' => ArrayHelper::getValue($arParams, 'SHADOW_USE') == 'Y',
    'COLOR' => [
        'VALUE' => ArrayHelper::getValue($arParams, 'SHADOW_COLOR'),
        'CUSTOM' => ArrayHelper::getValue($arParams, 'SHADOW_COLOR_CUSTOM')
    ],
    'OPACITY' => ArrayHelper::getValue($arParams, 'SHADOW_OPACITY')
];

if ($arShadow['COLOR']['VALUE'] == 'custom' && empty($arShadow['COLOR']['CUSTOM'])) {
    $arShadow['COLOR']['VALUE'] = 'default';
    $arShadow['COLOR']['CUSTOM'] = null;
}

if (!empty($arShadow['OPACITY'])) {
    $arShadow['OPACITY'] = StringHelper::replace($arShadow['OPACITY'], ['%' => '']);
    $arShadow['OPACITY'] = 1 - ($arShadow['OPACITY'] / 100);
}

$arResult['VISUAL'] = [
    'SHADOW' => $arShadow
];