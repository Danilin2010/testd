<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$sTemplateId = spl_object_hash($this);

$this->setFrameMode(true);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arVisual = $arResult['VISUAL'];
$arFooter = $arResult['FOOTER_BLOCK'];

$bSliderUse = $arVisual['SLIDER']['USE'];
$bNoHeaderButtonTop = !$arHeader['SHOW'] && !$arDescription['SHOW'] && $arFooter['POSITION'] == 'top-right';
$bNoHeaderButtonTop = $bNoHeaderButtonTop ? ' no-header' : null;

?>
<div class="intec-content intec-content-visible">
    <div class="intec-content-wrapper">
        <div class="widget c-brands c-brands-template-1<?= $bNoHeaderButtonTop ?>" id="<?= $sTemplateId ?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [
                'class' => Html::cssClassFromArray([
                    'widget-content' => [
                        '' => true,
                        'slider clearfix' => $bSliderUse,
                        'grid' => !$bSliderUse
                    ]
                ], true)
            ]) ?>
                <?= Html::beginTag('div', [
                    'class' => Html::cssClassFromArray([
                        'widget-elements' => [
                            'wrapper' => true,
                            'slider' => $bSliderUse,
                            'with-dots' => $bSliderUse && $arVisual['SLIDER']['DOTS_USE'],
                            'with-arrows' => $bSliderUse && $arVisual['SLIDER']['ARROWS_USE']
                        ],'intec-grid' => [
                            '' => !$bSliderUse,
                            'wrap' => !$bSliderUse,
                            'a-v-start' => !$bSliderUse,
                            'a-h-center' => !$bSliderUse
                        ],
                        'owl-carousel' => $bSliderUse
                    ], true)
                ]) ?>
                    <?php foreach ($arResult['ITEMS'] as $arItem) {

                        $sId = $sTemplateId.'_'.$arItem['ID'];
                        $sAreaId = $this->GetEditAreaId($sId);
                        $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                        $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                        $sImage = $arItem['PREVIEW_PICTURE']['SRC'];
                        $sTag = 'div';

                        if ($arVisual['ELEMENT_LINK_USE']) $sTag = 'a';

                    ?>
                        <?= Html::beginTag('div', [
                            'class' => Html::cssClassFromArray([
                                'widget-element' => true,
                                'grid-'.$arVisual['LINE_COUNT'] => true,
                                'intec-grid-item' => [
                                    $arVisual['LINE_COUNT'] => !$bSliderUse,
                                    '1000-4' => !$bSliderUse && $arVisual['LINE_COUNT'] >= 5,
                                    '800-3' => !$bSliderUse && $arVisual['LINE_COUNT'] >= 4,
                                    '500-2' => !$bSliderUse && $arVisual['LINE_COUNT'] >= 3,
                                ]
                            ], true)
                        ]) ?>
                            <div class="widget-element-wrapper" id="<?= $sAreaId ?>">
                                <?= Html::tag($sTag, '', [
                                    'class' => 'widget-element-image',
                                    'style' => [
                                        'background-image' => 'url('.$sImage.')',
                                        'opacity' => $arVisual['ELEMENT_TRANSPARENT']
                                    ],
                                    'href' => $arVisual['ELEMENT_LINK_USE'] ? $arItem['DETAIL_PAGE_URL'] : null,
                                    'target' => $arVisual['ELEMENT_LINK_USE'] && $arVisual['ELEMENT_LINK_BLANK'] ? "_blank" : null
                                ]) ?>
                            </div>
                        <?= Html::endTag('div') ?>
                    <?php } ?>
                <?= Html::endTag('div') ?>
            <?= Html::endTag('div') ?>
            <?php if ($arFooter['SHOW']) { ?>
                <div class="widget-footer align-<?= $arFooter['POSITION'] ?>">
                    <a class="widget-footer-all" href="<?= $arFooter['LIST_PAGE'] ?>">
                        <span class="widget-footer-all-text">
                            <?= $arFooter['TEXT'] ?>
                        </span>
                        <svg class="widget-footer-all-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                            <path d="M216.464 36.465l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L387.887 239H12c-6.627 0-12 5.373-12 12v10c0 6.627 5.373 12 12 12h375.887L209.393 451.494c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l211.051-211.05c4.686-4.686 4.686-12.284 0-16.971L233.434 36.465c-4.686-4.687-12.284-4.687-16.97 0z" class=""></path>
                        </svg>
                    </a>
                </div>
            <?php } ?>
        </div>
        <?php if ($bSliderUse) include(__DIR__.'/script.php') ?>
    </div>
</div>