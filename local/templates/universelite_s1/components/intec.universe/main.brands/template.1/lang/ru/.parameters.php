<?php

$MESS['BRAND_TEMP1_ELEMENT_LINK_USE'] = 'Использовать ссылки на элементах';
$MESS['BRAND_TEMP1_ELEMENT_LINK_BLANK'] = 'Открывать ссылки в новой вкладке';
$MESS['BRAND_TEMP1_LINE_COUNT'] = 'Количество элементов в строке';
$MESS['BRAND_TEMP1_ELEMENT_TRANSPARENT'] = 'Степень прозрачности элементов, если на них не наведен курсор (%)';
$MESS['BRAND_TEMP1_SLIDER_USE'] = 'Использовать слайдер';
$MESS['BRAND_TEMP1_SLIDER_ARROWS_USE'] = 'Показывать навигационные стрелки';
$MESS['BRAND_TEMP1_SLIDER_DOTS_USE'] = 'Показывать навигационные точки';
$MESS['BRAND_TEMP1_SLIDER_LOOP_USE'] = 'Зациклить прокрутку слайдов';
$MESS['BRAND_TEMP1_SLIDER_AUTO_PLAY_USE'] = 'Использовать автопрокрутку слайдов';
$MESS['BRAND_TEMP1_SLIDER_AUTO_PLAY_TIME'] = 'Время задержки перед сменой слайда (мс)';
$MESS['BRAND_TEMP1_SLIDER_AUTO_PLAY_SPEED'] = 'Скорость анимации смены слайда при автопрокрутке слайда (мс)';
$MESS['BRAND_TEMP1_SLIDER_AUTO_PLAY_HOVER_PAUSE'] = 'Останавливать автопрокрутку при наведении курсора мыши на слайдер';
$MESS['BRAND_TEMP1_FOOTER_SHOW'] = 'Показывать кнопку "Показать все"';
$MESS['BRAND_TEMP1_FOOTER_POSITION'] = 'Расположение кнопки "Показать все"';
$MESS['BRAND_TEMP1_FOOTER_TEXT'] = 'Текст кнопки "Показать все"';

$MESS['BRAND_TEMP1_POSITION_LEFT'] = 'Слева';
$MESS['BRAND_TEMP1_POSITION_CENTER'] = 'По центру';
$MESS['BRAND_TEMP1_POSITION_RIGHT'] = 'Справа';
$MESS['BRAND_TEMP1_POSITION_TOP_RIGHT'] = 'Сверху справа';

$MESS['BRAND_TEMP1_FOOTER_TEXT_DEFAULT'] = 'Показать все';