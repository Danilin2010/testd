<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Параметры кнопки "Показать все" */
$sFooterText = ArrayHelper::getValue($arParams, 'FOOTER_TEXT');
$sFooterText = trim($sFooterText);
$sListPage = ArrayHelper::getValue($arParams, 'LIST_PAGE_URL');

if (!empty($sListPage)) {
    $sListPage = trim($sListPage);
    $sListPage = StringHelper::replaceMacros($sListPage, $arMacros);
} else {
    $sListPage = ArrayHelper::getFirstValue($arResult['ITEMS']);
    $sListPage = $sListPage['LIST_PAGE_URL'];
}

$bFooterShow = ArrayHelper::getValue($arParams, 'FOOTER_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sFooterText) && !empty($sListPage);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'FOOTER_POSITION'),
    'TEXT' => Html::encode($sFooterText),
    'LIST_PAGE' => $sListPage
];

if ($arResult['FOOTER_BLOCK']['POSITION'] == 'top-right') {
    $arResult['HEADER_BLOCK']['POSITION'] = $arResult['HEADER_BLOCK']['POSITION'].' right-button';
    $arResult['DESCRIPTION_BLOCK']['POSITION'] = $arResult['DESCRIPTION_BLOCK']['POSITION'].' right-button';
}

/** Параметры отображения шаблона */
$sOpacity = ArrayHelper::getValue($arParams, 'ELEMENT_TRANSPARENT');

if (!empty($sOpacity)) {
    $sOpacity = trim($sOpacity);
    $sOpacity = StringHelper::replace($sOpacity, ['%' => '']);

    if ($sOpacity > 100) {
        $sOpacity = 1;
    } else if ($sOpacity < 0) {
        $sOpacity = 0;
    } else {
        $sOpacity = 1 - ($sOpacity / 100);
    }
} else {
    $sOpacity = 1;
}

$iLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($iLineCount < 3)
    $iLineCount = 3;

if ($iLineCount > 5)
    $iLineCount = 5;

$arResult['VISUAL'] = [
    'ELEMENT_LINK_USE' => ArrayHelper::getValue($arParams, 'ELEMENT_LINK_USE') == 'Y',
    'ELEMENT_LINK_BLANK' => ArrayHelper::getValue($arParams, 'ELEMENT_LINK_BLANK') == 'Y',
    'ELEMENT_TRANSPARENT' => $sOpacity,
    'LINE_COUNT' => $iLineCount,
    'SLIDER' => [
        'USE' => ArrayHelper::getValue($arParams, 'SLIDER_USE') == 'Y',
        'ITEMS' => $iLineCount,
        'ARROWS_USE' => ArrayHelper::getValue($arParams, 'SLIDER_ARROWS_USE') == 'Y',
        'DOTS_USE' => ArrayHelper::getValue($arParams, 'SLIDER_DOTS_USE') == 'Y',
        'LOOP_USE' => ArrayHelper::getValue($arParams, 'SLIDER_LOOP_USE') == 'Y',
        'AUTO_PLAY_USE' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_PLAY_USE') == 'Y',
        'AUTO_PLAY_TIME' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_PLAY_TIME'),
        'AUTO_PLAY_SPEED' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_PLAY_SPEED'),
        'AUTO_PLAY_HOVER_PAUSE' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_PLAY_HOVER_PAUSE') == 'Y'
    ]

];

/** NO IMAGE */
$sNoImage = SITE_TEMPLATE_PATH.'/images/no-img/noimg_original.jpg';

/** Обработка данных */
foreach ($arResult['ITEMS'] as $iKey => $arItem) {
    /** Обработка картинок */
    $arPreviewPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');
    $arDetailPicture = ArrayHelper::getValue($arItem, 'DETAIL_PICTURE');

    if (empty($arPreviewPicture) && !empty($arDetailPicture)) {
        $arPreviewPicture = $arDetailPicture;
    }

    if (!empty($arPreviewPicture)) {
        $arPicture = CFile::ResizeImageGet(
            $arPreviewPicture,
            array(
                'width' => 400,
                'height' => 400
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT
        );

        $arItem['PREVIEW_PICTURE']['SRC'] = $arPicture['src'];
    } else {
        $arPreviewPicture['SRC'] = $sNoImage;
    }

    $arResult['ITEMS'][$iKey] = $arItem;
}