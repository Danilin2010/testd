<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

$arTemplateParameters['ELEMENT_LINK_USE'] = array(
    'PARENT' => 'BASE',
    'NAME' => Loc::getMessage('BRAND_TEMP1_ELEMENT_LINK_USE'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['ELEMENT_LINK_USE'] == 'Y') {
    $arTemplateParameters['ELEMENT_LINK_BLANK'] = array(
        'PARENT' => 'BASE',
        'NAME' => Loc::getMessage('BRAND_TEMP1_ELEMENT_LINK_BLANK'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N'
    );
}
$arTemplateParameters['LINE_COUNT'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('BRAND_TEMP1_LINE_COUNT'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        3 => '3',
        4 => '4',
        5 => '5'
    ),
    'DEFAULT' => '4'
);
$arTemplateParameters['ELEMENT_TRANSPARENT'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('BRAND_TEMP1_ELEMENT_TRANSPARENT'),
    'TYPE' => 'STRING'
);
$arTemplateParameters['SLIDER_USE'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('BRAND_TEMP1_SLIDER_USE'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['SLIDER_USE'] == 'Y') {
    $arTemplateParameters['SLIDER_ARROWS_USE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('BRAND_TEMP1_SLIDER_ARROWS_USE'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N'
    );
    $arTemplateParameters['SLIDER_DOTS_USE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('BRAND_TEMP1_SLIDER_DOTS_USE'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N'
    );
    $arTemplateParameters['SLIDER_LOOP_USE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('BRAND_TEMP1_SLIDER_LOOP_USE'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N'
    );
    $arTemplateParameters['SLIDER_AUTO_PLAY_USE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('BRAND_TEMP1_SLIDER_AUTO_PLAY_USE'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N',
        'REFRESH' => 'Y'
    );

    if ($arCurrentValues['SLIDER_AUTO_PLAY_USE'] == 'Y') {
        $arTemplateParameters['SLIDER_AUTO_PLAY_TIME'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('BRAND_TEMP1_SLIDER_AUTO_PLAY_TIME'),
            'TYPE' => 'STRING',
            'DEFAULT' => '10000'
        );
        $arTemplateParameters['SLIDER_AUTO_PLAY_SPEED'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('BRAND_TEMP1_SLIDER_AUTO_PLAY_SPEED'),
            'TYPE' => 'STRING',
            'DEFAULT' => '500'
        );
        $arTemplateParameters['SLIDER_AUTO_PLAY_HOVER_PAUSE'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('BRAND_TEMP1_SLIDER_AUTO_PLAY_HOVER_PAUSE'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N'
        );
    }
}

$arTemplateParameters['FOOTER_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('BRAND_TEMP1_FOOTER_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['FOOTER_SHOW'] == 'Y') {
    $arTemplateParameters['FOOTER_POSITION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('BRAND_TEMP1_FOOTER_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'left' => Loc::getMessage('BRAND_TEMP1_POSITION_LEFT'),
            'center' => Loc::getMessage('BRAND_TEMP1_POSITION_CENTER'),
            'right' => Loc::getMessage('BRAND_TEMP1_POSITION_RIGHT'),
            'top-right' => Loc::getMessage('BRAND_TEMP1_POSITION_TOP_RIGHT')
        ),
        'DEFAULT' => 'center'
    );
    $arTemplateParameters['FOOTER_TEXT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('BRAND_TEMP1_FOOTER_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('BRAND_TEMP1_FOOTER_TEXT_DEFAULT')
    );
}