<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use Bitrix\Main\Localization\Loc;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 */

$sTemplateId = spl_object_hash($this);

$this->setFrameMode(true);

$bHeaderShow = ArrayHelper::getValue($arResult, ['HEADER_BLOCK', 'SHOW']);
$sHeaderPosition = ArrayHelper::getValue($arResult, ['HEADER_BLOCK', 'POSITION']);
$sHeaderText = ArrayHelper::getValue($arResult, ['HEADER_BLOCK', 'TEXT']);

$bDescriptionShow = ArrayHelper::getValue($arResult, ['DESCRIPTION_BLOCK', 'SHOW']);
$sDescriptionPosition = ArrayHelper::getValue($arResult, ['DESCRIPTION_BLOCK', 'POSITION']);
$sDescriptionText = ArrayHelper::getValue($arResult, ['DESCRIPTION_BLOCK', 'TEXT']);

$iElementsRowCount = ArrayHelper::getValue($arResult, ['ELEMENTS_ROW_COUNT']);;

?>
<div class="intec-content intec-content-visible">
    <div class="intec-content-wrapper">
        <div class="widget c-advantages c-advantages-template-1" id="<?= $sTemplateId ?>">
            <?php if ($bHeaderShow || $bDescriptionShow) { ?>
                <div class="widget-header">
                    <?php if ($bHeaderShow && !empty($sHeaderText)) { ?>
                        <div class="widget-title align-<?= $sHeaderPosition ?>">
                            <?= Html::encode($sHeaderText) ?>
                        </div>
                    <?php } ?>
                    <?php if ($bDescriptionShow && !empty($sDescriptionText)) { ?>
                        <div class="widget-description align-<?= $sDescriptionPosition ?>">
                            <?= Html::encode($sDescriptionText) ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="widget-content">
                <div class="widget-elements advantages-elements-row-count-<?= $iElementsRowCount ?>">
                    <div class="widget-elements-wrapper">
                        <?php foreach ($arResult['ITEMS'] as $arItem) {
                            $sId = $sTemplateId.'_'.$arItem['ID'];
                            $sAreaId = $this->GetEditAreaId($sId);
                            $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                            $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                            $sName = $arItem['NAME'];
                            $sImage = !empty($arItem['PREVIEW_PICTURE']) ? $arItem['PREVIEW_PICTURE']['SRC'] : null;

                            if (!empty($arItem['PREVIEW_PICTURE'])) {
                                $sImage = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], [
                                    'width' => 40,
                                    'height' => 40
                                ], BX_RESIZE_IMAGE_PROPORTIONAL_ALT);

                                if (!empty($sImage)) {
                                    $sImage = $sImage['src'];
                                } else {
                                    $sImage = null;
                                }
                            }

                            $sDescription = $arItem['PREVIEW_TEXT'];
                        ?>
                            <div class="widget-element<?= empty($sImage) ? ' widget-element-without-image' : null ?>">
                                <div class="widget-element-wrapper" id="<?= $sAreaId ?>">
                                    <?php if (!empty($sImage)) { ?>
                                        <div class="advantages-image">
                                            <img src="<?=$sImage?>" />
                                        </div>
                                    <?php } ?>
                                    <div class="widget-element-name intec-cl-text-hover">
                                        <?= $sName ?>
                                        <? if (!empty($sDescription)) { ?>
                                            <div class="widget-element-description">
                                                <?= Html::encode($sDescription) ?>
                                            </div>
                                        <? } ?>
                                    </div>
                                    <div class="intec-clearfix"></div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                     </div>
                 </div>
            </div>
         </div>
    </div>
</div>
