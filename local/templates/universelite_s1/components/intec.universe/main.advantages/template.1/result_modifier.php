<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\Type;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arResult['VIEW_PARAMETERS'] = [

];

$arResult['ELEMENTS_ROW_COUNT'] = Type::toInteger($arParams['ELEMENTS_ROW_COUNT']);

if ($arResult['ELEMENTS_ROW_COUNT'] < 2)
    $arResult['ELEMENTS_ROW_COUNT'] = 2;

if ($arResult['ELEMENTS_ROW_COUNT'] > 4)
    $arResult['ELEMENTS_ROW_COUNT'] = 4;