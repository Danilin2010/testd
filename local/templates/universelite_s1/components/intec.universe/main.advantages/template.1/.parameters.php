<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!Loader::includeModule('iblock'))
    return;

/**
 * @var array $arCurrentValues
 */

$arTemplateParameters = array(
    'ELEMENTS_ROW_COUNT' => array(
        'PARENT' => 'BASE',
        'NAME' => Loc::getMessage('C_ADVANTAGES_TEMP1_ELEMENTS_ROW_COUNT'),
        'TYPE' => 'LIST',
        'DEFAULT' => 4,
        'VALUES' => array(
            2 => 2,
            3 => 3,
            4 => 4
        )
    )
);

