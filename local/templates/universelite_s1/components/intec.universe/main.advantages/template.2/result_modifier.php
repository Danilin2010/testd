<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Type;

/**
 * @var array $arResult
 * @var array $arParams
 */

$sTemplateIcon = ArrayHelper::getValue($arParams, 'VIEW');
$sLineCount = Type::toInteger($arParams['LINE_COUNT']);

if ($sLineCount <= 2)
    $sLineCount = 2;

if ($sLineCount >= 5)
    $sLineCount = 5;

$arResult['VISUAL'] = [
    'LINE_COUNT' => $sLineCount,
    'VIEW' => $sTemplateIcon
];

if ($sTemplateIcon == 'icon') {
    foreach ($arResult['ITEMS'] as $key => $arItem) {
        $arPreviewPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');
        $arDetailPicture = ArrayHelper::getValue($arItem, 'DETAIL_PICTURE');
        $sNoImg = SITE_TEMPLATE_PATH.'/images/noimg/noimg_minquadro.jpg';

        if (empty($arPreviewPicture) && !empty($arDetailPicture))
            $arPreviewPicture = $arDetailPicture;

        if (!empty($arPreviewPicture)) {
            $arResizeImage = CFile::ResizeImageGet(
                $arItem['PREVIEW_PICTURE'],
                array(
                    'width'=>80,
                    'height'=>80
                ),
                BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                true
            );

            $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] = $arResizeImage['src'];
        } else {
            $arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] = $sNoImg;
        }
    }
}
