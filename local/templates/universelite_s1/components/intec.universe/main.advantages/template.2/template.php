<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arVisual = $arResult['VISUAL'];

$iCount = 0;

?>
<div class="intec-content intec-content-visible">
    <div class="intec-content-wrapper">
        <div class="widget c-advantages c-advantages-template-2" id="<?= $sTemplateId ?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [
                'class' => [
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-center'
                    ]
                ]
            ]) ?>
                <?php foreach ($arResult ['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = $arItem['NAME'];
                    $sDescription = $arItem['PREVIEW_TEXT'];
                    $sIcon = $arItem['PREVIEW_PICTURE']['SRC'];

                    $iCount++;

                ?>
                    <?= Html::beginTag('div', [
                        'class' => Html::cssClassFromArray([
                            'widget-element' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1150-4' => $arVisual['LINE_COUNT'] >= 5,
                                '900-3' => $arVisual['LINE_COUNT'] >= 4,
                                '750-2' => $arVisual['LINE_COUNT'] >= 3,
                                '500-1' => $arVisual['LINE_COUNT'] >= 2
                            ]
                        ], true)
                    ]) ?>
                        <div class="widget-element-wrapper" id="<?= $sAreaId ?>">
                            <?php if ($arVisual['VIEW'] == 'icon') { ?>
                                <?= Html::tag('div', '', [
                                    'class' => 'widget-element-icon',
                                    'style' => ['background-image' => 'url('.$sIcon.')']
                                ]) ?>
                            <?php } else { ?>
                                <div class="widget-element-counter">
                                    <?= $iCount.'.' ?>
                                </div>
                            <?php } ?>
                            <div class="widget-element-name">
                                <?= $sName ?>
                            </div>
                            <div class="widget-element-description">
                                <?= $sDescription ?>
                            </div>
                        </div>
                    <?= Html::endTag('div') ?>
                <? } ?>
            <?= Html::endTag('div') ?>
        </div>
    </div>
</div>

