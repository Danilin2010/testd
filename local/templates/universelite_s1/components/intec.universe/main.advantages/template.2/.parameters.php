<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!Loader::includeModule('iblock'))
    return;

/**
 * @var array $arCurrentValues
 */

$arTemplateParameters = array(
    'LINE_COUNT' => array(
        'PARENT' => 'BASE',
        'NAME' => Loc::getMessage('C_ADVANTAGES_TEMP2_LINE_COUNT'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5'
        ),
        'DEFAULT' => 4
    )
);

$arTemplateParameters['VIEW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_ADVANTAGES_TEMP2_VIEW'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        'number' => Loc::getMessage('C_ADVANTAGES_TEMP2_VIEW_NUMBER'),
        'icon' => Loc::getMessage('C_ADVANTAGES_TEMP2_VIEW_ICON')
    ),
    'DEFAULT' => 'number'
);