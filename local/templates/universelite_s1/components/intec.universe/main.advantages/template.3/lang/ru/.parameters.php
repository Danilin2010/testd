<?php
$MESS['C_ADVANTAGES_TEMP3_BACKGROUND_SIZE'] = 'Размер изображения';
$MESS['C_ADVANTAGES_TEMP3_ARROW_SHOW'] = 'Отображать стрелки с края текста';

$MESS['C_ADVANTAGES_TEMP3_BACKGROUND_SIZE_COVER'] = 'Растянуть пропорционально';
$MESS['C_ADVANTAGES_TEMP3_BACKGROUND_SIZE_CONTAIN'] = 'Уместить';