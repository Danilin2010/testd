<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$sTemplateId = spl_object_hash($this);

$this->setFrameMode(true);

$arVisual = $arResult['VISUAL'];

$bHeaderShow = ArrayHelper::getValue($arResult, ['HEADER_BLOCK', 'SHOW']);
$sHeaderPosition = ArrayHelper::getValue($arResult, ['HEADER_BLOCK', 'POSITION']);
$sHeaderText = ArrayHelper::getValue($arResult, ['HEADER_BLOCK', 'TEXT']);

$bDescriptionShow = ArrayHelper::getValue($arResult, ['DESCRIPTION_BLOCK', 'SHOW']);
$sDescriptionPosition = ArrayHelper::getValue($arResult, ['DESCRIPTION_BLOCK', 'POSITION']);
$sDescriptionText = ArrayHelper::getValue($arResult, ['DESCRIPTION_BLOCK', 'TEXT']);

?>
<div class="intec-content intec-content-visible">
    <div class="intec-content-wrapper">
        <div class="widget c-advantages c-advantages-template-3" id="<?= $sTemplateId ?>">
            <?php if ($bHeaderShow || $bDescriptionShow) { ?>
                <div class="widget-header">
                    <?php if ($bHeaderShow && !empty($sHeaderText)) { ?>
                        <div class="widget-title align-<?= $sHeaderPosition ?>">
                            <?= Html::encode($sHeaderText) ?>
                        </div>
                    <?php } ?>
                    <?php if ($bDescriptionShow && !empty($sDescriptionText)) { ?>
                        <div class="widget-description align-<?= $sDescriptionPosition ?>">
                            <?= Html::encode($sDescriptionText) ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="widget-content">
                <div class="c-widget-elements">
                    <div class="c-widget-elements-wrapper">
                        <?php $iCounter = 0; ?>
                        <?php foreach ($arResult['ITEMS'] as $arItem) {
                            $sId = $sTemplateId.'_'.$arItem['ID'];
                            $sAreaId = $this->GetEditAreaId($sId);
                            $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                            $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                            $sName = $arItem['NAME'];
                            $sImage = !empty($arItem['PREVIEW_PICTURE']) ? $arItem['PREVIEW_PICTURE']['SRC'] : null;

                            if (!empty($arItem['PREVIEW_PICTURE'])) {
                                $sImage = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], [
                                    'max-width' => 400,
                                    'max-height' => 400
                                ], BX_RESIZE_IMAGE_PROPORTIONAL_ALT);

                                if (!empty($sImage)) {
                                    $sImage = $sImage['src'];
                                } else {
                                    $sImage = null;
                                }
                            }

                            $sDescription = $arItem['PREVIEW_TEXT'];
                            $iCounter++;
                            $bLeft = $iCounter % 2 == 1;
                            ?>
                                <div class="widget-element<?= $bLeft ? ' widget-element-left' : null ?>" id="<?= $sAreaId ?>">
                                    <div class="widget-element-wrapper">
                                        <?php if ($bLeft) { ?>
                                            <div class="widget-element-content">
                                                <div class="widget-element-content-aligner"></div>
                                                <div class="widget-element-content-wrapper">
                                                    <div class="widget-element-name">
                                                        <?= Html::encode($sName) ?>
                                                    </div>
                                                    <div class="widget-element-line"></div>
                                                    <?php if (!empty($sDescription)) { ?>
                                                        <div class="widget-element-description">
                                                            <?= Html::encode($sDescription) ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?= Html::tag('div', '', [
                                                'class' => 'widget-element-image',
                                                'style' => [
                                                    'background-image' => !empty($sImage) ? 'url('.$sImage.')' : null,
                                                    'background-size' => $arVisual['BACKGROUND']['SIZE']
                                                ]
                                            ]) ?>
                                            <?php if ($arVisual['ARROW']['SHOW']) { ?>
                                                <div class="widget-element-arrow"></div>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <?= Html::tag('div', '', [
                                                'class' => 'widget-element-image',
                                                'style' => [
                                                    'background-image' => !empty($sImage) ? 'url('.$sImage.')' : null,
                                                    'background-size' => $arVisual['BACKGROUND']['SIZE']
                                                ]
                                            ]) ?>
                                            <div class="widget-element-content">
                                                <div class="widget-element-content-aligner"></div>
                                                <div class="widget-element-content-wrapper">
                                                    <div class="widget-element-name">
                                                        <?= Html::encode($sName) ?>
                                                    </div>
                                                    <div class="widget-element-line"></div>
                                                    <?php if (!empty($sDescription)) { ?>
                                                        <div class="widget-element-description">
                                                            <?= Html::encode($sDescription) ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php if ($arVisual['ARROW']['SHOW']) { ?>
                                                <div class="widget-element-arrow"></div>
                                            <?php } ?>
                                        <?php } ?>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
