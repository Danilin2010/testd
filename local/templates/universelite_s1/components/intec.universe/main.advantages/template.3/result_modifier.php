<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;

/**
 * @var array $arParams
 * @var array $arResult
 */

$sBgSize = ArrayHelper::getValue($arParams, 'BACKGROUND_SIZE');
$sBgSize = ArrayHelper::fromRange(['cover', 'contain'], $sBgSize);

$arResult['VISUAL'] = [
    'BACKGROUND' => [
        'SIZE' => $sBgSize
    ],
    'ARROW' => [
        'SHOW' => ArrayHelper::getValue($arParams, 'ARROW_SHOW') == 'Y'
    ]
];