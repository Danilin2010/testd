<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use Bitrix\Main\Localization\Loc;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arVisual = $arResult['VISUAL'];

$iCount = 0;

?>
<div class="intec-content">
    <div class="intec-content-wrapper">
        <div class="widget c-stages c-stages-template-2" id="<?= $sTemplateId ?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [
                'class' => [
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-stretch',
                        'a-h-center',
                        'i-h-20'
                    ]
                ]
            ]) ?>
                <?php foreach ($arResult ['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = $arItem['NAME'];
                    $sDescription = $arItem['PREVIEW_TEXT'];
                    $iCount++;

                ?>
                    <?= Html::beginTag('div', [
                        'class' => Html::cssClassFromArray([
                            'widget-element' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1150-3' => $arVisual['LINE_COUNT'] >= 4,
                                '850-2' => $arVisual['LINE_COUNT'] >= 3,
                                '600-1' => $arVisual['LINE_COUNT'] >= 2
                            ]
                        ], true)
                    ]) ?>
                        <div class="widget-element-wrapper" id="<?= $sAreaId ?>">
                            <div class="widget-element-number">
                                <?= $iCount ?>
                            </div>
                            <div class="widget-element-text">
                                <?= $sName ?>
                                <?php if ($arVisual['ELEMENT_SHOW_DESCRIPTION'] && !empty($sDescription)) { ?>
                                    <?= ' - '.$sDescription ?>
                                <?php } ?>
                            </div>
                        </div>
                    <?= Html::endTag('div') ?>
                <? } ?>
            <?= Html::endTag('div') ?>
        </div>
    </div>
</div>


