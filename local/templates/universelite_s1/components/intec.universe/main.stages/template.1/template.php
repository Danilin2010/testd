<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arVisual = $arResult['VISUAL'];

$iCount = 0;
$iSideCounter = 0;

?>
<div class="widget c-stages c-stages-template-1" id="<?= $sTemplateId ?>">
    <div class="intec-content">
        <div class="intec-content-wrapper">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="widget-content">
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = $arItem['NAME'];
                    $sDescription = $arItem['PREVIEW_TEXT'];
                    $iCount++;
                    $iSideCounter++;
                    $bLeft = $iSideCounter % 2 == 1;

                ?>
                    <?= Html::beginTag('div', [
                        'class' => Html::cssClassFromArray([
                            'widget-element' => true,
                            'widget-element-left' => $bLeft,
                            'widget-element-right' => !$bLeft
                        ], true)
                    ]) ?>
                        <div class="widget-element-wrapper" id="<?= $sAreaId ?>">
                            <?php if ($arVisual['COUNT_SHOW']) { ?>
                                <div class="widget-element-number">
                                    <?= $iCount.'.' ?>
                                </div>
                            <?php } ?>
                            <div class="widget-element-name">
                                <?= $sName ?>
                            </div>
                            <?php if ($arVisual['ELEMENT_DESCRIPTION_SHOW'] && !empty($sDescription)) { ?>
                                <div class="widget-element-description">
                                    <?= $sDescription ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
