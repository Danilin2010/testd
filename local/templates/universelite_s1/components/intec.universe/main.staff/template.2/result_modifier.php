<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

$sButtonLink = ArrayHelper::getValue($arParams, 'BUTTON_LINK');
$sButtonLink = trim($sButtonLink);
$sButtonLink = StringHelper::replaceMacros($sButtonLink, $arMacros);
$bButtonShow = ArrayHelper::getValue($arParams, 'BUTTON_SHOW') == 'Y';
$bButtonShow = $bButtonShow && !empty($sButtonLink);
$iLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($iLineCount <= 3)
    $iLineCount = 3;

if ($iLineCount >= 5)
    $iLineCount = 5;

$arResult['VISUAL'] = [
    'LINE_COUNT' => $iLineCount,
    'BUTTON' => [
        'SHOW' => $bButtonShow,
        'LINK' => $sButtonLink
    ]
];

foreach ($arResult['ITEMS'] as $iKey => $arItem) {
    /** Обработка кантинок */
    $arPreviewPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');
    $arDetailPicture = ArrayHelper::getValue($arItem, 'DETAIL_PICTURE');
    $sNoImg = SITE_TEMPLATE_PATH.'/images/noimg/noimg_minquadro.jpg';

    if (empty($arPreviewPicture) && !empty($arDetailPicture))
        $arPreviewPicture = $arDetailPicture;

    if (!empty($arPreviewPicture)) {
        $arPicture = CFile::ResizeImageGet(
            $arPreviewPicture,
            array(
                'width' => 350,
                'height' => 350
            ),
            BX_RESIZE_IMAGE_EXACT
        );

        $arPreviewPicture['SRC'] = $arPicture['src'];
    } else {
        $arPreviewPicture['SRC'] = $sNoImg;
    }

    $arItem['PREVIEW_PICTURE']['SRC'] = $arPreviewPicture['SRC'];

    $arResult['ITEMS'][$iKey] = $arItem;
}