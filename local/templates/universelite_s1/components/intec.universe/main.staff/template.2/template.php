<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use Bitrix\Main\Localization\Loc;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arVisual = $arResult['VISUAL'];

?>
<div class="intec-content intec-content-visible">
    <div class="intec-content-wrapper">
        <div class="widget c-staff c-staff-template-2" id="<?= $sTemplateId ?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [ /** Контентный блок */
                'class' => [
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-center'
                    ]
                ]
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = ArrayHelper::getValue($arItem, 'NAME');
                    $sImage = ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']);

                ?>
                    <?= Html::beginTag('div', [
                        'class' => Html::cssClassFromArray([ /** Главный тег элемента */
                            'widget-element' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1000-4' => $arVisual['LINE_COUNT'] >= 5,
                                '800-3' => $arVisual['LINE_COUNT'] >= 4,
                                '600-2' => $arVisual['LINE_COUNT'] >= 3,
                                '400-1' => true
                            ]
                        ], true)
                    ]) ?>
                        <div class="widget-element-wrapper" id="<?= $sAreaId ?>">
                            <div class="widget-element-image-wrap" >
                                <?= Html::tag('div', '', [ /** Картинка элемента */
                                    'class' => 'widget-element-image',
                                    'style' => [
                                        'background-image' => 'url('.$sImage.')'
                                    ]
                                ]) ?>
                            </div>
                            <div class="widget-element-name">
                                <?= $sName ?>
                            </div>
                        </div>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
            <?php if ($arVisual['BUTTON']['SHOW']) { ?>
                <div class="widget-button">
                    <a class="widget-button-wrapper intec-cl-text-hover" href="<?= $arVisual['BUTTON']['LINK'] ?>">
                        <div class="widget-button-text">
                            <div class="widget-button-name">
                                <?= Loc::getMessage('C_STAFF_TEMP2_BUTTON_NAME') ?>
                            </div>
                            <div class="widget-button-description">
                                <?= Loc::getMessage('C_STAFF_TEMP2_BUTTON_SECTION') ?>
                            </div>
                        </div>
                        <div class="widget-button-icon">
                            <i class="fal fa-arrow-right"></i>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
