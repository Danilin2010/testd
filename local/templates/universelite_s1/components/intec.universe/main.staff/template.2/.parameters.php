<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

if (!empty($arCurrentValues['IBLOCK_ID'])) {
    $arTemplateParameters['LINE_COUNT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_STAFF_TEMP2_LINE_COUNT'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            3 => '3',
            4 => '4',
            5 => '5'
        ),
        'DEFAULT' => 4
    );
    $arTemplateParameters ['BUTTON_SHOW'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_STAFF_TEMP2_BUTTON_SHOW'),
        'TYPE' => 'CHECKBOX',
        'REFRESH' => 'Y'
    );

    if ($arCurrentValues['BUTTON_SHOW'] == 'Y') {
        $arTemplateParameters ['BUTTON_LINK'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_STAFF_TEMP2_BUTTON_LINK'),
            'TYPE' => 'STRING'
        );
    }
}