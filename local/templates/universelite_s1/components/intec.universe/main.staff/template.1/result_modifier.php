<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

$sPropertyPosition = ArrayHelper::getValue($arParams, 'PROPERTY_POSITION');
$bPositionShow = ArrayHelper::getValue($arParams, 'POSITION_SHOW') == 'Y';
$bPositionShow = $bPositionShow && !empty($sPropertyPosition);
$iLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($iLineCount <= 2)
    $iLineCount = 2;

if ($iLineCount >= 5)
    $iLineCount = 5;

$arResult['VISUAL'] = [
    'SOCIAL_SHOW' => ArrayHelper::getValue($arParams, 'SOCIAL_SHOW') == 'Y',
    'LINE_COUNT' => $iLineCount,
    'POSITION_SHOW' => $bPositionShow
];

$arResult['PROPERTY_CODES'] = [
    'SOCIAL' => [
        'VK' => ArrayHelper::getValue($arParams, 'PROPERTY_VK'),
        'FACEBOOK' => ArrayHelper::getValue($arParams, 'PROPERTY_FACEBOOK'),
        'INSTAGRAM' => ArrayHelper::getValue($arParams, 'PROPERTY_INSTAGRAM'),
        'TWITTER' => ArrayHelper::getValue($arParams, 'PROPERTY_TWITTER')
    ],
    'POSITION' => $sPropertyPosition
];

foreach ($arResult['ITEMS'] as $iKey => $arItem) {
    /** Обработка свойств соц. сетей */
    foreach ($arResult['PROPERTY_CODES']['SOCIAL'] as $sCode) {
        $arProperty = ArrayHelper::getValue($arItem, ['PROPERTIES', $sCode]);

        if (!empty($arProperty) && !empty($arProperty['VALUE'])) {
            $arProperty['VALUE'] = StringHelper::replaceMacros($arProperty['VALUE'], $arMacros);

            $arItem['PROPERTIES'][$sCode]['VALUE'] = $arProperty['VALUE'];
        }
    }

    /** Обработка картинок */
    $arPreviewPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');
    $arDetailPicture = ArrayHelper::getValue($arItem, 'DETAIL_PICTURE');
    $sNoImg = SITE_TEMPLATE_PATH.'/images/noimg/noimg_minquadro.jpg';

    if (empty($arPreviewPicture) && !empty($arDetailPicture))
        $arPreviewPicture = $arDetailPicture;

    if (!empty($arPreviewPicture)) {
        $arPicture = CFile::ResizeImageGet(
            $arPreviewPicture,
            array(
                'width' => 250,
                'height' => 250
            ),
            BX_RESIZE_IMAGE_EXACT
        );

        $arPreviewPicture['SRC'] = $arPicture['src'];
    } else {
        $arPreviewPicture['SRC'] = $sNoImg;
    }

    $arItem['PREVIEW_PICTURE']['SRC'] = $arPreviewPicture['SRC'];

    $arResult['ITEMS'][$iKey] = $arItem;
}