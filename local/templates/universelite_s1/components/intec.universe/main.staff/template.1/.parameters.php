<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

if (!Loader::includeModule('iblock'))
    return;

$arPropertiesText = [];

if (!empty($arCurrentValues['IBLOCK_ID'])) {
    $rsProperties = CIBlockProperty::GetList(
        array(),
        array(
            'IBLOCK_ID' => $arCurrentValues['IBLOCK_ID']
        )
    );

    while ($arProperty = $rsProperties->Fetch()) {
        if ($arProperty['PROPERTY_TYPE'] == 'S' && $arProperty['LIST_TYPE'] == 'L') {
            $arPropertiesText[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        }
    }

    $arTemplateParameters['PROPERTY_POSITION'] = array (
        'PARENT' => 'DATA_SOURCE',
        'NAME' => Loc::getMessage('C_STAFF_TEMP1_PROPERTY_POSITION'),
        'TYPE' => 'LIST',
        'ADDITIONAL_VALUES' => 'Y',
        'VALUES' => $arPropertiesText,
        'REFRESH' => 'Y'
    );
    $arTemplateParameters ['SOCIAL_SHOW'] = array (
        'PARENT' => 'BASE',
        'NAME' => Loc::getMessage('C_STAFF_TEMP1_SOCIAL_SHOW'),
        'TYPE' => 'CHECKBOX',
        'ADDITIONAL_VALUES' => 'Y',
        'REFRESH' => 'Y'
    );

    if ($arCurrentValues['SOCIAL_SHOW'] == 'Y') {
        $arTemplateParameters['PROPERTY_VK'] = array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_STAFF_TEMP1_PROPERTY_VK'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        );
        $arTemplateParameters['PROPERTY_FACEBOOK'] = array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_STAFF_TEMP1_PROPERTY_FACEBOOK'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        );
        $arTemplateParameters['PROPERTY_INSTAGRAM'] = array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_STAFF_TEMP1_PROPERTY_INSTAGRAM'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        );
        $arTemplateParameters['PROPERTY_TWITTER'] = array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_STAFF_TEMP1_PROPERTY_TWITTER'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        );
    }
}

$arTemplateParameters['LINE_COUNT'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_STAFF_TEMP1_LINE_COUNT'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        3 => '3',
        4 => '4',
        5 => '5',
    ),
    'DEFAULT' => 4
);

if (!empty($arCurrentValues['PROPERTY_POSITION'])) {
    $arTemplateParameters['POSITION_SHOW'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_STAFF_TEMP1_POSITION_SHOW'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N'
    );
}