<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
* @var array $arResult
*/

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arVisual = $arResult['VISUAL'];
$arCodes = $arResult['PROPERTY_CODES'];

?>
<div class="intec-content">
    <div class="intec-content-wrapper">
        <div class="widget c-staff c-staff-template-1" id="<?= $sTemplateId ?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [ /** Контентный блок */
                'class' => [
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-center'
                    ]
                ]
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = ArrayHelper::getValue($arItem, 'NAME');
                    $sImage = ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']);
                    $sPosition = ArrayHelper::getValue($arItem,['PROPERTIES', $arCodes['POSITION'], 'VALUE']);
                    $arSocial = [
                        'VK' => [
                            'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['SOCIAL']['VK'], 'VALUE']),
                            'ICON' => 'vk'
                        ],
                        'FACEBOOK' => [
                            'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['SOCIAL']['FACEBOOK'], 'VALUE']),
                            'ICON' => 'facebook-f'
                        ],
                        'INSTAGRAM' => [
                            'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['SOCIAL']['INSTAGRAM'], 'VALUE']),
                            'ICON' => 'instagram'
                        ],
                        'TWITTER' => [
                            'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['SOCIAL']['TWITTER'], 'VALUE']),
                            'ICON' => 'twitter'
                        ]
                    ];

                ?>
                    <?= Html::beginTag('div', [ /** Главный тег элемента */
                        'class' => Html::cssClassFromArray([
                            'widget-element' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1150-4' => $arVisual['LINE_COUNT'] >= 5,
                                '950-3' => $arVisual['LINE_COUNT'] >= 4,
                                '700-2' => $arVisual['LINE_COUNT'] >= 3,
                                '500-1' => true
                            ]
                        ], true)
                    ]) ?>
                        <div class="widget-element-wrapper" id="<?= $sAreaId ?>">
                            <div class="widget-element-image-wrap">
                                <?= Html::tag('div', '', [ /** Картинка элемента */
                                    'class' => 'widget-element-image',
                                    'style' => [
                                        'background-image' => 'url('.$sImage.')'
                                    ]
                                ]) ?>
                            </div>
                            <div class="widget-element-name intec-cl-text">
                                <?= $sName ?>
                            </div>
                            <?php if ($arVisual['POSITION_SHOW'] && !empty($sPosition)) { ?>
                                <div class="widget-element-position">
                                    <?= $sPosition ?>
                                </div>
                            <?php } ?>
                            <?php if ($arVisual['SOCIAL_SHOW']) { ?>
                                <?= Html::beginTag('div', [ /** Контейнер для иконок соц. сетей */
                                    'class' => [
                                        'widget-element-icons',
                                        'intec-grid' => [
                                            '',
                                            'wrap',
                                            'a-v-start',
                                            'a-h-center'
                                        ]
                                    ]
                                ]) ?>
                                    <?php foreach ($arSocial as $iKey => $arElement) { ?>
                                        <?php if (!empty($arElement['VALUE'])) { ?>
                                            <div class="widget-element-icon-wrap intec-grid-item-4">
                                                <?= Html::tag('a', '', [ /** Ссылка на соц. сеть */
                                                    'href' => $arElement['VALUE'],
                                                    'target' => '_blank',
                                                    'class' => [
                                                        'widget-element-icon',
                                                        'intec-cl-text-hover',
                                                        'fab fa-'.$arElement['ICON']
                                                    ]
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                <?= Html::endTag('div') ?>
                            <?php } ?>
                        </div>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
        </div>
    </div>
</div>
