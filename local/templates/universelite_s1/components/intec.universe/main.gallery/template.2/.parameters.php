<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

$arTemplateParameters['HEADER_SHOW'] = [
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_GALLERY_TEMP2_HEADER_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
];

if ($arCurrentValues['HEADER_SHOW'] == 'Y') {
    $arTemplateParameters['HEADER_POSITION'] = [
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_GALLERY_TEMP2_HEADER_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => [
            'left' => Loc::getMessage('C_GALLERY_TEMP2_POSITION_LEFT'),
            'center' => Loc::getMessage('C_GALLERY_TEMP2_POSITION_CENTER'),
            'right' => Loc::getMessage('C_GALLERY_TEMP2_POSITION_RIGHT')
        ],
        'DEFAULT' => 'center'
    ];
    $arTemplateParameters['HEADER_TEXT'] = [
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_GALLERY_TEMP2_HEADER_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('C_GALLERY_TEMP2_HEADER_TEXT_DEFAULT')
    ];
}

$arTemplateParameters['DESCRIPTION_SHOW'] = [
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_GALLERY_TEMP2_DESCRIPTION_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
];

if ($arCurrentValues['DESCRIPTION_SHOW'] == 'Y') {
    $arTemplateParameters['DESCRIPTION_POSITION'] = [
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_GALLERY_TEMP2_DESCRIPTION_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => [
            'left' => Loc::getMessage('C_GALLERY_TEMP2_POSITION_LEFT'),
            'center' => Loc::getMessage('C_GALLERY_TEMP2_POSITION_CENTER'),
            'right' => Loc::getMessage('C_GALLERY_TEMP2_POSITION_RIGHT')
        ],
        'DEFAULT' => 'center'
    ];
    $arTemplateParameters['DESCRIPTION_TEXT'] = [
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_GALLERY_TEMP2_DESCRIPTION_POSITION'),
        'TYPE' => 'STRING'
    ];
}

$arTemplateParameters['LINE_COUNT'] = [
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_GALLERY_TEMP2_LINE_COUNT'),
    'TYPE' => 'LIST',
    'VALUES' => [
        4 => '4',
        6 => '6'
    ],
    'DEFAULT' => 6
];

$arTemplateParameters['WIDTH_FULL'] = [
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_GALLERY_TEMP2_WIDTH_FULL'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
];
$arTemplateParameters['DELIMITER_USE'] = [
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_GALLERY_TEMP2_DELIMITER_USE'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
];
$arTemplateParameters['FOOTER_SHOW'] = [
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_GALLERY_TEMP2_FOOTER_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
];

if ($arCurrentValues['FOOTER_SHOW'] == 'Y') {
    $arTemplateParameters['FOOTER_POSITION'] = [
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_GALLERY_TEMP2_FOOTER_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => [
            'left' => Loc::getMessage('C_GALLERY_TEMP2_POSITION_LEFT'),
            'center' => Loc::getMessage('C_GALLERY_TEMP2_POSITION_CENTER'),
            'right' => Loc::getMessage('C_GALLERY_TEMP2_POSITION_RIGHT')
        ],
        'DEFAULT' => 'center'
    ];
    $arTemplateParameters['FOOTER_TEXT'] = [
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_GALLERY_TEMP2_FOOTER_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('C_GALLERY_TEMP2_FOOTER_TEXT_DEFAULT')
    ];
    $arTemplateParameters['LIST_PAGE'] = [
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_GALLERY_TEMP2_LIST_PAGE'),
        'TYPE' => 'STRING'
    ];
}