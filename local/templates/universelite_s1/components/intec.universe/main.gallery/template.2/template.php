<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arFooter = $arResult['FOOTER_BLOCK'];
$arViewParams = ArrayHelper::getValue($arResult, 'VIEW_PARAMETERS');

$arVisual = $arResult['VISUAL'];

?>
<div class="widget c-gallery c-gallery-template-2" id="<?= $sTemplateId ?>">
    <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
        <div class="widget-header">
            <div class="intec-content">
                <div class="intec-content-wrapper">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="widget-content">
        <?php if (!$arVisual['WIDTH_FULL']) { ?>
            <div class="intec-content">
                <div class="intec-content-wrapper">
        <?php } ?>
            <?= Html::beginTag('div', [ /** Контентный тег */
                'class' => [
                    'widget-content-wrapper',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-center'
                    ]
                ]
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = ArrayHelper::getValue($arItem, 'NAME');
                    $arPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');

                ?>
                    <?= Html::beginTag('div', [ /** Главный тег элемента */
                        'id' => $sAreaId,
                        'class' => Html::cssClassFromArray([
                            'widget-element-wrap' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1000-4' => $arVisual['LINE_COUNT'] >= 6,
                                '720-3' => $arVisual['LINE_COUNT'] >= 4,
                                '500-2' => true
                            ]
                        ], true),
                        'data-src' => $arPicture['SRC']
                    ]) ?>
                        <?= Html::beginTag('div', [ /** Тег-обертка для картинки */
                            'class' => Html::cssClassFromArray([
                                'widget-element' => true,
                                'with-delimiter' => $arVisual['DELIMITER_USE']
                            ], true)
                        ]) ?>
                            <?= Html::beginTag('div', [ /** Картинка элемента */
                                'class' => 'widget-picture',
                                'style' => [
                                    'background-image' => 'url('.$arPicture['SRC_SMALL'].')'
                                ]
                            ]) ?>
                                <?= Html::img($arPicture['SRC_ICON'], [ /** Иконка для галлереи */
                                    'alt' => $sName,
                                    'title' => $sName
                                ]) ?>
                             <?= Html::endTag('div') ?>
                        <?= Html::endTag('div') ?>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
        <?php if (!$arVisual['WIDTH_FULL']) { ?>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php if ($arFooter['SHOW']) { ?>
        <div class="widget-footer align-<?= $arFooter['POSITION'] ?>">
            <a class="widget-footer-all intec-cl-border intec-cl-background-hover" href="<?= $arFooter['LIST_PAGE'] ?>">
                <?= $arFooter['TEXT'] ?>
            </a>
        </div>
    <?php } ?>
</div>
<?php if (!defined('EDITOR')) {
    include(__DIR__.'/script.php');
} ?>