<?php

$MESS['C_GALLERY_TEMP2_HEADER_SHOW'] = 'Показывать заголовок блока';
$MESS['C_GALLERY_TEMP2_HEADER_POSITION'] = 'Расположение заголовка блока';
$MESS['C_GALLERY_TEMP2_HEADER_TEXT'] = 'Текст заголовка блока';
$MESS['C_GALLERY_TEMP2_DESCRIPTION_SHOW'] = 'Показывать описание блока';
$MESS['C_GALLERY_TEMP2_DESCRIPTION_POSITION'] = 'Расположение описания блока';
$MESS['C_GALLERY_TEMP2_DESCRIPTION_POSITION'] = 'Текст описания блока';
$MESS['C_GALLERY_TEMP2_LINE_COUNT'] = 'Количество элементов в строке';
$MESS['C_GALLERY_TEMP2_WIDTH_FULL'] = 'Блок по всю ширину окна';
$MESS['C_GALLERY_TEMP2_DELIMITER_USE'] = 'Отступ между элементами';
$MESS['C_GALLERY_TEMP2_FOOTER_SHOW'] = 'Показывать кнопку "Показать все"';
$MESS['C_GALLERY_TEMP2_FOOTER_POSITION'] = 'Расположение кнопки "Показать все"';
$MESS['C_GALLERY_TEMP2_FOOTER_TEXT'] = 'Текст кнопки "Показать все"';
$MESS['C_GALLERY_TEMP2_LIST_PAGE'] = 'Ссылка на страницу раздела';

$MESS['C_GALLERY_TEMP2_POSITION_LEFT'] = 'Слева';
$MESS['C_GALLERY_TEMP2_POSITION_CENTER'] = 'По центру';
$MESS['C_GALLERY_TEMP2_POSITION_RIGHT'] = 'Справа';

$MESS['C_GALLERY_TEMP2_HEADER_TEXT_DEFAULT'] = 'Фотогалерея';
$MESS['C_GALLERY_TEMP2_FOOTER_TEXT_DEFAULT'] = 'Показать все';