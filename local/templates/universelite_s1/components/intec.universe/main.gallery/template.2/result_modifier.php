<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

$sHeaderText = ArrayHelper::getValue($arParams, 'HEADER_TEXT');
$sHeaderText = trim($sHeaderText);
$bHeaderShow = ArrayHelper::getValue($arParams, 'HEADER_SHOW');
$bHeaderShow = $bHeaderShow == 'Y' && !empty($sHeaderText);

$arResult['HEADER_BLOCK'] = [
    'SHOW' => $bHeaderShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'HEADER_POSITION'),
    'TEXT' => Html::encode($sHeaderText)
];

$sDescriptionText = ArrayHelper::getValue($arParams, 'DESCRIPTION_TEXT');
$sDescriptionText = trim($sDescriptionText);
$bDescriptionShow = ArrayHelper::getValue($arParams, 'DESCRIPTION_SHOW');
$bDescriptionShow = $bDescriptionShow == 'Y' && !empty($sDescriptionText);

$arResult['DESCRIPTION_BLOCK'] = [
    'SHOW' => $bDescriptionShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'DESCRIPTION_POSITION'),
    'TEXT' => Html::encode($sDescriptionText)
];

$iLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($iLineCount <= 4)
    $iLineCount = 4;

if ($iLineCount >= 6)
    $iLineCount = 6;

$arResult['VISUAL'] = [
    'LINE_COUNT' => $iLineCount,
    'WIDTH_FULL' => ArrayHelper::getValue($arParams, 'WIDTH_FULL') == 'Y',
    'DELIMITER_USE' => ArrayHelper::getValue($arParams, 'DELIMITER_USE') == 'Y'
];

$sFooterText = ArrayHelper::getValue($arParams, 'FOOTER_TEXT');
$sFooterText = trim($sFooterText);
$sListPage = ArrayHelper::getValue($arParams, 'LIST_PAGE');
$sListPage = StringHelper::replaceMacros($sListPage, $arMacros);
$bFooterShow = ArrayHelper::getValue($arParams, 'FOOTER_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sFooterText) && !empty($sListPage);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'FOOTER_POSITION'),
    'TEXT' => Html::encode($sFooterText),
    'LIST_PAGE' => $sListPage
];

foreach ($arResult['ITEMS'] as $iKey => $arItem) {
    $arPreviewPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');

    $arPicture = CFile::ResizeImageGet(
        $arPreviewPicture,
        array(
            'width' => 500,
            'height' => 500
        ),
        BX_RESIZE_IMAGE_EXACT
    );
    $arIcon = CFile::ResizeImageGet(
        $arPreviewPicture,
        array(
            'width' => 150,
            'height' => 150
        ),
        BX_RESIZE_IMAGE_EXACT
    );

    $arItem['PREVIEW_PICTURE']['SRC_SMALL'] = $arPicture['src'];
    $arItem['PREVIEW_PICTURE']['SRC_ICON'] = $arIcon['src'];

    $arResult['ITEMS'][$iKey] = $arItem;
}