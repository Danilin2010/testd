<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;
use intec\core\helpers\Type;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Параметры заголовка */
$sHeaderText = ArrayHelper::getValue($arParams, 'HEADER_TEXT');
$sHeaderText = trim($sHeaderText);
$sHeaderText = !empty($sHeaderText) ? $sHeaderText : Loc::getMessage('GALLERY_TEMP1_HEADER_DEFAULT');
$bHeaderShow = ArrayHelper::getValue($arParams, 'HEADER_SHOW');
$bHeaderShow = $bHeaderShow == 'Y' && !empty($sHeaderText);

$arResult['HEADER_BLOCK'] = [
    'SHOW' => $bHeaderShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'HEADER_POSITION'),
    'TEXT' => Html::encode($sHeaderText)
];

/** Параметры описания */
$sDescriptionText = ArrayHelper::getValue($arParams, 'DESCRIPTION_TEXT');
$sDescriptionText = trim($sDescriptionText);
$bDescriptionShow = ArrayHelper::getValue($arParams, 'DESCRIPTION_SHOW');
$bDescriptionShow = $bDescriptionShow == 'Y' && !empty($sDescriptionText);

$arResult['DESCRIPTION_BLOCK'] = [
    'SHOW' => $bDescriptionShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'DESCRIPTION_POSITION'),
    'TEXT' => Html::encode($sDescriptionText)
];

/** Параметры отображения */
$iLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($iLineCount <= 3)
    $iLineCount = 3;

if ($iLineCount >= 5)
    $iLineCount = 5;

$iMaxElements = ArrayHelper::getValue($arParams, 'MAX_SECTION_ELEMENTS');

if (empty($iMaxElements)) {
    $iMaxElements = null;
} else if (!Type::isNumeric($iMaxElements) || $iMaxElements <= $iLineCount) {
    $iMaxElements = $iLineCount;
}

$arResult['VISUAL'] = [
    'TABS_POSITION' => ArrayHelper::getValue($arParams, 'TABS_POSITION'),
    'LINE_COUNT' => $iLineCount,
    'MAX_SECTION_ELEMENTS' => $iMaxElements,
    'DELIMITER_USE' => ArrayHelper::getValue($arParams, 'DELIMITER_USE') == 'Y'
];

/** Параметры "Смотреть все" */
$sFooterText = ArrayHelper::getValue($arParams, 'FOOTER_TEXT');
$sFooterText = trim($sFooterText);
$sFooterText = !empty($sFooterText) ? $sFooterText : Loc::getMessage('GALLERY_TEMP1_FOOTER_DEFAULT');
$bFooterShow = ArrayHelper::getValue($arParams, 'FOOTER_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sFooterText);
$sListPage = ArrayHelper::getValue($arParams, 'LIST_PAGE');
$sListPage = trim($sListPage);
$sListPage = StringHelper::replaceMacros($sListPage, $arMacros);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'FOOTER_POSITION'),
    'TEXT' => Html::encode($sFooterText),
    'LIST_PAGE' => $sListPage
];

foreach ($arResult['SECTIONS'] as $sectionKey => $arSection) {
    foreach ($arSection['ITEMS'] as $itemKey => $arItem) {
        $arPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');

        $arPictureSmall = CFile::ResizeImageGet(
            $arPicture,
            array(
                'width' => 500,
                'height' => 500
            ),
            BX_RESIZE_IMAGE_EXACT
        );
        $arIcon = CFile::ResizeImageGet(
            $arPicture,
            array(
                'width' => 150,
                'height' => 150
            ),
            BX_RESIZE_IMAGE_EXACT
        );

        $arPicture['SRC_SMALL'] = $arPictureSmall['src'];
        $arPicture['SRC_ICON'] = $arIcon['src'];
        $arItem['PREVIEW_PICTURE'] = $arPicture;
        $arSection['ITEMS'][$itemKey] = $arItem;
    }

    $arResult['SECTIONS'][$sectionKey] = $arSection;
}