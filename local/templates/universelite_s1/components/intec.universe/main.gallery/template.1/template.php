<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateID = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arFooter = $arResult['FOOTER_BLOCK'];
$arVisual = $arResult['VISUAL'];

$bFirstTab = true;

?>
<div class="intec-content">
    <div class="intec-content-wrapper">
        <div class="widget c-gallery c-gallery-template-1" id="<?= $sTemplateID ?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="widget-content">
                <?php if (!empty($arResult['SECTIONS'])) { ?>
                    <div class="widget-tabs">
                        <ul class="nav nav-tabs align-<?= $arVisual['TABS_POSITION'] ?>">
                            <?php foreach ($arResult['SECTIONS'] as $arSection) { ?>
                                <?php if (!empty($arSection['ITEMS'])) { ?>
                                    <?= Html::beginTag('li', [ /** Тег элемента списка */
                                        'class' => Html::cssClassFromArray([
                                            'widget-tabs-tab' => true,
                                            'active' => $bFirstTab
                                        ], true)
                                    ]) ?>
                                        <?= Html::tag('a', $arSection['NAME'], [ /** Название элемента списка */
                                            'class' => Html::cssClassFromArray([
                                                'intec-cl-text' => $bFirstTab
                                            ], true),
                                            'href' => '#gallery-temp1-tab-'.$arSection['ID'],
                                            'data-toggle' => 'tab'
                                        ]) ?>
                                    <?= Html::endTag('li') ?>
                                    <?php $bFirstTab = false ?>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                    <?= Html::beginTag('div', [ /** Главный тег для вкладок */
                        'class' => Html::cssClassFromArray([
                            'widget-tab-list-content' => true,
                            'tab-content' => true,
                            'img-delimiter' => $arVisual['DELIMITER_USE']
                        ], true)
                    ]) ?>
                        <?php $bFirstTab = true ?>
                        <?php foreach ($arResult['SECTIONS'] as $arSectionContent) {

                            $iCount = 0;

                        ?>
                            <?php if (!empty($arSectionContent['ITEMS'])) { ?>
                                <?= Html::beginTag('div', [ /** Главный тег вкладки */
                                    'id' => 'gallery-temp1-tab-'.$arSectionContent['ID'],
                                    'class' => Html::cssClassFromArray([
                                        'widget-tab-list-element' => true,
                                        'tab-pane' => true,
                                        'fade' => true,
                                        'in active' => $bFirstTab
                                    ], true)
                                ]) ?>
                                    <?= Html::beginTag('div', [
                                        'class' => [
                                            'intec-grid' => [
                                                '',
                                                'wrap',
                                                'a-v-start',
                                                'a-h-center'
                                            ]
                                        ]
                                    ]) ?>
                                        <?php foreach ($arSectionContent['ITEMS'] as $arItem) {

                                            $sId = $sTemplateId.'_'.$arItem['ID'];
                                            $sAreaId = $this->GetEditAreaId($sId);
                                            $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                                            $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                                            $iCount++;
                                            $sPicture = ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']);
                                            $sPictureSmall = ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC_SMALL']);
                                            $sIcon = ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC_ICON']);

                                            if ($iCount > $arVisual['MAX_SECTION_ELEMENTS'] && $arVisual['MAX_SECTION_ELEMENTS'] !== null) {
                                                $iCount = 0;
                                                break;
                                            }

                                        ?>
                                            <?= Html::beginTag('div', [ /** Главный тег элемента */
                                                'class' => Html::cssClassFromArray([
                                                    'widget-tab-element' => true,
                                                    'intec-grid-item' => [
                                                        $arVisual['LINE_COUNT'] => true,
                                                        '1000-4' => $arVisual['LINE_COUNT'] >= 5,
                                                        '800-3' => $arVisual['LINE_COUNT'] >= 4,
                                                        '600-2' => $arVisual['LINE_COUNT'] >= 3,
                                                        '400-1' => true
                                                    ]
                                                ], true),
                                                'data-src' => $sPicture
                                            ]) ?>
                                                <?= Html::beginTag('div', [ /** Картинка элемента */
                                                    'class' => 'widget-tab-element-img',
                                                    'style' => [
                                                        'background-image' => 'url('.$sPictureSmall.')'
                                                    ]
                                                ]) ?>
                                                    <i class="widget-tab-element-icon fa fa-search-plus"></i>
                                                    <?= Html::img($sIcon, [ /** Иконка элемента */
                                                        'alt' => $arItem['NAME'],
                                                        'title' => $arItem['NAME']
                                                    ]) ?>
                                                <?= Html::endTag('div') ?>
                                            <?= Html::endTag('div') ?>
                                        <?php } ?>
                                    <?= Html::endTag('div') ?>
                                <?= Html::endTag('div') ?>
                                <?php $bFirstTab = false ?>
                                <?php if (defined('EDITOR')) break ?>
                            <?php } ?>
                        <?php } ?>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            </div>
            <?php if ($arFooter['SHOW']) { ?>
                <div class="widget-footer align-<?= $arFooter['POSITION'] ?>">
                    <a class="widget-footer-all intec-cl-border intec-cl-background-hover" href="<?= $arFooter['LIST_PAGE'] ?>">
                        <?= $arFooter['TEXT'] ?>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php if (!defined('EDITOR')) { ?>
    <?php include(__DIR__.'/script.php') ?>
<?php } ?>