<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

/** VISUAL */
$arTemplateParameters['HEADER_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('GALLERY_TEMP1_HEADER_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['HEADER_SHOW'] == 'Y') {
    $arTemplateParameters['HEADER_POSITION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('GALLERY_TEMP1_HEADER_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'left' => Loc::getMessage('GALLERY_TEMP1_POSITION_LEFT'),
            'center' => Loc::getMessage('GALLERY_TEMP1_POSITION_CENTER'),
            'right' => Loc::getMessage('GALLERY_TEMP1_POSITION_RIGHT')
        ),
        'DEFAULT' => 'center'
    );
    $arTemplateParameters['HEADER_TEXT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('GALLERY_TEMP1_HEADER_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('GALLERY_TEMP1_HEADER_DEFAULT')
    );
}

$arTemplateParameters['DESCRIPTION_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('GALLERY_TEMP1_DESCRIPTION_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['DESCRIPTION_SHOW'] == 'Y') {
    $arTemplateParameters['DESCRIPTION_POSITION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('GALLERY_TEMP1_DESCRIPTION_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'left' => Loc::getMessage('GALLERY_TEMP1_POSITION_LEFT'),
            'center' => Loc::getMessage('GALLERY_TEMP1_POSITION_CENTER'),
            'right' => Loc::getMessage('GALLERY_TEMP1_POSITION_RIGHT')
        ),
        'DEFAULT' => 'center'
    );
    $arTemplateParameters['DESCRIPTION_TEXT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('GALLERY_TEMP1_DESCRIPTION_TEXT'),
        'TYPE' => 'STRING'
    );
}

$arTemplateParameters['TABS_POSITION'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('GALLERY_TEMP1_TABS_POSITION'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        'left' => Loc::getMessage('GALLERY_TEMP1_POSITION_LEFT'),
        'center' => Loc::getMessage('GALLERY_TEMP1_POSITION_CENTER'),
        'right' => Loc::getMessage('GALLERY_TEMP1_POSITION_RIGHT')
    ),
    'DEFAULT' => 'center'
);
$arTemplateParameters['LINE_COUNT'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('GALLERY_TEMP1_LINE_COUNT'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        3 => '3',
        4 => '4',
        5 => '5'
    ),
    'DEFAULT' => '4'
);
$arTemplateParameters['MAX_SECTION_ELEMENTS'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('GALLERY_TEMP1_MAX_SECTION_ELEMENTS'),
    'TYPE' => 'STRING'
);
$arTemplateParameters['DELIMITER_USE'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('GALLERY_TEMP1_DELIMITER_USE'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
);
$arTemplateParameters['FOOTER_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('GALLERY_TEMP1_FOOTER_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['FOOTER_SHOW'] == 'Y') {
    $arTemplateParameters['FOOTER_POSITION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('GALLERY_TEMP1_FOOTER_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'left' => Loc::getMessage('GALLERY_TEMP1_POSITION_LEFT'),
            'center' => Loc::getMessage('GALLERY_TEMP1_POSITION_CENTER'),
            'right' => Loc::getMessage('GALLERY_TEMP1_POSITION_RIGHT')
        ),
        'DEFAULT' => 'center'
    );
    $arTemplateParameters['LIST_PAGE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('GALLERY_TEMP1_LIST_PAGE'),
        'TYPE' => 'STRING'
    );
    $arTemplateParameters['FOOTER_TEXT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('GALLERY_TEMP1_FOOTER_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('GALLERY_TEMP1_FOOTER_DEFAULT')
    );
}