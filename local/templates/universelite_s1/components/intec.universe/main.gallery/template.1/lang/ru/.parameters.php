<?php

$MESS['GALLERY_TEMP1_HEADER_SHOW'] = 'Показывать заголовок блока';
$MESS['GALLERY_TEMP1_HEADER_POSITION'] = 'Расположение заголовка';
$MESS['GALLERY_TEMP1_HEADER_TEXT'] = 'Текст заголовка';
$MESS['GALLERY_TEMP1_DESCRIPTION_SHOW'] = 'Показывать описание блока';
$MESS['GALLERY_TEMP1_DESCRIPTION_POSITION'] = 'Расположение описания';
$MESS['GALLERY_TEMP1_DESCRIPTION_TEXT'] = 'Текст описания';
$MESS['GALLERY_TEMP1_TABS_POSITION'] = 'Расположение вкладок разделов';
$MESS['GALLERY_TEMP1_LINE_COUNT'] = 'Количество элементов в строке';
$MESS['GALLERY_TEMP1_MAX_SECTION_ELEMENTS'] = 'Максимальное количество отображаемых элементов для каждого раздела';
$MESS['GALLERY_TEMP1_DELIMITER_USE'] = 'Использовать отступ между элементами галереи';
$MESS['GALLERY_TEMP1_FOOTER_SHOW'] = 'Показывать кнопку "Смотреть все"';
$MESS['GALLERY_TEMP1_FOOTER_POSITION'] = 'Расположение кнопки "Смотреть все"';
$MESS['GALLERY_TEMP1_LIST_PAGE'] = 'Путь до страницы списка раздела';
$MESS['GALLERY_TEMP1_FOOTER_TEXT'] = 'Текст кнопки "Смотерть все"';

$MESS['GALLERY_TEMP1_POSITION_LEFT'] = 'Слева';
$MESS['GALLERY_TEMP1_POSITION_CENTER'] = 'По центру';
$MESS['GALLERY_TEMP1_POSITION_RIGHT'] = 'Справа';

$MESS['GALLERY_TEMP1_HEADER_DEFAULT'] = 'Фотогалерея';
$MESS['GALLERY_TEMP1_FOOTER_DEFAULT'] = 'Смотреть все';