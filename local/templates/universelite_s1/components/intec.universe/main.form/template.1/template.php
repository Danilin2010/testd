<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\JavaScript;

/**
 * @var array $arResult
 */

$sTemplateId = spl_object_hash($this);

$this->setFrameMode(true);

$arTitle = ArrayHelper::getValue($arResult, 'TITLE');
$arDescription = ArrayHelper::getValue($arResult, 'DESCRIPTION');
$arButton = ArrayHelper::getValue($arResult, 'BUTTON');
$arImage = ArrayHelper::getValue($arResult, 'BG_IMAGE');
$sView = ArrayHelper::getValue($arResult, 'VIEW');
$sTheme = ArrayHelper::getValue($arResult, 'TEXT_THEME');
$sTemplate = ArrayHelper::getValue($arResult, 'TEMPLATE');
$sFormID = ArrayHelper::getValue($arResult, 'ID');
$sConsent = ArrayHelper::getValue($arResult, 'CONSENT');
$sFormName = ArrayHelper::getValue($arResult, 'NAME');

$sDefaultButtonColor = 'widget-form-button intec-cl-background intec-cl-background-light-hover';
$arTitleAttributes = [];
$arDescriptionAttributes = [];

$arForm = [];

if (!empty($sFormID)) {
    $arForm = [
        'id' => $sFormID,
        'template' => $sTemplate,
        'parameters' => [
            'AJAX_OPTION_ADDITIONAL' => $sTemplateId . '_FORM_ASK',
            'CONSENT_URL' => $sConsent
        ],
        'settings' => [
            'title' => $sFormName
        ]
    ];
}

$arButtonAttributes = [
    'class' => $sDefaultButtonColor,
    'onclick' => 'universe.forms.show('.JavaScript::toObject($arForm).')'
];

$arMainBlockAttributes = [
    'class' => 'widget c-form c-form-template-1 clearfix',
    'data-stellar-background-ratio' => $arImage['PARALLAX']['USE'] ? $arImage['PARALLAX']['RATIO'] : null,
    'style' => [
        'background-color' => ArrayHelper::getValue($arResult, 'BG_COLOR')
    ]
];

if ($arImage['SHOW'])
    $arMainBlockAttributes = ArrayHelper::merge($arMainBlockAttributes, ['style' => [
        'background-image' => 'url('.$arImage['SRC'].')',
        'background-position' => $arImage['POSITION_V'].' '.$arImage['POSITION_H'],
        'background-size' => $arImage['SIZE'],
        'background-repeat' => $arImage['REPEAT']
    ]]);

if ($sTheme == 'custom') {
    $arTitleAttributes = [
        'style' => [
            'color' => $arTitle['COLOR']
        ]
    ];

    $arDescriptionAttributes = [
        'style' => [
            'color' => $arDescription['COLOR']
        ]
    ];

    if (!empty($arButton['TEXT_COLOR']) || $arButton['COLOR']) {
        $bButtonHaveBg = !empty($arButton['COLOR']);

        $arButtonAttributes = [
            'class' => $bButtonHaveBg ? 'widget-form-button' : $sDefaultButtonColor,
            'style' => [
                'color' => $arButton['TEXT_COLOR'],
                'background-color' => $arButton['COLOR']
            ],
            'onclick' => 'universe.forms.show('.JavaScript::toObject($arForm).')'
        ];
    }
}

?>
<?= Html::beginTag('div', $arMainBlockAttributes) ?>
    <div class="intec-content intec-content-primary intec-content-visible">
        <div class="intec-content-wrapper clearfix">
            <div class="widget-content theme-<?= $sTheme ?> view-<?= $sView ?>">
                <?php if ($sView == 'right') { ?>
                    <div class="widget-form-action">
                        <?= Html::tag('div', $arButton['TEXT'], $arButtonAttributes) ?>
                    </div>
                <?php } ?>
                <div class="widget-form-text clearfix">
                    <div class="widget-form-title">
                        <?= Html::tag('span', $arTitle['TEXT'], $arTitleAttributes) ?>
                    </div>
                    <div class="widget-form-description">
                        <?= Html::tag('span', $arDescription['TEXT'], $arDescriptionAttributes) ?>
                    </div>
                </div>
                <?php if ($sView == 'left' || $sView == 'center') { ?>
                    <div class="widget-form-action">
                        <?= Html::tag('div', $arButton['TEXT'], $arButtonAttributes) ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?= Html::endTag('div') ?>