<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Вид отображения */
$arResult['VIEW'] = ArrayHelper::getValue($arParams, 'VIEW');

/** Заголовок блока */
$sTitle = ArrayHelper::getValue($arParams, 'TITLE');
$sTitle = trim($sTitle);
$sTitle = !empty($sTitle) ? $sTitle : Loc::getMessage('C_FORM_TEMP1_TITLE_DEFAULT');
$arResult['TITLE'] = [
    'TEXT' => $sTitle,
    'COLOR' => ArrayHelper::getValue($arParams, 'TITLE_COLOR')
];

/** Описание блока */
$sDescription = ArrayHelper::getValue($arParams, 'DESCRIPTION');
$sDescription = trim($sDescription);
$bDescriptionShow = ArrayHelper::getValue($arParams, 'DESCRIPTION_SHOW');
$bDescriptionShow = $bDescriptionShow == 'Y' && !empty($sDescription);
$arResult['DESCRIPTION'] = [
    'SHOW' => $bDescriptionShow,
    'TEXT' => $sDescription,
    'COLOR' => ArrayHelper::getValue($arParams, 'DESCRIPTION_COLOR')
];

/** Заливка фона блока */
$arResult['BG_COLOR'] = ArrayHelper::getValue($arParams, 'BG_COLOR');

/** Фоновое изображение */
$sImgPath = ArrayHelper::getValue($arParams, 'BG_IMAGE');
$sImgPath = trim($sImgPath);
$sImgPath = StringHelper::replaceMacros($sImgPath, $arMacros);
$bImgShow = ArrayHelper::getValue($arParams, 'BG_IMAGE_SHOW');
$bImgShow = $bImgShow == 'Y' && !empty($sImgPath);

$arImgParallax = [
    'USE' => ArrayHelper::getValue($arParams, 'BG_IMAGE_PARALLAX_USE') == 'Y',
    'RATIO' => ArrayHelper::getValue($arParams, 'BG_IMAGE_PARALLAX_RATIO')
];

if ($arImgParallax['RATIO'] < 0) $arImgParallax['RATIO'] = 0 && $bImgShow;
if ($arImgParallax['RATIO'] > 100) $arImgParallax['RATIO'] = 100;

$arImgParallax['RATIO'] = (100 - $arImgParallax['RATIO']) / 100;

$arResult['BG_IMAGE'] = [
    'SHOW' => $bImgShow,
    'SRC' => $sImgPath,
    'POSITION_H' => ArrayHelper::getValue($arParams, 'BG_IMAGE_POSITION_H'),
    'POSITION_V' => ArrayHelper::getValue($arParams, 'BG_IMAGE_POSITION_V'),
    'SIZE' => ArrayHelper::getValue($arParams, 'BG_IMAGE_SIZE'),
    'REPEAT' => ArrayHelper::getValue($arParams, 'BG_IMAGE_REPEAT'),
    'PARALLAX' => $arImgParallax
];

/** Цветовая схема */
$arResult['TEXT_THEME'] = ArrayHelper::getValue($arParams, 'TEXT_THEME');

/** Кнопка */
$arResult['BUTTON'] = [
    'TEXT' => ArrayHelper::getValue($arParams, 'BUTTON_TEXT'),
    'TEXT_COLOR' => ArrayHelper::getValue($arParams, 'BUTTON_TEXT_COLOR'),
    'COLOR' => ArrayHelper::getValue($arParams, 'BUTTON_COLOR')
];