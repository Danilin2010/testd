<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

$arTemplateParameters = array();

/** VISUAL */
$arTemplateParameters['VIEW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_FORM_TEMP1_VIEW'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        'left' => Loc::getMessage('C_FORM_TEMP1_VIEW_LEFT'),
        'center' => Loc::getMessage('C_FORM_TEMP1_VIEW_CENTER'),
        'right' => Loc::getMessage('C_FORM_TEMP1_VIEW_RIGHT')
    ),
    'DEFAULT' => 'left'
);
$arTemplateParameters['BG_COLOR'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_FORM_TEMP1_BG_COLOR'),
    'TYPE' => 'STRING'
);
$arTemplateParameters['BG_IMAGE_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['BG_IMAGE_SHOW'] == 'Y') {
    $arTemplateParameters['BG_IMAGE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE'),
        'TYPE' => 'STRING',
        'DEFAULT' => '#TEMPLATE_PATH#/images/bg.png'
    );
    $arTemplateParameters['BG_IMAGE_POSITION_V'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_POSITION_V'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'top' => Loc::getMessage('C_FORM_TEMP1_VIEW_TOP'),
            'center' => Loc::getMessage('C_FORM_TEMP1_VIEW_CENTER'),
            'right' => Loc::getMessage('C_FORM_TEMP1_VIEW_BOTTOM')
        ),
        'DEFAULT' => 'center'
    );
    $arTemplateParameters['BG_IMAGE_POSITION_H'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_POSITION_H'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'left' => Loc::getMessage('C_FORM_TEMP1_VIEW_LEFT'),
            'center' => Loc::getMessage('C_FORM_TEMP1_VIEW_CENTER'),
            'right' => Loc::getMessage('C_FORM_TEMP1_VIEW_RIGHT')
        ),
        'DEFAULT' => 'left'
    );
    $arTemplateParameters['BG_IMAGE_SIZE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_SIZE'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'cover' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_SIZE_COVER'),
            'contain' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_SIZE_CONTAIN')
        ),
        'DEFAULT' => 'cover'
    );
    $arTemplateParameters['BG_IMAGE_REPEAT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_REPEAT'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'no-repeat' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_REPEAT_NR'),
            'repeat' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_REPEAT_R'),
            'repeat-x' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_REPEAT_RX'),
            'repeat-y' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_REPEAT_RY')
        ),
        'DEFAULT' => 'no-repeat'
    );

    $arTemplateParameters['BG_IMAGE_PARALLAX_USE'] = [
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_PARALLAX_USE'),
        'TYPE' => 'CHECKBOX',
        'REFRESH' => 'Y'
    ];

    if ($arCurrentValues['BG_IMAGE_PARALLAX_USE'] == 'Y') {
        $arTemplateParameters['BG_IMAGE_PARALLAX_RATIO'] = [
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_FORM_TEMP1_BG_IMAGE_PARALLAX_RATIO'),
            'TYPE' => 'STRING',
            'DEFAULT' => 20
        ];
    }
}

$arTemplateParameters['TITLE'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_FORM_TEMP1_TITLE'),
    'TYPE' => 'STRING',
    'DEFAULT' => Loc::getMessage('C_FORM_TEMP1_TITLE_DEFAULT')
);
$arTemplateParameters['DESCRIPTION_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_FORM_TEMP1_DESCRIPTION_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['DESCRIPTION_SHOW'] == 'Y') {
    $arTemplateParameters['DESCRIPTION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_FORM_TEMP1_DESCRIPTION'),
        'TYPE' => 'STRING'
    );
}

$arTemplateParameters['TEXT_THEME'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_FORM_TEMP1_TEXT_THEME'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        'light' => Loc::getMessage('C_FORM_TEMP1_TEXT_THEME_LIGHT'),
        'dark' => Loc::getMessage('C_FORM_TEMP1_TEXT_THEME_DARK'),
        'custom' => Loc::getMessage('C_FORM_TEMP1_TEXT_THEME_CUSTOM')
    ),
    'DEFAULT' => 'dark',
    'REFRESH' => 'Y'
);
$arTemplateParameters['BUTTON_TEXT'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_FORM_TEMP1_BUTTON_TEXT'),
    'TYPE' => 'STRING',
    'DEFAULT' => Loc::getMessage('C_FORM_TEMP1_BUTTON_TEXT_DEFAULT')
);

if ($arCurrentValues['TEXT_THEME'] == 'custom') {
    $arTemplateParameters['TITLE_COLOR'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_FORM_TEMP1_TITLE_COLOR'),
        'TYPE' => 'STRING'
    );

    if ($arCurrentValues['DESCRIPTION_SHOW'] == 'Y') {
        $arTemplateParameters['DESCRIPTION_COLOR'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_FORM_TEMP1_DESCRIPTION_COLOR'),
            'TYPE' => 'STRING'
        );
    }

    $arTemplateParameters['BUTTON_COLOR'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_FORM_TEMP1_BUTTON_COLOR'),
        'TYPE' => 'STRING'
    );
    $arTemplateParameters['BUTTON_TEXT_COLOR'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_FORM_TEMP1_BUTTON_TEXT_COLOR'),
        'TYPE' => 'STRING'
    );
}