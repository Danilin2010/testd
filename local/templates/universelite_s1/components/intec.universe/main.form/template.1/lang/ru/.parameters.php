<?php

$MESS['C_FORM_TEMP1_VIEW'] = 'Вид отображения';
$MESS['C_FORM_TEMP1_BG_COLOR'] = 'Цвет заливки фона блока';
$MESS['C_FORM_TEMP1_BG_IMAGE_SHOW'] = 'Использовать фоновое изображение';
$MESS['C_FORM_TEMP1_BG_IMAGE'] = 'Путь до изображения блока (может быть внешней ссылкой)';
$MESS['C_FORM_TEMP1_BG_IMAGE_POSITION_H'] = 'Горизонтальное расположение фонового изображения';
$MESS['C_FORM_TEMP1_BG_IMAGE_POSITION_V'] = 'Вертикальное расположение фонового изображения';
$MESS['C_FORM_TEMP1_BG_IMAGE_SIZE'] = 'Размер изображения';
$MESS['C_FORM_TEMP1_BG_IMAGE_REPEAT'] = 'Заполнение блока фоновым изображением';
$MESS['C_FORM_TEMP1_BG_IMAGE_PARALLAX_USE'] = 'Использовать эффект паралакса';
$MESS['C_FORM_TEMP1_BG_IMAGE_PARALLAX_RATIO'] = 'Сила эффекта паралакса (0 - 100)';
$MESS['C_FORM_TEMP1_TITLE'] = 'Заголовок блока';
$MESS['C_FORM_TEMP1_DESCRIPTION_SHOW'] = 'Отображать описание блока';
$MESS['C_FORM_TEMP1_DESCRIPTION'] = 'Описание блока';
$MESS['C_FORM_TEMP1_BUTTON_TEXT'] = 'Текст кнопки';
$MESS['C_FORM_TEMP1_TEXT_THEME'] = 'Цветовая схема';
$MESS['C_FORM_TEMP1_TITLE_COLOR'] = 'Цвет заголовка блока';
$MESS['C_FORM_TEMP1_DESCRIPTION_COLOR'] = 'Цвет описания блока';
$MESS['C_FORM_TEMP1_BUTTON_COLOR'] = 'Цвет кнопки';
$MESS['C_FORM_TEMP1_BUTTON_TEXT_COLOR'] = 'Цвет текста кнопки';

$MESS['C_FORM_TEMP1_BG_IMAGE_SIZE_COVER'] = 'Подогнать по высоте/ширине';
$MESS['C_FORM_TEMP1_BG_IMAGE_SIZE_CONTAIN'] = 'Уместить в блок';

$MESS['C_FORM_TEMP1_BG_IMAGE_REPEAT_NR'] = 'Не повторять';
$MESS['C_FORM_TEMP1_BG_IMAGE_REPEAT_R'] = 'Повторять';
$MESS['C_FORM_TEMP1_BG_IMAGE_REPEAT_RX'] = 'Повторять по горизонтали';
$MESS['C_FORM_TEMP1_BG_IMAGE_REPEAT_RY'] = 'Повторять по вертикали';

$MESS['C_FORM_TEMP1_VIEW_LEFT'] = 'Слева';
$MESS['C_FORM_TEMP1_VIEW_CENTER'] = 'По центру';
$MESS['C_FORM_TEMP1_VIEW_RIGHT'] = 'Справа';
$MESS['C_FORM_TEMP1_VIEW_TOP'] = 'Сверху';
$MESS['C_FORM_TEMP1_VIEW_BOTTOM'] = 'Снизу';

$MESS['C_FORM_TEMP1_TEXT_THEME_LIGHT'] = 'Светлая';
$MESS['C_FORM_TEMP1_TEXT_THEME_DARK'] = 'Темная';
$MESS['C_FORM_TEMP1_TEXT_THEME_CUSTOM'] = 'Своя';

$MESS['C_FORM_TEMP1_TITLE_DEFAULT'] = 'Ваш текст';
$MESS['C_FORM_TEMP1_BUTTON_TEXT_DEFAULT'] = 'Заказать';