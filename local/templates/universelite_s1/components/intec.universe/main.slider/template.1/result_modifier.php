<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;
use intec\core\helpers\Type;

/**
 * @var array $arResult
 * @var array $arParams
 */

/** Коды свойств */
$arResult['PROPERTY_CODES'] += [
    'HEADER_COLOR' => ArrayHelper::getValue($arParams, 'PROPERTY_HEADER_COLOR'),
    'DESCRIPTION_COLOR' => ArrayHelper::getValue($arParams, 'PROPERTY_DESCRIPTION_COLOR'),
    'TEXT_POSITION' => ArrayHelper::getValue($arParams, 'PROPERTY_TEXT_POSITION'),
    'LINK' => ArrayHelper::getValue($arParams, 'PROPERTY_LINK'),
    'LINK_BLANK' => ArrayHelper::getValue($arParams, 'PROPERTY_LINK_BLANK'),
    'BUTTON_SHOW' => ArrayHelper::getValue($arParams, 'PROPERTY_BUTTON_SHOW'),
    'BUTTON_TEXT' => ArrayHelper::getValue($arParams, 'PROPERTY_BUTTON_TEXT'),
    'BUTTON_COLOR' => ArrayHelper::getValue($arParams, 'PROPERTY_BUTTON_COLOR'),
    'BUTTON_TEXT_COLOR' => ArrayHelper::getValue($arParams, 'PROPERTY_BUTTON_TEXT_COLOR'),
    'IMAGE' => ArrayHelper::getValue($arParams, 'PROPERTY_IMAGE'),
    'IMAGE_POSITION' => ArrayHelper::getValue($arParams, 'PROPERTY_IMAGE_VERTICAL_POSITION'),
    'BANNER_COLOR' => ArrayHelper::getValue($arParams, 'PROPERTY_BANNER_COLOR'),
    'VIDEO_URL' => ArrayHelper::getValue($arParams, 'PROPERTY_VIDEO_URL')
];

/** Параметры отображения */
$arVisual = [
    'HEIGHT' => ArrayHelper::getValue($arParams, 'HEIGHT'),
    'SELECTOR' => ArrayHelper::getValue($arParams, 'SELECTOR'),
    'ATTRIBUTE' => ArrayHelper::getValue($arParams, 'ATTRIBUTE'),
    'CLASS' => ArrayHelper::getValue($arParams, 'CLASS'),
    'SLIDER' => [
        'USE' => count($arResult['ITEMS']) > 1,
        'DOTS' => ArrayHelper::getValue($arParams, 'SLIDER_DOTS') == 'Y',
        'LOOP' => ArrayHelper::getValue($arParams, 'SLIDER_LOOP') == 'Y',
        'SPEED' => ArrayHelper::getValue($arParams, 'SLIDER_SPEED'),
        'AUTO' => [
            'USE' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_USE') == 'Y',
            'TIME' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_TIME'),
            'PAUSE' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_PAUSE') == 'Y'
        ]
    ],
    'VIDEO' => [
        'SHADOW' => [
            'USE' => ArrayHelper::getValue($arParams, 'VIDEO_SHADOW_USE') == 'Y',
            'COLOR' => [
                'VALUE' => ArrayHelper::getValue($arParams, 'VIDEO_SHADOW_COLOR'),
                'CUSTOM' => ArrayHelper::getValue($arParams, 'VIDEO_SHADOW_COLOR_CUSTOM'),
            ],
            'OPACITY' => ArrayHelper::getValue($arParams, 'VIDEO_SHADOW_OPACITY')
        ]
    ]
];

if (empty($arVisual['HEIGHT']))
    $arVisual['HEIGHT'] = 500;

if (empty($arVisual['SELECTOR']))
    $arVisual['SELECTOR'] = null;

if (empty($arVisual['ATTRIBUTE']))
    $arVisual['ATTRIBUTE'] = null;

if (empty($arVisual['SLIDER']['SPEED']))
    $arVisual['SLIDER']['SPEED'] = 500;

if (empty($arVisual['SLIDER']['AUTO']['TIME']))
    $arVisual['SLIDER']['AUTO']['TIME'] = 10000;

$arResult['VISUAL'] = ArrayHelper::merge($arResult['VISUAL'], $arVisual);


foreach ($arResult['ITEMS'] as $sKey => $arItem) {
    /** Обработка выбранных свойств */
    $arCodes = ArrayHelper::getValue($arResult, 'PROPERTY_CODES');

    foreach ($arCodes as $sCode) {
        if (!empty($sCode)) {
            $arProperty = ArrayHelper::getValue($arItem, ['PROPERTIES', $sCode]);

            if (!empty($arProperty)) {
                /** Текстовое поле */
                if ($arProperty['PROPERTY_TYPE'] == 'S' && $arProperty['LIST_TYPE'] == 'L') {
                    if (Type::isArray($arProperty['VALUE'])) {
                        if (!empty($arProperty['VALUE']['TEXT'])) {
                            if ($arProperty['VALUE']['TYPE'] == 'HTML') {
                                $arProperty['VALUE'] = $arProperty['~VALUE']['TEXT'];
                            } else {
                                $arProperty['VALUE'] = $arProperty['VALUE']['TEXT'];
                            }
                        } else {
                            $arProperty['VALUE'] = null;
                        }

                        $arItem['PROPERTIES'][$sCode] = $arProperty;
                    }
                }

                /** Файл */
                if ($arProperty['PROPERTY_TYPE'] == 'F' && $arProperty['LIST_TYPE'] == 'L') {
                    if (!empty($arProperty['VALUE'])) {
                        $arPicture = CFile::ResizeImageGet(
                            $arProperty['VALUE'],
                            array(
                                'width' => 300,
                                'height' => 300
                            ),
                            BX_RESIZE_IMAGE_PROPORTIONAL_ALT
                        );

                        $arProperty['VALUE'] = $arPicture;
                        $arItem['PROPERTIES'][$sCode] = $arProperty;
                    }
                }
            }
        }

        $arResult['ITEMS'][$sKey] = $arItem;
    }
}