<?php 

$MESS['C_SLIDER_TEMP1_PROPERTY_HEADER_COLOR'] = 'Свойство "Цвет заголовка"';
$MESS['C_SLIDER_TEMP1_PROPERTY_DESCRIPTION_COLOR'] = 'Свойство "Цвет описания"';
$MESS['C_SLIDER_TEMP1_TEXT_POSITION'] = 'Свойство "Расположение текста"';
$MESS['C_SLIDER_TEMP1_PROPERTY_LINK'] = 'Свойство "Ссылка"';
$MESS['C_SLIDER_TEMP1_PROPERTY_LINK_BLANK'] = 'Свойство "Открывать в новой вкладке"';
$MESS['C_SLIDER_TEMP1_PROPERTY_BUTTON_SHOW'] = 'Свойство "Отображать кнопку"';
$MESS['C_SLIDER_TEMP1_PROPERTY_BUTTON_TEXT'] = 'Свойство "Текст кнопки"';
$MESS['C_SLIDER_TEMP1_PROPERTY_BUTTON_COLOR'] = 'Свойство "Цвет кнопки"';
$MESS['C_SLIDER_TEMP1_PROPERTY_BUTTON_TEXT_COLOR'] = 'Свойство "Цвет текста кнопки"';
$MESS['C_SLIDER_TEMP1_PROPERTY_IMAGE'] = 'Свойство "Картинка баннера"';
$MESS['C_SLIDER_TEMP1_PROPERTY_IMAGE_VERTICAL_POSITION'] = 'Свойство "Вертикальное расположение картинки"';
$MESS['C_SLIDER_TEMP1_PROPERTY_BANNER_COLOR'] = 'Свойство "Цвет баннера"';
$MESS['C_SLIDER_TEMP1_PROPERTY_VIDEO_URL'] = 'Свойство "Ссылка на видео"';
$MESS['C_SLIDER_TEMP1_HEIGHT'] = 'Высота баннера';
$MESS['C_SLIDER_TEMP1_VIDEO_SHADOW_USE'] = 'Использовать затемнение видео';
$MESS['C_SLIDER_TEMP1_VIDEO_SHADOW_COLOR'] = 'Цвет блока для затемнения видео';
$MESS['C_SLIDER_TEMP1_VIDEO_SHADOW_COLOR_CUSTOM'] = 'Ваш цвет блока для затемнения видео';
$MESS['C_SLIDER_TEMP1_VIDEO_SHADOW_OPACITY'] = 'Прозрачность блока для затемнения видео (%)';
$MESS['C_SLIDER_TEMP1_SLIDER_DOTS'] = 'Отображать навигационные точки';
$MESS['C_SLIDER_TEMP1_SLIDER_LOOP'] = 'Зацикленная прокрутка слайдов';
$MESS['C_SLIDER_TEMP1_SLIDER_SLIDE'] = 'Скорость анимации прокрутки (мс)';
$MESS['C_SLIDER_TEMP1_SLIDER_AUTO_USE'] = 'Использовать автопрокрутку слайдов';
$MESS['C_SLIDER_TEMP1_SLIDER_AUTO_TIME'] = 'Время задержки перед сменой слайдов (мс)';
$MESS['C_SLIDER_TEMP1_SLIDER_AUTO_PAUSE'] = 'Останавливать прокрутку при наведении мыши на баннер';