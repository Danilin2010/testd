<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!Loader::includeModule('iblock'))
    return;

/**
 * @var array $arCurrentValues
 */

$arPropertiesText = array();
$arPropertiesCheckbox = array();
$arPropertiesFile = array();
$arPropertiesList = array();

if (!empty($arCurrentValues['IBLOCK_ID'])) {
    /** Список свойств инфоблока */
    $rsProperties = CIBlockProperty::GetList(
        array(),
        array(
            'IBLOCK_ID' => $arCurrentValues['IBLOCK_ID']
        )
    );

    while ($arProperty = $rsProperties->Fetch()) {
        if ($arProperty['PROPERTY_TYPE'] == 'S' && $arProperty['LIST_TYPE'] == 'L') {
            $arPropertiesText[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        } elseif ($arProperty['PROPERTY_TYPE'] == 'L' && $arProperty['LIST_TYPE'] == 'C') {
            $arPropertiesCheckbox[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        } elseif ($arProperty['PROPERTY_TYPE'] == 'F' && $arProperty['LIST_TYPE'] == 'L') {
            $arPropertiesFile[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        } elseif ($arProperty['PROPERTY_TYPE'] == 'L' && $arProperty['LIST_TYPE'] == 'L') {
            $arPropertiesList[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        }
    }
}


$arTemplateParameters = array();

if (!empty($arCurrentValues['IBLOCK_ID'])) {
    $arTemplateParameters = array(
        'PROPERTY_HEADER_COLOR' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_HEADER_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_DESCRIPTION_COLOR' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_DESCRIPTION_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_TEXT_POSITION' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_TEXT_POSITION'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesList,
            'DEFAULT' => 'left',
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_LINK' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_LINK'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_LINK_BLANK' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_LINK_BLANK'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesCheckbox,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_BUTTON_SHOW' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_BUTTON_SHOW'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesCheckbox,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_BUTTON_TEXT' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_BUTTON_TEXT'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_BUTTON_COLOR' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_BUTTON_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_BUTTON_TEXT_COLOR' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_BUTTON_TEXT_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_IMAGE' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_IMAGE'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesFile,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_IMAGE_VERTICAL_POSITION' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_IMAGE_VERTICAL_POSITION'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesList,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_BANNER_COLOR' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_PROPERTY_BANNER_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesList,
            'ADDITIONAL_VALUES' => 'Y'
        )
    );
}

$arTemplateParameters += array(
    'HEIGHT' => array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SLIDER_TEMP1_HEIGHT'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            350 => '350px',
            400 => '400px',
            450 => '450px',
            500 => '500px',
            550 => '550px',
            600 => '600px'
        ),
        'DEFAULT' => 500,
        'ADDITIONAL_VALUES' => 'Y'
    ),
    'SLIDER_DOTS' => array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SLIDER_TEMP1_SLIDER_DOTS'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N'
    ),
    'SLIDER_LOOP' => array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SLIDER_TEMP1_SLIDER_LOOP'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N'
    ),
    'SLIDER_SLIDE' => array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SLIDER_TEMP1_SLIDER_SLIDE'),
        'TYPE' => 'STRING',
        'DEFAULT' => '500'
    ),
    'SLIDER_AUTO_USE' => array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SLIDER_TEMP1_SLIDER_AUTO_USE'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N',
        'REFRESH' => 'Y'
    )
);

if ($arCurrentValues['SLIDER_AUTO_USE'] == 'Y') {
    $arTemplateParameters += array(
        'SLIDER_AUTO_TIME' => array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_SLIDER_AUTO_TIME'),
            'TYPE' => 'STRING',
            'DEFAULT' => '10000'
        ),
        'SLIDER_AUTO_PAUSE' => array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP1_SLIDER_AUTO_PAUSE'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N'
        )
    );
}