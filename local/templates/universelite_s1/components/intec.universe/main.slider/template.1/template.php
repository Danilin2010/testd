<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use Bitrix\Main\Localization\Loc;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arCodes = $arResult['PROPERTY_CODES'];
$arVisual = $arResult['VISUAL'];

?>
<div class="intec-content-wrap">
    <div class="widget c-slider c-slider-template-1" id="<?= $sTemplateId ?>">
        <?= Html::beginTag('div', [ /** Контентный тег */
            'class' => Html::cssClassFromArray([
                'widget-content' => true,
                'owl-carousel' => $arVisual['SLIDER']['USE']
            ], true)
        ]) ?>
            <?php foreach ($arResult['ITEMS'] as $arItem) {

                $sId = $sTemplateId.'_'.$arItem['ID'];
                $sAreaId = $this->GetEditAreaId($sId);
                $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                $arSlide = [
                    'BACKGROUND' => ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']),
                    'POSITION' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['TEXT_POSITION'], 'VALUE_XML_ID']),
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BANNER_COLOR'], 'VALUE_XML_ID']),
                    'TAG' => 'div'
                ];

                if (empty($arSlide['POSITION']))
                    $arSlide['POSITION'] = 'left';

                $arHeader = [
                    'TEXT' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['HEADER'], 'VALUE']),
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['HEADER_COLOR'], 'VALUE'])
                ];

                if (empty($arHeader['COLOR']))
                    $arHeader['COLOR'] = null;

                $arDescription = [
                    'TEXT' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['DESCRIPTION'], 'VALUE']),
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['DESCRIPTION_COLOR'], 'VALUE'])
                ];

                if (empty($arDescription['COLOR']))
                    $arDescription['COLOR'] = null;

                $arLink = [
                    'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK'], 'VALUE']),
                    'BLANK' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK_BLANK'], 'VALUE']) == 'Y'
                ];

                if (!empty($arLink['VALUE']))
                    $arLink['VALUE'] = StringHelper::replaceMacros($arLink['VALUE'], ['SITE_DIR' => SITE_DIR]);

                $arButton = [
                    'SHOW' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_SHOW'], 'VALUE']) == 'Y',
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_COLOR'], 'VALUE']),
                    'TEXT' => [
                        'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_TEXT'], 'VALUE']),
                        'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_TEXT_COLOR'], 'VALUE'])
                    ]
                ];

                if (!$arButton['SHOW'] && !empty($arLink['VALUE']))
                    $arSlide['TAG'] = 'a';

                if (empty($arButton['COLOR']))
                    $arButton['COLOR'] = null;

                if (empty($arButton['TEXT']['VALUE']))
                    $arButton['TEXT']['VALUE'] = Loc::getMessage('C_SLIDER_TEMP1_BUTTON_TEXT_DEFAULT');

                if (empty($arButton['TEXT']['COLOR']))
                    $arButton['TEXT']['COLOR'] = null;

                $arImage = [
                    'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['IMAGE'], 'VALUE', 'src']),
                    'POSITION' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['IMAGE_POSITION'], 'VALUE_XML_ID'])
                ];

                if (empty($arImage['POSITION']))
                    $arImage['POSITION'] = 'middle';

                $arVideo = [
                    'SHOW' => false,
                    'URL' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['VIDEO_URL'], 'VALUE'])
                ];

                $arVideo['SHOW'] = !empty($arVideo['URL']);
            ?>
                <?= Html::beginTag($arSlide['TAG'], [ /** Главный тег элемента */
                    'class' => Html::cssClassFromArray([
                        'widget-element' => [
                            '' => true,
                            'video' => $arVideo['SHOW'],
                            'text-'.$arSlide['POSITION'] => true
                        ]
                    ], true),
                    'href' => $arSlide['TAG'] == 'a' ? $arLink['VALUE'] : null,
                    'target' => $arSlide['TAG'] == 'a' && $arLink['BLANK'] ? '_blank' : null,
                    'data-color' => $arSlide['COLOR'],
                    'style' => [
                        'height' => $arVisual['HEIGHT'].'px',
                        'background-image' => 'url('.$arSlide['BACKGROUND'].')'
                    ]
                ]) ?>
                    <?php if ($arVideo['SHOW']) { ?>
                        <div class="widget-element-video-wrap">
                            <?php $APPLICATION->IncludeComponent(
                                'intec.universe:system.video.frame',
                                '.default',
                                array(
                                    'URL' => $arVideo['URL'],
                                    'SHADOW_USE' => $arVisual['VIDEO']['SHADOW']['USE'] ? 'Y' : 'N',
                                    'SHADOW_COLOR' => $arVisual['VIDEO']['SHADOW']['COLOR']['VALUE'],
                                    'SHADOW_COLOR_CUSTOM' => $arVisual['VIDEO']['SHADOW']['COLOR']['CUSTOM'],
                                    'SHADOW_OPACITY' => $arVisual['VIDEO']['SHADOW']['OPACITY']
                                ),
                                $component
                            ) ?>
                        </div>
                    <?php } ?>
                    <div class="widget-element-wrapper intec-content">
                        <div class="widget-element-wrapper-2 intec-content-wrapper">
                            <div class="widget-element-wrapper-3" id="<?= $sAreaId ?>">
                                <div class="widget-element-content">
                                    <div class="widget-element-text">
                                        <div class="widget-element-text-wrapper">
                                            <?php if ($arVisual['HEADER']['SHOW'] && !empty($arHeader['TEXT'])) { ?>
                                                <div class="widget-element-text-header">
                                                    <?= Html::tag('div', $arHeader['TEXT'], [
                                                        'style' => ['color' => $arHeader['COLOR']]
                                                    ]) ?>
                                                </div>
                                            <?php } ?>
                                            <?php if ($arSlide['POSITION'] == 'center') { ?>
                                                <?php if ($arVisual['DESCRIPTION']['SHOW'] && !empty($arDescription['TEXT'])) { ?>
                                                    <div class="widget-element-text-description">
                                                        <?= Html::tag('div', $arDescription['TEXT'], [
                                                            'style' => ['color' => $arDescription['COLOR']]
                                                        ]) ?>
                                                    </div>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <?php if ($arVisual['DESCRIPTION']['SHOW'] && !empty($arDescription['TEXT'])) { ?>
                                                    <div class="widget-element-text-description-mobile">
                                                        <?= Html::tag('div', $arDescription['TEXT'], [
                                                            'style' => ['color' => $arDescription['COLOR']]
                                                        ]) ?>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if ($arButton['SHOW'] && !empty($arLink['VALUE'])) { ?>
                                                <div class="widget-element-text-button-wrap">
                                                    <?= Html::tag('a', $arButton['TEXT']['VALUE'], [ /** Кнопка */
                                                        'class' => Html::cssClassFromArray([
                                                            'widget-element-text-button' => true,
                                                            'intec-cl-background' => empty($arButton['TEXT']['COLOR']),
                                                            'intec-cl-background-light-hover' => empty($arButton['TEXT']['COLOR'])
                                                        ], true),
                                                        'href' => $arLink['VALUE'],
                                                        'style' => [
                                                            'color' => $arButton['TEXT']['COLOR'],
                                                            'background' => $arButton['COLOR']
                                                        ]
                                                    ]) ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($arSlide['POSITION'] != 'center') { ?>
                                    <?php if ($arButton['SHOW'] && !empty($arLink['VALUE'])) { ?>
                                        <div class="widget-element-button-container">
                                            <?= Html::beginTag('div', [ /** Тег-обертка для позиционарования кнопки */
                                                'class' => 'widget-element-button-container-wrapper',
                                                'style' => ['vertical-align' => $arImage['POSITION']]
                                            ]) ?>
                                                <?= Html::beginTag('a', [
                                                    'class' => Html::cssClassFromArray([ /** Кнопка */
                                                        'widget-element-button' => true,
                                                        'intec-cl-background' => empty($arButton['COLOR'])
                                                    ], true),
                                                    'href' => $arLink['VALUE'],
                                                    'style' => ['background' => $arButton['COLOR']]
                                                ]) ?>
                                                    <?= Html::beginTag('div', [
                                                        'class' => Html::cssClassFromArray([
                                                            'widget-element-button-text-wrap' => [ /** Тег-обертка для текста */
                                                                '' => true,
                                                                'full' => empty($arImage['VALUE'])
                                                            ]
                                                        ], true)
                                                    ]) ?>
                                                        <?= Html::tag('i', '', [ /** Стрелка */
                                                            'class' => 'widget-element-button-text-arrow far fa-arrow-right',
                                                            'style' => ['color' => $arButton['TEXT']['COLOR']]
                                                        ]) ?>
                                                        <div class="widget-element-button-text">
                                                            <?= Html::tag('div', $arButton['TEXT']['VALUE'], [ /** Текст кнопки */
                                                                'style' => ['color' => $arButton['TEXT']['COLOR']]
                                                            ]) ?>
                                                        </div>
                                                        <?php if ($arVisual['DESCRIPTION']['SHOW'] && !empty($arDescription['TEXT'])) { ?>
                                                            <div class="widget-element-button-description">
                                                                <?= Html::tag('div', $arDescription['TEXT'], [ /** Описание кнопки */
                                                                    'style' => ['color' => $arDescription['COLOR']]
                                                                ]) ?>
                                                            </div>
                                                        <?php } ?>
                                                    <?= Html::endTag('div') ?>
                                                    <?php if (!empty($arImage['VALUE'])) { ?>
                                                        <?= Html::tag('div', '', [ /** Картинка кнопки */
                                                            'class' => 'widget-element-button-image',
                                                            'style' => [
                                                                'background-image' => 'url('.$arImage['VALUE'].')'
                                                            ]
                                                        ]) ?>
                                                    <?php } ?>
                                                <?= Html::endTag('a') ?>
                                            <?= Html::endTag('div') ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?= Html::endTag($arSlide['TAG']) ?>
            <?php } ?>
        <?= Html::endTag('div') ?>
    </div>
    <?php include(__DIR__.'/parts/script.php') ?>
</div>