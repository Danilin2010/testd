<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use Bitrix\Main\Localization\Loc;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arVisual = $arResult['VISUAL'];
$arCodes = $arResult['PROPERTY_CODES'];

?>
<div class="intec-content-wrap">
    <div class="widget c-slider c-slider-template-2" id="<?= $sTemplateId ?>">
        <?= Html::beginTag('div', [
            'class' => Html::cssClassFromArray([
                'widget-content' => true,
                'owl-carousel' => $arVisual['SLIDER']['USE']
            ], true)
        ]) ?>
            <?php foreach ($arResult['ITEMS'] as $arItem) {

                $sId = $sTemplateId.'_'.$arItem['ID'];
                $sAreaId = $this->GetEditAreaId($sId);
                $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                $arSlide = [
                    'BACKGROUND' => ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']),
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BANNER_COLOR'], 'VALUE_XML_ID']),
                    'POSITION' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['TEXT_POSITION'], 'VALUE_XML_ID']),
                ];

                if (empty($arSlide['POSITION']))
                    $arSlide['POSITION'] = 'left';

                $arHeader = [
                    'TEXT' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['HEADER'], 'VALUE']),
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['HEADER_COLOR'], 'VALUE'])
                ];

                if (empty($arHeader['COLOR']))
                    $arHeader['COLOR'] = null;

                $arDescription = [
                    'TEXT' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['DESCRIPTION'], 'VALUE']),
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['DESCRIPTION_COLOR'], 'VALUE'])
                ];

                if (empty($arDescription['COLOR']))
                    $arDescription['COLOR'] = null;

                $arLink = [
                    'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK'], 'VALUE']),
                    'BLANK' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK_BLANK'], 'VALUE']) == 'Y'
                ];

                if (!empty($arLink['VALUE']))
                    $arLink['VALUE'] = StringHelper::replaceMacros($arLink['VALUE'], ['SITE_DIR' => SITE_DIR]);

                $arButton = [
                    'SHOW' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_SHOW'], 'VALUE']) == 'Y',
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_COLOR'], 'VALUE']),
                    'TEXT' => [
                        'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_TEXT'], 'VALUE']),
                        'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_TEXT_COLOR'], 'VALUE'])
                    ]
                ];

                if (!$arButton['SHOW'] && !empty($arLink['VALUE']))
                    $arSlide['TAG'] = 'a';

                if (empty($arButton['COLOR']))
                    $arButton['COLOR'] = null;

                if (empty($arButton['TEXT']['VALUE']))
                    $arButton['TEXT']['VALUE'] = Loc::getMessage('C_SLIDER_TEMP2_BUTTON_TEXT_DEFAULT');

                if (empty($arButton['TEXT']['COLOR']))
                    $arButton['TEXT']['COLOR'] = null;

                $arImage = [
                    'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['IMAGE'], 'VALUE', 'src']),
                    'POSITION' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['IMAGE_VERTICAL_POSITION'], 'VALUE_XML_ID'])
                ];

                if (empty($arImage['POSITION']))
                    $arImage['POSITION'] = 'middle';

                $arVideo = [
                    'SHOW' => false,
                    'URL' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['VIDEO_URL'], 'VALUE'])
                ];

                $arVideo['SHOW'] = !empty($arVideo['URL']);
            ?>
                <?= Html::beginTag('div', [ /** Главный тег элемента */
                    'class' => Html::cssClassFromArray([
                        'widget-element' => [
                            '' => true,
                            'video' => $arVideo['SHOW'],
                        ]
                    ], true),
                    'data-color' => $arSlide['COLOR'],
                    'style' => [
                        'height' => $arVisual['HEIGHT'].'px',
                        'background-image' => 'url('.$arSlide['BACKGROUND'].')'
                    ]
                ]) ?>
                    <?php if ($arVideo['SHOW']) { ?>
                        <div class="widget-element-video-wrap">
                            <?php $APPLICATION->IncludeComponent(
                                'intec.universe:system.video.frame',
                                '.default',
                                array(
                                    'URL' => $arVideo['URL'],
                                    'SHADOW_USE' => $arVisual['VIDEO']['SHADOW']['USE'] ? 'Y' : 'N',
                                    'SHADOW_COLOR' => $arVisual['VIDEO']['SHADOW']['COLOR']['VALUE'],
                                    'SHADOW_COLOR_CUSTOM' => $arVisual['VIDEO']['SHADOW']['COLOR']['CUSTOM'],
                                    'SHADOW_OPACITY' => $arVisual['VIDEO']['SHADOW']['OPACITY']
                                ),
                                $component
                            ) ?>
                        </div>
                    <?php } ?>
                    <div class="widget-element-wrapper intec-content">
                        <div class="widget-element-wrapper-2 intec-content-wrapper">
                            <?php if ($arSlide['POSITION'] !=  'center' && !empty($arImage['VALUE'])) { ?>
                                <?= Html::beginTag('div', [ /** Тег для горизонтального расположения картинки */
                                    'class' => [
                                        'widget-element-image' => [
                                            '',
                                            $arSlide['POSITION']
                                        ]
                                    ]
                                ]) ?>
                                    <?= Html::beginTag('div', [ /** Тег для вертикального раположения картинки */
                                        'class' => 'widget-element-image-wrapper',
                                        'style' => ['vertical-align' => $arImage['POSITION']]
                                    ]) ?>
                                        <?= Html::img($arImage['VALUE']) ?>
                                    <?= Html::endTag('div') ?>
                                <?= Html::endTag('div') ?>
                            <?php } ?>
                            <div class="widget-element-content" id="<?= $sAreaId ?>">
                                <?= Html::beginTag('div', [ /** Тег-обертка для текста баннера */
                                    'class' => [
                                        'widget-element-text' => [
                                            '',
                                            $arSlide['POSITION']
                                        ]
                                    ]
                                ]) ?>
                                    <div class="widget-element-text-wrapper">
                                        <?php if ($arVisual['HEADER']['SHOW'] && !empty($arHeader['TEXT'])) { ?>
                                            <div class="widget-element-text-header">
                                                <?= Html::tag('div', $arHeader['TEXT'], [
                                                    'style' => ['color' => $arHeader['COLOR']]
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($arVisual['DESCRIPTION']['SHOW'] && !empty($arDescription['TEXT'])) { ?>
                                            <div class="widget-element-text-description">
                                                <?= Html::tag('div', $arDescription['TEXT'], [
                                                    'style' => ['color' => $arDescription['COLOR']]
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($arButton['SHOW'] && !empty($arLink['VALUE'])) { ?>
                                            <div class="widget-element-text-button-wrap">
                                                <?= Html::tag('a', $arButton['TEXT']['VALUE'], [ /** Кнопка баннера */
                                                    'class' => Html::cssClassFromArray([
                                                        'widget-element-text-button' => true,
                                                        'intec-cl-background' => empty($arButton['COLOR']),
                                                        'intec-cl-background-light-hover' => empty($arButton['COLOR'])
                                                    ], true),
                                                    'href' => $arLink['VALUE'],
                                                    'target' => $arLink['BLANK'] ? '_blank' : null,
                                                    'style' => [
                                                        'background' => $arButton['COLOR'],
                                                        'color' => $arButton['TEXT']['COLOR']
                                                    ]
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?= Html::endTag('div') ?>
                            </div>
                        </div>
                    </div>
                <?= Html::endTag('div') ?>
            <?php } ?>
        <?= Html::endTag('div') ?>
    </div>
    <?php include(__DIR__.'/parts/script.php') ?>
</div>
