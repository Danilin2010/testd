<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;
use Bitrix\Main\Localization\Loc;

/**
 * @var CMain $APPLICATION
 * @var CBitrixComponent $component
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arVisual = $arResult['VISUAL'];
$arCodes = $arResult['PROPERTY_CODES'];

?>
<div class="intec-content-wrap">
    <div class="widget c-slider c-slider-template-4" id="<?= $sTemplateId ?>">
        <?= Html::beginTag('div', [
            'class' => Html::cssClassFromArray([
                'widget-content' => true,
                'owl-carousel' => $arVisual['SLIDER']['USE']
            ], true)
        ]) ?>
            <?php foreach ($arResult['ITEMS'] as $arItem) {

                $sId = $sTemplateId.'_'.$arItem['ID'];
                $sAreaId = $this->GetEditAreaId($sId);
                $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                $arSlide = [
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BANNER_COLOR'], 'VALUE_XML_ID']),
                    'BACKGROUND' => ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']),
                    'POSITION' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['TEXT_POSITION'], 'VALUE_XML_ID']),
                    'TAG' => 'div'
                ];

                if (empty($arSlide['POSITION']))
                    $arSlide['POSITION'] = 'left';

                $arHeader = [
                    'TEXT' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['HEADER'], 'VALUE']),
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['HEADER_COLOR'], 'VALUE'])
                ];

                if (empty($arHeader['COLOR']))
                    $arHeader['COLOR'] = null;

                $arDescription = [
                    'TEXT' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['DESCRIPTION'], 'VALUE']),
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['DESCRIPTION_COLOR'], 'VALUE'])
                ];

                if (empty($arDescription['COLOR']))
                    $arDescription['COLOR'] = null;

                $arLink = [
                    'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK'], 'VALUE']),
                    'BLANK' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['LINK_BLANK'], 'VALUE']) == 'Y'
                ];

                $arLink['VALUE'] = StringHelper::replaceMacros($arLink['VALUE'], [
                    'SITE_DIR' => SITE_DIR
                ]);

                $arButton = [
                    'SHOW' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_SHOW'], 'VALUE']) == 'Y',
                    'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_COLOR'], 'VALUE']),
                    'CLASS' => null,
                    'TEXT' => [
                        'VALUE' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['BUTTON_TEXT'], 'VALUE']),
                        'COLOR' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['PROPERTY_BUTTON_TEXT_COLOR'], 'VALUE'])
                    ]
                ];

                if (empty($arButton['TEXT']['VALUE']))
                    $arButton['TEXT']['VALUE'] = Loc::getMessage('C_SLIDER_TEMP4_BUTTON_TEXT_DEFAULT');

                if (empty($arButton['TEXT']['COLOR']))
                    $arButton['TEXT']['COLOR'] = null;

                if (empty($arButton['COLOR']))
                    $arButton['COLOR'] = null;

                if (!$arButton['SHOW'] && !empty($arLink['VALUE']))
                    $arSlide['TAG'] = 'a';

                $arInsertions = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['INSERTIONS'], 'VALUE']);
                $arVideo = [
                    'SHOW' => false,
                    'URL' => ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['VIDEO_URL'], 'VALUE'])
                ];

                $arVideo['SHOW'] = !empty($arVideo['URL']);

            ?>
                <?= Html::beginTag($arSlide['TAG'], [ /** Главный тег элемента */
                    'class' => Html::cssClassFromArray([
                        'widget-element' => [
                            '' => true,
                            'video' => $arVideo['SHOW'],
                            'text-'.$arSlide['POSITION'] => true
                        ]
                    ], true),
                    'href' => $arSlide['TAG'] == 'a' && !empty($arLink['VALUE']) ? $arLink['VALUE'] : null,
                    'target' => $arSlide['TAG'] == 'a' && $arLink['BLANK'] ? '_blank' : null,
                    'data-color' => $arSlide['COLOR'],
                    'style' => [
                        'height' => $arVisual['HEIGHT'].'px',
                        'background-image' => 'url('.$arSlide['BACKGROUND'].')'
                    ]
                ]) ?>
                    <?php if ($arVideo['SHOW']) { ?>
                        <div class="widget-element-video-wrap">
                            <?php $APPLICATION->IncludeComponent(
                                'intec.universe:system.video.frame',
                                '.default',
                                array(
                                    'URL' => $arVideo['URL'],
                                    'SHADOW_USE' => $arVisual['VIDEO']['SHADOW']['USE'] ? 'Y' : 'N',
                                    'SHADOW_COLOR' => $arVisual['VIDEO']['SHADOW']['COLOR']['VALUE'],
                                    'SHADOW_COLOR_CUSTOM' => $arVisual['VIDEO']['SHADOW']['COLOR']['CUSTOM'],
                                    'SHADOW_OPACITY' => $arVisual['VIDEO']['SHADOW']['OPACITY']
                                ),
                                $component
                            ) ?>
                        </div>
                    <?php } ?>
                    <div class="widget-element-wrapper intec-content">
                        <div class="widget-element-wrapper-2 intec-content-wrapper">
                            <div class="widget-element-wrapper-3" id="<?= $sAreaId ?>">
                                <div class="widget-element-wrapper-4<?= !empty($arSlide['COLOR']) ? ' slide-'.$arSlide['COLOR'] : '' ?>">
                                    <div class="widget-element-content">
                                        <?php if ($arVisual['HEADER']['SHOW'] && !empty($arHeader['TEXT'])) { ?>
                                            <div class="widget-element-header">
                                                <?= Html::tag('div', $arHeader['TEXT'], [ /** Заголовок элемента */
                                                    'style' => ['color' => $arHeader['COLOR']]
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($arVisual['DESCRIPTION']['SHOW'] && !empty($arDescription['TEXT'])) { ?>
                                            <div class="widget-element-description">
                                                <?= Html::tag('div', $arDescription['TEXT'], [ /** Описание элемента */
                                                    'style' => ['color' => $arDescription['COLOR']]
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($arButton['SHOW'] && !empty($arLink['VALUE'])) { ?>
                                            <div class="widget-element-button-wrap">
                                                <?= Html::tag('a', $arButton['TEXT']['VALUE'], [ /** Тег кнопки элемента */
                                                    'class' => Html::cssClassFromArray([
                                                        'widget-element-button' => true,
                                                        'intec-cl-background' => empty($arButton['COLOR']),
                                                        'intec-cl-background-light-hover' => empty($arButton['COLOR'])

                                                    ], true),
                                                    'href' => $arLink['VALUE'],
                                                    'target' => $arLink['BLANK'] ? '_blank' : null,
                                                    'style' => [
                                                        'color' => $arButton['TEXT']['COLOR'],
                                                        'background' => $arButton['COLOR']
                                                    ]
                                                ]) ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php if ($arVisual['INSERTIONS']['SHOW'] && !empty($arInsertions)) { ?>
                                        <div class="widget-text-block">
                                            <?php $iInsertionsCount = 0 ?>
                                            <?php foreach ($arInsertions as $arInsertion) { ?>
                                            <?php
                                                $iInsertionsCount++;

                                                if ($iInsertionsCount > $arVisual['INSERTIONS']['COUNT'])
                                                    break;
                                            ?>
                                                <div class="widget-text-element">
                                                    <?php if (!empty($arInsertion['HEADER'])) { ?>
                                                        <div class="widget-text-name">
                                                            <?= $arInsertion['HEADER'] ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (!empty($arInsertion['DESCRIPTION'])) { ?>
                                                        <div class="widget-text-description">
                                                            <?= $arInsertion['DESCRIPTION'] ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?= Html::endTag($arSlide['TAG']) ?>
            <?php } ?>
        <?= Html::endTag('div') ?>
    </div>
    <?php include(__DIR__.'/parts/script.php') ?>
</div>