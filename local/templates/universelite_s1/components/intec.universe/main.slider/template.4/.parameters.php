<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

if (!Loader::includeModule('iblock'))
    return;

$arPropertiesText = array();
$arPropertiesCheckbox = array();
$arPropertiesFile = array();
$arPropertiesList = array();

if (!empty($arCurrentValues['IBLOCK_ID'])) {
    /** Список свойств инфоблока */
    $rsProperties = CIBlockProperty::GetList(
        array(),
        array(
            'IBLOCK_ID' => $arCurrentValues['IBLOCK_ID']
        )
    );

    while ($arProperty = $rsProperties->Fetch()) {
        if ($arProperty['PROPERTY_TYPE'] == 'S' && $arProperty['LIST_TYPE'] == 'L') {
            $arPropertiesText[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        } elseif ($arProperty['PROPERTY_TYPE'] == 'L' && $arProperty['LIST_TYPE'] == 'C') {
            $arPropertiesCheckbox[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        } elseif ($arProperty['PROPERTY_TYPE'] == 'F' && $arProperty['LIST_TYPE'] == 'L') {
            $arPropertiesFile[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        } elseif ($arProperty['PROPERTY_TYPE'] == 'L' && $arProperty['LIST_TYPE'] == 'L') {
            $arPropertiesList[$arProperty['CODE']] = '[' . $arProperty['CODE'] . '] ' . $arProperty['NAME'];
        }
    }
}

$arTemplateParameters = array();

/** DATA_SOURCE */
if (!empty($arCurrentValues['IBLOCK_ID'])) {
    $arTemplateParameters = array(
        'PROPERTY_HEADER_COLOR' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_HEADER_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_DESCRIPTION_COLOR' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_DESCRIPTION_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_TEXT_POSITION' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_TEXT_POSITION'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesList,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_LINK' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_LINK'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_LINK_BLANK' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_LINK_BLANK'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesCheckbox,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_BUTTON_SHOW' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_BUTTON_SHOW'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesCheckbox,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_BUTTON_TEXT' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_BUTTON_TEXT'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_BUTTON_TEXT_COLOR' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_BUTTON_TEXT_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_BUTTON_COLOR' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_BUTTON_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_BANNER_COLOR' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_BANNER_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesList,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_INSERTIONS' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_INSERTIONS'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        ),
        'PROPERTY_VIDEO_URL' => array(
            'PARENT' => 'DATA_SOURCE',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_PROPERTY_VIDEO_URL'),
            'TYPE' => 'LIST',
            'VALUES' => $arPropertiesText,
            'ADDITIONAL_VALUES' => 'Y'
        )
    );
}

/** VISUAL */
$arTemplateParameters['HEIGHT'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SLIDER_TEMP4_HEIGHT'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        500 => '500px',
        550 => '550px',
        600 => '600px',
        650 => '650px',
        700 => '700px',
        750 => '750px',
        800 => '800px'
    ),
    'DEFAULT' => 650,
    'ADDITIONAL_VALUES' => 'Y'
);
$arTemplateParameters['INSERTIONS_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SLIDER_TEMP4_INSERTIONS_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['INSERTIONS_SHOW'] == 'Y') {
    $arTemplateParameters['INSERTIONS_COUNT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SLIDER_TEMP4_INSERTIONS_COUNT'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4'
        ),
        'DEFAULT' => 3
    );
}

if (!empty($arCurrentValues['PROPERTY_VIDEO_URL'])) {
    $arTemplateParameters['VIDEO_SHADOW_USE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SLIDER_TEMP4_VIDEO_SHADOW_USE'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N',
        'REFRESH' => 'Y'
    );

    if ($arCurrentValues['VIDEO_SHADOW_USE'] == 'Y') {
        $arTemplateParameters['VIDEO_SHADOW_COLOR'] = [
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_VIDEO_SHADOW_COLOR'),
            'TYPE' => 'LIST',
            'VALUES' => [
                'default' => Loc::getMessage('C_SLIDER_TEMP4_VIDEO_SHADOW_COLOR_DEFAULT'),
                'custom' => Loc::getMessage('C_SLIDER_TEMP4_VIDEO_SHADOW_COLOR_CUSTOM')
            ],
            'REFRESH' => 'Y'
        ];

        if ($arCurrentValues['VIDEO_SHADOW_COLOR'] == 'custom') {
            $arTemplateParameters['VIDEO_SHADOW_COLOR_CUSTOM'] = [
                'PARENT' => 'VISUAL',
                'NAME' => Loc::getMessage('C_SLIDER_TEMP4_VIDEO_SHADOW_COLOR_CUSTOM'),
                'TYPE' => 'STRING'
            ];
        }

        $arTemplateParameters['VIDEO_SHADOW_OPACITY'] = [
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_SLIDER_TEMP4_VIDEO_SHADOW_OPACITY'),
            'TYPE' => 'STRING',
            'DEFAULT' => '50'
        ];
    }
}

$arTemplateParameters['SLIDER_DOTS'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SLIDER_TEMP4_SLIDER_DOTS'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
);
$arTemplateParameters['SLIDER_LOOP'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SLIDER_TEMP4_SLIDER_LOOP'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N'
);
$arTemplateParameters['SLIDER_SPEED'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SLIDER_TEMP4_SLIDER_SPEED'),
    'TYPE' => 'STRING',
    'DEFAULT' => '500'
);
$arTemplateParameters['SLIDER_AUTO_USE'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_SLIDER_TEMP4_SLIDER_AUTO_USE'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['SLIDER_AUTO_USE'] == 'Y') {
    $arTemplateParameters['SLIDER_AUTO_TIME'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SLIDER_TEMP4_SLIDER_AUTO_TIME'),
        'TYPE' => 'STRING',
        'DEFAULT' => '10000'
    );
    $arTemplateParameters['SLIDER_AUTO_PAUSE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_SLIDER_TEMP4_SLIDER_AUTO_PAUSE'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N'
    );
}