<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;
use intec\core\helpers\Type;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arPropertyCodes = $arResult['PROPERTY_CODES'];

foreach ([
    'HEADER_COLOR',
    'DESCRIPTION_COLOR',
    'TEXT_POSITION',
    'LINK',
    'LINK_BLANK',
    'BUTTON_SHOW',
    'BUTTON_TEXT',
    'BUTTON_TEXT_COLOR',
    'BUTTON_COLOR',
    'BANNER_COLOR',
    'INSERTIONS',
    'VIDEO_URL'
] as $sCode) $arPropertyCodes[$sCode] = ArrayHelper::getValue(
    $arParams,
    'PROPERTY_'.$sCode
);

/** Настройки внешнего вида */
$sHeight = ArrayHelper::getValue($arParams, 'HEIGHT');
$sHeight = !empty($sHeight) ? $sHeight : 500;
$sHeight = StringHelper::replace($sHeight, ['px' => '']);

$sSelector = ArrayHelper::getValue($arParams, 'SELECTOR');
$sAttribute = ArrayHelper::getValue($arParams, 'ATTRIBUTE');
$sClass = ArrayHelper::getValue($arParams, 'CLASS');

if (empty($sSelector))
    $sSelector = null;

if (empty($sAttribute))
    $sAttribute = null;

$arInsertions = [
    'SHOW' => ArrayHelper::getValue($arParams, 'INSERTIONS_SHOW') == 'Y',
    'COUNT' => ArrayHelper::getValue($arParams, 'INSERTIONS_COUNT')
];

if (empty($arInsertions['COUNT']))
    $arInsertions['COUNT'] = 3;

$arSlider = [
    'USE' => false,
    'DOTS' => ArrayHelper::getValue($arParams, 'SLIDER_DOTS') == 'Y',
    'SPEED' => ArrayHelper::getValue($arParams, 'SLIDE_SPEED'),
    'LOOP' => ArrayHelper::getValue($arParams, 'SLIDE_LOOP') == 'Y',
    'AUTO' => [
        'USE' => ArrayHelper::getValue($arParams, 'SLIDE_AUTO_USE') == 'Y',
        'TIME' => ArrayHelper::getValue($arParams, 'SLIDE_AUTO_TIME'),
        'PAUSE' => ArrayHelper::getValue($arParams, 'SLIDE_AUTO_PAUSE') == 'Y'
    ]
];

$arSlider['USE'] = count($arResult['ITEMS']) > 1;

$arVideo = [
    'SHADOW' => [
        'USE' => ArrayHelper::getValue($arParams, 'VIDEO_SHADOW_USE') == 'Y',
        'COLOR' => [
            'VALUE' => ArrayHelper::getValue($arParams, 'VIDEO_SHADOW_COLOR'),
            'CUSTOM' => ArrayHelper::getValue($arParams, 'VIDEO_SHADOW_COLOR_CUSTOM'),
        ],
        'OPACITY' => ArrayHelper::getValue($arParams, 'VIDEO_SHADOW_OPACITY')
    ]
];

if (empty($arSlider['SPEED']))
    $arSlider['SPEED'] = 500;

if (empty($arSlider['AUTO']['TIME']))
    $arSlider['AUTO']['TIME'] = 10000;

$arResult['PROPERTY_CODES'] = $arPropertyCodes;
$arResult['VISUAL']['HEIGHT'] = $sHeight;
$arResult['VISUAL']['SELECTOR'] = $sSelector;
$arResult['VISUAL']['ATTRIBUTE'] = $sAttribute;
$arResult['VISUAL']['CLASS'] = $sClass;
$arResult['VISUAL']['INSERTIONS'] = $arInsertions;
$arResult['VISUAL']['SLIDER'] = $arSlider;
$arResult['VISUAL']['VIDEO'] = $arVideo;

foreach ($arResult['ITEMS'] as $sKey => $arItem) {
    foreach ($arPropertyCodes as $sCode) {
        if (!empty($sCode)) {
            $arProperty = ArrayHelper::getValue($arItem, ['PROPERTIES', $sCode]);

            if (!empty($arProperty)) {
                /** Текстовое поле */
                if ($arProperty['PROPERTY_TYPE'] == 'S' && $arProperty['LIST_TYPE'] == 'L') {
                    /** Текстовое поле с HTML */
                    if (Type::isArray($arProperty['VALUE'])) {
                        if (!empty($arProperty['VALUE']['TEXT'])) {
                            if ($arProperty['VALUE']['TYPE'] == 'HTML') {
                                $arProperty['VALUE'] = $arProperty['~VALUE']['TEXT'];
                            } else {
                                $arProperty['VALUE'] = $arProperty['VALUE']['TEXT'];
                            }
                        } else {
                            $arProperty['VALUE'] = null;
                        }

                        $arItem['PROPERTIES'][$sCode] = $arProperty;
                    }

                    /** Текстовое поле с описанием */
                    if (Type::isArray($arProperty['~VALUE']) && Type::isArray($arProperty['DESCRIPTION'])) {
                        $arSet = [];
                        foreach ($arProperty['~VALUE'] as $sKeyValue => $sValue) {
                            $sEncodeValue = Html::encode($sValue);

                            $arSet[] = [
                                'HEADER' => $sEncodeValue,
                                'DESCRIPTION' => ArrayHelper::getValue($arProperty, ['DESCRIPTION', $sKeyValue])
                            ];
                        }

                        $arProperty['VALUE'] = $arSet;
                        $arItem['PROPERTIES'][$sCode] = $arProperty;
                    }
                }

                /** Файл */
                if ($arProperty['PROPERTY_TYPE'] == 'F' && $arProperty['LIST_TYPE'] == 'L') {
                    if (!empty($arProperty['VALUE'])) {
                        $arPicture = CFile::ResizeImageGet(
                            $arProperty['VALUE'],
                            array(
                                'width' => 800,
                                'height' => 800
                            ),
                            BX_RESIZE_IMAGE_PROPORTIONAL_ALT
                        );

                        $arProperty['VALUE'] = $arPicture;
                        $arItem['PROPERTIES'][$sCode] = $arProperty;
                    }
                }
            }
        }

        $arResult['ITEMS'][$sKey] = $arItem;
    }
}