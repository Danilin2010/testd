<?php

$MESS['C_CERTIFICATES_TEMP1_GRID'] = 'Количество элементов в строке';
$MESS['C_CERTIFICATES_TEMP1_NAME_SHOW'] = 'Показывать название элементов';
$MESS['C_CERTIFICATES_TEMP1_FOOTER_SHOW'] = 'Показывать кнопку "Показать все"';
$MESS['C_CERTIFICATES_TEMP1_FOOTER_POSITION'] = 'Расположение кнопки "Показать все"';
$MESS['C_CERTIFICATES_TEMP1_FOOTER_TEXT'] = 'Текст кнопки "Показать все"';
$MESS['C_CERTIFICATES_TEMP1_LIST_PAGE'] = 'Ссылка на страницу раздела';

$MESS['C_CERTIFICATES_TEMP1_POSITION_LEFT'] = 'Слева';
$MESS['C_CERTIFICATES_TEMP1_POSITION_CENTER'] = 'По центру';
$MESS['C_CERTIFICATES_TEMP1_POSITION_RIGHT'] = 'Справа';

$MESS['C_CERTIFICATES_TEMP1_FOOTER_TEXT_DEFAULT'] = 'Показать все';