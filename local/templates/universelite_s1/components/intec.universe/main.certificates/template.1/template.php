<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\JavaScript;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arFooter = $arResult['FOOTER_BLOCK'];
$arVisual = $arResult['VISUAL'];

?>
<div class="intec-content">
    <div class="intec-content-wrapper">
        <div class="widget c-certificates c-certificates-template-1" id="<?= $sTemplateId ?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [
                'class' => [
                    'widget-content',
                    'intec-grid' => [
                        '',
                        'wrap',
                        'a-v-start',
                        'a-h-center'
                    ]
                ]
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sName = ArrayHelper::getValue($arItem, 'NAME');
                    $arPicture = [
                        'SMALL' => ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC_SMALL']),
                        'ICON' => ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC_ICON']),
                        'BIG' => ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC'])
                    ];

                ?>
                    <?= Html::beginTag('div', [
                        'class' => Html::cssClassFromArray([
                            'widget-element-wrap' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1000-4' => $arVisual['LINE_COUNT'] >= 5,
                                '750-3' => $arVisual['LINE_COUNT'] >= 4,
                                '550-2' => $arVisual['LINE_COUNT'] >= 3,
                                '350-1' => true
                            ]
                        ], true),
                        'id' => $sAreaId
                    ]) ?>
                        <div class="widget-element">
                            <?= Html::beginTag('div', [ /** Обертка для изображения элемента */
                                'class' => 'widget-element-picture-wrap',
                                'style' => [
                                    'background-image' => 'url('.$arPicture['SMALL'].')'
                                ],
                                'data-src' => $arPicture['BIG']
                            ]) ?>
                                <?= Html::img($arPicture['ICON'], ['alt' => $sName, 'title' => $sName]) ?>
                            <?= Html::endTag('div') ?>
                            <?php if ($arVisual['NAME_SHOW']) { ?>
                                <div class="widget-element-name">
                                    <?= $sName ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
            <?php if ($arFooter['SHOW']) { ?>
                <div class="widget-footer align-<?= $arFooter['POSITION'] ?>">
                    <a class="widget-footer-all intec-cl-border intec-cl-background-hover" href="<?= $arFooter['LIST_PAGE'] ?>">
                        <?= $arFooter['TEXT'] ?>
                    </a>
                </div>
            <?php } ?>
        </div>
        <?php if (!defined('EDITOR')) { ?>
            <script>
                (function ($, api) {
                    $(document).ready(function () {
                        var root = $(<?= JavaScript::toObject('#'.$sTemplateId) ?>);

                        $('.widget-content', root).lightGallery({
                            selector: '.widget-element-picture-wrap'
                        });
                    });
                })(jQuery, intec)
            </script>
        <?php } ?>
    </div>
</div>
