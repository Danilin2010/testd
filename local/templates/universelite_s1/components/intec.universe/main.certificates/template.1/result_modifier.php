<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;

/**
 * @var array $arResult
 * @var array $arParams
 */

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/',
    'TEMPLATE_PATH' => $this->GetFolder().'/'
];

/** Настройки отображения */
$iLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($iLineCount <= 3)
    $iLineCount = 3;

if ($iLineCount >= 5)
    $iLineCount = 5;

$arResult['VISUAL'] = [
    'LINE_COUNT' => $iLineCount,
    'NAME_SHOW' => ArrayHelper::getValue($arParams, 'NAME_SHOW')
];

$sFooterText = ArrayHelper::getValue($arParams, 'FOOTER_TEXT');
$sFooterText = trim($sFooterText);
$sListPage = ArrayHelper::getValue($arParams, 'LIST_PAGE');
$sListPage = StringHelper::replaceMacros($sListPage, $arMacros);
$bFooterShow = ArrayHelper::getValue($arParams, 'FOOTER_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sFooterText) && !empty($sListPage);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'FOOTER_POSITION'),
    'TEXT' => Html::encode($sFooterText),
    'LIST_PAGE' => $sListPage
];

$arResize = [
    3 => ['width' => 600, 'height' => 600],
    4 => ['width' => 500, 'height' => 500],
    5 => ['width' => 400, 'height' => 400]
];

foreach ($arResult['ITEMS'] as $iKey => $arItem) {
    $arPreviewPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');
    $arDetailPicture = ArrayHelper::getValue($arItem, 'DETAIL_PICTURE');
    $sNoImage = SITE_TEMPLATE_PATH.'/images/noimg/noimg_vertical.jpg';

    if (empty($arPreviewPicture) && !empty($arDetailPicture)) {
        $arPreviewPicture = $arDetailPicture;
    }

    if (empty($arPreviewPicture) && empty($arDetailPicture)) {
        $arPreviewPicture['SRC'] = $sNoImage;
        $arPreviewPicture['SRC_SMALL'] = $sNoImage;
        $arPreviewPicture['SRC_ICON'] = $sNoImage;
    } else if (!empty($arPreviewPicture)) {
        $arPicture = CFile::ResizeImageGet(
            $arPreviewPicture,
            $arResize[$arResult['VISUAL']['LINE_COUNT']],
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT
        );

        $arIcon = CFile::ResizeImageGet(
            $arPreviewPicture,
            array(
                'width' => 150,
                'height' => 150
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT
        );

        $arPreviewPicture['SRC_SMALL'] = $arPicture['src'];
        $arPreviewPicture['SRC_ICON'] = $arIcon['src'];
    }

    $arItem['PREVIEW_PICTURE'] = $arPreviewPicture;

    $arResult['ITEMS'][$iKey] = $arItem;
}