<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\JavaScript;

/**
 * @var array $arResult
 */

$this->setFrameMode(true);

$sTemplateId = spl_object_hash($this);

$arHeader = $arResult['HEADER_BLOCK'];
$arDescription = $arResult['DESCRIPTION_BLOCK'];
$arVisual = $arResult['VISUAL'];
$arFooter = $arResult['FOOTER_BLOCK'];
$arCodes = $arResult['PROPERTY_CODES'];

?>
<div class="intec-content">
    <div class="intec-content-wrapper">
        <div class="widget c-videos c-videos-template-1" id="<?= $sTemplateId ?>">
            <?php if ($arHeader['SHOW'] || $arDescription['SHOW']) { ?>
                <div class="widget-header">
                    <?php if ($arHeader['SHOW']) { ?>
                        <div class="widget-title align-<?= $arHeader['POSITION'] ?>">
                            <?= $arHeader['TEXT'] ?>
                        </div>
                    <?php } ?>
                    <?php if ($arDescription['SHOW']) { ?>
                        <div class="widget-description align-<?= $arDescription['POSITION'] ?>">
                            <?= $arDescription['TEXT'] ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?= Html::beginTag('div', [ /** Контентный блок */
                'class' => Html::cssClassFromArray([
                    'widget-content' => true,
                    'owl-carousel' => $arVisual['SLIDER']['USE'],
                    'intec-grid' => [
                        '' => !$arVisual['SLIDER']['USE'],
                        'wrap' => !$arVisual['SLIDER']['USE'],
                        'a-v-start' => !$arVisual['SLIDER']['USE'],
                        'a-h-center' => !$arVisual['SLIDER']['USE'],
                    ]
                ], true)
            ]) ?>
                <?php foreach ($arResult['ITEMS'] as $arItem) {

                    $sId = $sTemplateId.'_'.$arItem['ID'];
                    $sAreaId = $this->GetEditAreaId($sId);
                    $this->AddEditAction($sId, $arItem['EDIT_LINK']);
                    $this->AddDeleteAction($sId, $arItem['DELETE_LINK']);

                    $sBackground = ArrayHelper::getValue($arItem, ['PREVIEW_PICTURE', 'SRC']);
                    $arYoutube = ArrayHelper::getValue($arItem, ['PROPERTIES', $arCodes['PROPERTY_URL'], 'VALUE']);

                ?>
                    <?= Html::beginTag('div', [ /** Главный тег элемента */
                        'id' => $sAreaId,
                        'class' => Html::cssClassFromArray([
                            'widget-element-wrap' => true,
                            'intec-grid-item' => [
                                $arVisual['LINE_COUNT'] => true,
                                '1050-4' => !$arVisual['SLIDER']['USE'] && $arVisual['LINE_COUNT'] >= 5,
                                '900-3' => !$arVisual['SLIDER']['USE'] && $arVisual['LINE_COUNT'] >= 4,
                                '750-2' => !$arVisual['SLIDER']['USE'] && $arVisual['LINE_COUNT'] >= 3,
                                '550-1' => !$arVisual['SLIDER']['USE'] && $arVisual['LINE_COUNT'] >= 2
                            ]
                        ], true),
                        'data-src' => $arYoutube['iframe']
                    ]) ?>
                        <div class="widget-element-bitrix-fix">
                            <?= Html::beginTag('div', [ /** Картинка элемента */
                                'class' => 'widget-element',
                                'style' => [
                                    'background-image' => 'url('.$sBackground.')'
                                ]
                            ]) ?>
                                <div class="widget-element-fade"></div>
                                <div class="widget-element-decoration">
                                    <div class="widget-element-decoration-wrapper">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="widget-element-decoration-icon">
                                            <path d="M216 354.9V157.1c0-10.7 13-16.1 20.5-8.5l98.3 98.9c4.7 4.7 4.7 12.2 0 16.9l-98.3 98.9c-7.5 7.7-20.5 2.3-20.5-8.4zM256 8c137 0 248 111 248 248S393 504 256 504 8 393 8 256 119 8 256 8zm0 48C145.5 56 56 145.5 56 256s89.5 200 200 200 200-89.5 200-200S366.5 56 256 56z"></path>
                                        </svg>
                                    </div>
                                </div>
                            <?= Html::endTag('div') ?>
                        </div>
                    <?= Html::endTag('div') ?>
                <?php } ?>
            <?= Html::endTag('div') ?>
            <?php if ($arFooter['SHOW']) { ?>
                <div class="widget-footer align-<?= $arFooter['POSITION'] ?>">
                    <a class="widget-footer-all intec-cl-border intec-cl-background-hover" href="<?= $arFooter['URL'] ?>">
                        <?= $arFooter['TEXT'] ?>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php if ($arVisual['SLIDER']['USE']) include(__DIR__.'/script.php') ?>