<?php

$MESS['C_VIDEOS_TEMP1_IMAGE_QUALITY'] = 'Качество картинки, загружаемой с YouTube';
$MESS['C_VIDEOS_TEMP1_LINE_COUNT'] = 'Количество элементов в строке';
$MESS['C_VIDEOS_TEMP1_ELEMENT_NAME_SHOW'] = 'Показывать название видео';
$MESS['C_VIDEOS_TEMP1_ELEMENT_DESCRIPTION_SHOW'] = 'Показывать описание видео';
$MESS['C_VIDEOS_TEMP1_FOOTER_SHOW'] = 'Показывать кнопку "Смотреть все"';
$MESS['C_VIDEOS_TEMP1_FOOTER_POSITION'] = 'Расположение кнопки "Смотреть все"';
$MESS['C_VIDEOS_TEMP1_LIST_PAGE_URL'] = 'Путь до страницы списка раздела';
$MESS['C_VIDEOS_TEMP1_FOOTER_TEXT'] = 'Текст кнопки "Смотреть все"';
$MESS['C_VIDEOS_TEMP1_SLIDER_USE'] = 'Использовать слайдер';
$MESS['C_VIDEOS_TEMP1_SLIDER_LOOP_USE'] = 'Зациклить прокрутку слайдов';
$MESS['C_VIDEOS_TEMP1_SLIDER_AUTO_PLAY_USE'] = 'Использовать автопрокрутку';
$MESS['C_VIDEOS_TEMP1_SLIDER_AUTO_PLAY_TIME'] = 'Время задержки перед сменой слайдов (мс)';
$MESS['C_VIDEOS_TEMP1_SLIDER_AUTO_PLAY_SPEED'] = 'Скорость анимации автопрокрутки слайдов (мс)';
$MESS['C_VIDEOS_TEMP1_SLIDER_AUTO_PLAY_HOVER_PAUSE'] = 'Останавливать автопрокрутку при наведении курсора мыши на слайдер';

$MESS['C_VIDEOS_TEMP1_POSITION_LEFT'] = 'Слева';
$MESS['C_VIDEOS_TEMP1_POSITION_CENTER'] = 'По центру';
$MESS['C_VIDEOS_TEMP1_POSITION_RIGHT'] = 'Справа';

$MESS['C_VIDEOS_TEMP1_FOOTER_TEXT_DEFAULT'] = 'Смотреть все';

$MESS['C_VIDEOS_TEMP1_IMAGE_QUALITY_MQ'] = 'Минимальное';
$MESS['C_VIDEOS_TEMP1_IMAGE_QUALITY_HQ'] = 'Среднее';
$MESS['C_VIDEOS_TEMP1_IMAGE_QUALITY_SD'] = 'Высокое';
$MESS['C_VIDEOS_TEMP1_IMAGE_QUALITY_MAX'] = 'Максимальное';