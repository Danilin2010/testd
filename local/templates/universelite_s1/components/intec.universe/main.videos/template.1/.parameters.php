<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arCurrentValues
 */

/** VISUAL */
$arTemplateParameters['IMAGE_QUALITY'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_IMAGE_QUALITY'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        'mqdefault' => Loc::getMessage('C_VIDEOS_TEMP1_IMAGE_QUALITY_MQ'),
        'hqdefault' => Loc::getMessage('C_VIDEOS_TEMP1_IMAGE_QUALITY_HQ'),
        'sddefault' => Loc::getMessage('C_VIDEOS_TEMP1_IMAGE_QUALITY_SD'),
        'maxresdefault' => Loc::getMessage('C_VIDEOS_TEMP1_IMAGE_QUALITY_MAX')
    ),
    'DEFAULT' => 'sddefault'
);
$arTemplateParameters['LINE_COUNT'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_LINE_COUNT'),
    'TYPE' => 'LIST',
    'VALUES' => array(
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5'
    ),
    'DEFAULT' => 3
);
$arTemplateParameters['FOOTER_SHOW'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_FOOTER_SHOW'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['FOOTER_SHOW'] == 'Y') {
    $arTemplateParameters['FOOTER_POSITION'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_FOOTER_POSITION'),
        'TYPE' => 'LIST',
        'VALUES' => array(
            'left' => Loc::getMessage('C_VIDEOS_TEMP1_POSITION_LEFT'),
            'center' => Loc::getMessage('C_VIDEOS_TEMP1_POSITION_CENTER'),
            'right' => Loc::getMessage('C_VIDEOS_TEMP1_POSITION_RIGHT')
        ),
        'DEFAULT' => 'center'
    );
    $arTemplateParameters['LIST_PAGE_URL'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_LIST_PAGE_URL'),
        'TYPE' => 'STRING'
    );
    $arTemplateParameters['FOOTER_TEXT'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_FOOTER_TEXT'),
        'TYPE' => 'STRING',
        'DEFAULT' => Loc::getMessage('C_VIDEOS_TEMP1_FOOTER_TEXT_DEFAULT')
    );
}

$arTemplateParameters['SLIDER_USE'] = array(
    'PARENT' => 'VISUAL',
    'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_SLIDER_USE'),
    'TYPE' => 'CHECKBOX',
    'DEFAULT' => 'N',
    'REFRESH' => 'Y'
);

if ($arCurrentValues['SLIDER_USE'] == 'Y') {
    $arTemplateParameters['SLIDER_LOOP_USE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_SLIDER_LOOP_USE'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N'
    );
    $arTemplateParameters['SLIDER_AUTO_PLAY_USE'] = array(
        'PARENT' => 'VISUAL',
        'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_SLIDER_AUTO_PLAY_USE'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N',
        'REFRESH' => 'Y'
    );

    if ($arCurrentValues['SLIDER_AUTO_PLAY_USE'] == 'Y') {
        $arTemplateParameters['SLIDER_AUTO_PLAY_TIME'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_SLIDER_AUTO_PLAY_TIME'),
            'TYPE' => 'STRING',
            'DEFAULT' => '10000'
        );
        $arTemplateParameters['SLIDER_AUTO_PLAY_SPEED'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_SLIDER_AUTO_PLAY_SPEED'),
            'TYPE' => 'STRING',
            'DEFAULT' => '500'
        );
        $arTemplateParameters['SLIDER_AUTO_PLAY_HOVER_PAUSE'] = array(
            'PARENT' => 'VISUAL',
            'NAME' => Loc::getMessage('C_VIDEOS_TEMP1_SLIDER_AUTO_PLAY_HOVER_PAUSE'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'N'
        );
    }
}