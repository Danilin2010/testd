<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use intec\core\helpers\ArrayHelper;
use intec\core\helpers\Html;
use intec\core\helpers\StringHelper;

/**
 * @var array $arParams
 * @var array $arResult
 */

/** Кодирование в HTML-формат заголовка и описания */
$sHeaderText = ArrayHelper::getValue($arResult, ['HEADER_BLOCK', 'TEXT']);
$arResult['HEADER_BLOCK']['TEXT'] = Html::encode($sHeaderText);

$sDescriptionText = ArrayHelper::getValue($arResult, ['DESCRIPTION_BLOCK', 'TEXT']);
$arResult['DESCRIPTION_BLOCK']['TEXT'] = Html::encode($sDescriptionText);

/** Параметры кнопки */
$sFooterText = ArrayHelper::getValue($arParams, 'FOOTER_TEXT');
$sFooterText = !empty($sFooterText) ? $sFooterText : Loc::getMessage('C_VIDEOS_TEMP1_FOOTER_TEXT_DEFAULT');

$sListPageUrl = ArrayHelper::getValue($arParams, 'LIST_PAGE_URL');
$sListPageUrl = trim($sListPageUrl);
$sListPageUrl = StringHelper::replaceMacros($sListPageUrl, ['SITE_DIR' => SITE_DIR]);

$bFooterShow = ArrayHelper::getValue($arParams, 'FOOTER_SHOW');
$bFooterShow = $bFooterShow == 'Y' && !empty($sListPageUrl);

$arResult['FOOTER_BLOCK'] = [
    'SHOW' => $bFooterShow,
    'POSITION' => ArrayHelper::getValue($arParams, 'FOOTER_POSITION'),
    'TEXT' => Html::encode($sFooterText),
    'URL' => Html::encode($sListPageUrl)
];

/** Параметры отображение */
$sLineCount = ArrayHelper::getValue($arParams, 'LINE_COUNT');

if ($sLineCount <= 1)
    $sLineCount = 1;

if ($sLineCount >= 5)
    $sLineCount = 5;

$arResult['VISUAL'] = [
    'LINE_COUNT' => $sLineCount,
    'SLIDER' => [
        'USE' => ArrayHelper::getValue($arParams, 'SLIDER_USE') == 'Y',
        'ITEMS' => $sLineCount,
        'LOOP_USE' => ArrayHelper::getValue($arParams, 'SLIDER_LOOP_USE') == 'Y',
        'AUTO_PLAY_USE' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_PLAY_USE') == 'Y',
        'AUTO_PLAY_TIME' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_PLAY_TIME'),
        'AUTO_PLAY_SPEED' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_PLAY_SPEED', '500'),
        'AUTO_PLAY_HOVER_PAUSE' => ArrayHelper::getValue($arParams, 'SLIDER_AUTO_PLAY_HOVER_PAUSE') == 'Y'
    ]
];

/** Список кодов свойств */
$sPropertyUrl = ArrayHelper::getValue($arResult, ['PROPERTY_CODES', 'PROPERTY_URL']);
$sPropertyUseIBlockCode = ArrayHelper::getValue($arResult, ['PROPERTY_CODES', 'PROPERTY_IBLOCK_IMAGE']);
$sImageQuality = ArrayHelper::getValue($arParams, 'IMAGE_QUALITY');

foreach ($arResult['ITEMS'] as $iKey => $arItem) {
    /** Обработка ссылок на изображения по настройкам элемента */
    $arYoutube = ArrayHelper::getValue($arItem, ['PROPERTIES', $sPropertyUrl, 'VALUE']);
    $arPreviewPicture = ArrayHelper::getValue($arItem, 'PREVIEW_PICTURE');
    $bUseIBlockImage = ArrayHelper::getValue($arItem, ['PROPERTIES', $sPropertyUseIBlockCode, 'VALUE']) == 'Y';

    if (!$bUseIBlockImage) {
        $arItem['PREVIEW_PICTURE']['SRC'] = $arYoutube[$sImageQuality];
    } else if ($bUseIBlockImage && empty($arPreviewPicture)) {
        $arItem['PREVIEW_PICTURE']['SRC'] = $arYoutube[$sImageQuality];
    } else if ($bUseIBlockImage && !empty($arPreviewPicture)) {
        $arPicture = CFile::ResizeImageGet(
            $arPreviewPicture,
            array(
                'width' => 800,
                'height' => 800
            ),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT
        );

        $arItem['PREVIEW_PICTURE']['SRC'] = $arPicture['src'];
    }

    $arResult['ITEMS'][$iKey] = $arItem;
}