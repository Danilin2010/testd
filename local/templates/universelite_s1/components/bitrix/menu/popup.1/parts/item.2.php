<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die() ?>
<?php

/**
 * @var array $arItem
 * @var array|null $arParent
 * @var integer $iLevel
 * @var string $sName
 * @var string $sLink
 * @var array $arChildren
 * @var Closure $fRenderItem
 */

?>
<div class="menu-item menu-item-level-<?= $iLevel ?>" data-role="item" data-level="<?= $iLevel ?>">
    <a href="<?= $sLink ?>" class="menu-item-name intec-cl-text-hover">
        <?= $sName ?>
    </a>
</div>