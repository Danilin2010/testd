<?php

$MESS['C_MENU_HORIZONTAL_1_IBLOCK_TYPE'] = 'Тип инфоблока';
$MESS['C_MENU_HORIZONTAL_1_IBLOCK_ID'] = 'Инфоблок';
$MESS['C_MENU_HORIZONTAL_1_PROPERTY_IMAGE'] = 'Свойство "Изображение раздела" (Пользовательское свойство раздела типа "Файл")';
$MESS['C_MENU_HORIZONTAL_1_TRANSPARENT'] = 'Прозрачный фон';
$MESS['C_MENU_HORIZONTAL_1_DELIMITERS'] = 'Отображать разделители';

$MESS['C_MENU_HORIZONTAL_1_SECTION_VIEW'] = 'Вид меню инфоблока';
$MESS['C_MENU_HORIZONTAL_1_SECTION_VIEW_DEFAULT'] = 'По умолчанию';
$MESS['C_MENU_HORIZONTAL_1_SECTION_VIEW_IMAGES'] = 'С изображениями';
$MESS['C_MENU_HORIZONTAL_1_SECTION_VIEW_SUBSECTION_COUNT'] = 'Кол-во выводимых элементов раздела';

$MESS['C_MENU_HORIZONTAL_1_CATALOG_LINKS'] = 'Ссылки на каталоги';