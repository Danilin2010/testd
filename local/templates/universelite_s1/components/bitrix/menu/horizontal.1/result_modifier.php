<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die() ?>
<?php

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\StringHelper;
use intec\core\helpers\Type;

/**
 * @var array $arParams
 * @var array $arResult
 * CBitrixComponentTemplate $this
 */

if (!CModule::IncludeModule('intec.core'))
    return;

$arMacros = [
    'SITE_DIR' => SITE_DIR,
    'SITE_TEMPLATE_PATH' => SITE_TEMPLATE_PATH.'/'
];

$sPropertyImage = ArrayHelper::getValue($arParams, 'PROPERTY_IMAGE');

$sView = ArrayHelper::getValue($arParams, 'SECTION_VIEW');
$sView = ArrayHelper::fromRange(array('default', 'images'), $sView);
$arParams['SECTION_VIEW'] = $sView;

/**
 * @param array $arResult
 * @return array
 */
$fBuild = function ($arResult) {
    $bFirst = true;

    if (empty($arResult))
        return [];

    $fBuild = function () use (&$fBuild, &$bFirst, &$arResult) {
        $iLevel = null;
        $arItems = array();
        $arItem = null;

        if ($bFirst) {
            $arItem = reset($arResult);
            $bFirst = false;
        }

        while (true) {
            if ($arItem === null) {
                $arItem = next($arResult);

                if (empty($arItem))
                    break;
            }

            if ($iLevel === null)
                $iLevel = $arItem['DEPTH_LEVEL'];

            if ($arItem['DEPTH_LEVEL'] < $iLevel) {
                prev($arResult);
                break;
            }

            if ($arItem['IS_PARENT'] === true)
                $arItem['ITEMS'] = $fBuild();

            $arItems[] = $arItem;
            $arItem = null;
        }

        return $arItems;
    };

    return $fBuild();
};

if (!empty($sPropertyImage)) {
    foreach ($arResult as $sKey => $arItem) {
        if (!empty($arItem['PARAMS']['SECTION'])) {
            $arSection = $arItem['PARAMS']['SECTION'];

            if (!empty($arSection[$sPropertyImage])) {
                $arResult[$sKey]['IMAGE'] = CFile::GetFileArray($arSection[$sPropertyImage]);
            } elseif (!empty($arSection['PICTURE'])) {
                $arResult[$sKey]['IMAGE'] = CFile::GetFileArray($arSection['PICTURE']);
            }
        }
    }
} else {
    foreach ($arResult as $sKey => $arItem) {
        if (!empty($arItem['PARAMS']['SECTION'])) {
            $arSection = $arItem['PARAMS']['SECTION'];

            if (!empty($arSection['PICTURE'])) {
                $arResult[$sKey]['IMAGE'] = CFile::GetFileArray($arSection['PICTURE']);
            }
        }
    }
}

$arCatalogLinks = ArrayHelper::getValue($arParams, 'CATALOG_LINKS');

if (!empty($arCatalogLinks) && Type::isArrayable($arCatalogLinks)) {
    foreach ($arCatalogLinks as $sKey => $sCatalogLink)
        $arCatalogLinks[$sKey] = StringHelper::replaceMacros($sCatalogLink, $arMacros);

    foreach ($arResult as $sKey => $arItem)
        if (ArrayHelper::isIn(
            $arItem['LINK'],
            $arCatalogLinks
        )) $arResult[$sKey]['IS_CATALOG'] = 'Y';
}

$arResult = $fBuild($arResult);
