<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="intec-content"> <div class="intec-content-wrapper">
    <div class="registration_page">
    <div class="bx_registration_page">
<?/*
        <div class="registration-info">
            <div class="registration-info-text">
                <?echo $arResult["MESSAGE_TEXT"]?>
            </div>
        </div>
*/?>
<?//here you can place your own messages
	switch($arResult["MESSAGE_CODE"])
	{
	case "E01":
		?><? //When user not found
		break;
	case "E02":
		?><? //User was successfully authorized after confirmation
		break;
	case "E03":
		?><? //User already confirm his registration
		break;
	case "E04":
		?><? //Missed confirmation code
		break;
	case "E05":
		?><? //Confirmation code provided does not match stored one
		break;
	case "E06":
		?><? //Confirmation was successfull
		break;
	case "E07":
		?><? //Some error occured during confirmation
		break;
	}
?>
<?if($arResult["SHOW_FORM"]):?>
	<form method="post" action="<?echo $arResult["FORM_ACTION"]?>">


        <div class="form-group">
            <label><?=GetMessage("CT_BSAC_LOGIN")?></label>
            <input type="text" name="<?echo $arParams["LOGIN"]?>" maxlength="50" value="<?echo $arResult["LOGIN"]?>" size="17" class="form-control form-control-local" />
        </div>

        <div class="form-group">
            <label><?=GetMessage("CT_BSAC_CONFIRM_CODE")?></label>
            <input type="text" name="<?echo $arParams["CONFIRM_CODE"]?>" maxlength="50" value="<?echo $arResult["CONFIRM_CODE"]?>" size="17" class="form-control form-control-local" />
        </div>

        <br>
        <input type="submit" value="<?=GetMessage("CT_BSAC_CONFIRM")?>" class="reg-button intec-button intec-button-s-7 intec-button-cl-common" />
        <input type="hidden" name="<?echo $arParams["USER_ID"]?>" value="<?echo $arResult["USER_ID"]?>" />
        <div style="clear:both;"></div>
	</form>
<?elseif(!$USER->IsAuthorized()):?>
	<?$APPLICATION->IncludeComponent("bitrix:system.auth.authorize", "", array());?>
<?endif?>
        </div></div></div></div>
