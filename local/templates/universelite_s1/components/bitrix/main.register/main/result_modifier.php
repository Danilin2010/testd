<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arr=array(
    "EMAIL",
    "PERSONAL_PHONE",
    "LOGIN",
    "PASSWORD",
    "CONFIRM_PASSWORD",
);

foreach ($arr as $val)
{
    $key = array_search($val, $arResult["SHOW_FIELDS"]);
    if (false !== $key)
    {
        unset($arResult["SHOW_FIELDS"][$key]);
        $arResult["SHOW_FIELDS"][]=$val;
    }
}
