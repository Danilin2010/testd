<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
use intec\constructor\models\Build;
$oBuild = Build::getCurrent();
if (!empty($oBuild)) {
    $oPage = $oBuild->getPage();
    $oProperties = $oPage->getProperties();
    $personal_data = $oProperties->get('inform_about_processing_personal_data');
}

?>

    <div class="intec-content">

<?

if (count($arResult["ERRORS"]) > 0) {

    foreach ($arResult["ERRORS"] as $key => $error) {
        if (intval($key) == 0 && $key !== 0)
            $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
    }

    ShowError(implode("<br />", $arResult["ERRORS"]));
}
//echo "<pre>";print_r($arResult);echo "</pre>";
?>

    <div class="intec-content-wrapper">
        <div class="registration_page">
            <div class="bx_registration_page">

<?if($USER->IsAuthorized()){?>
    <p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
<?} else {?>

                <h4>
                    <?=GetMessage("AUTH_REGISTER")?>
                </h4>
                <div class="registration-info">
                    <div class="registration-info-text">
                        <?=GetMessage("RERISTRATION_DESCRIPTION");?>
                    </div>
                </div>
                <?
                if (count($arResult["ERRORS"]) > 0)
                ShowMessage($arParams["~AUTH_RESULT"]);
                if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) &&  $arParams["AUTH_RESULT"]["TYPE"] === "OK") {?>
                    <p><?echo GetMessage("AUTH_EMAIL_SENT")?></p>
                <?} else {?>
                    <?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
                        <p><?echo GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></p>
                    <?endif?>
                    <!--noindex-->
                    <form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform">
                        <?if (strlen($arResult["BACKURL"]) > 0):?>
                            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                        <?endif?>

                        <input type="hidden" name="REGISTER[LOGIN]" value="<?=\local\helpers\UUID::v4();?>" />

                        <div class="form-group">
                            <label><?=GetMessage("AUTH_NAME")?></label>
                            <input type="text" name="REGISTER[NAME]" maxlength="50" value="<?=$arResult["VALUES"]["NAME"]?>" class="form-control form-control-local" />
                        </div>

                        <div class="form-group">
                            <label><?=GetMessage("AUTH_LAST_NAME")?></label>
                            <input type="text" name="REGISTER[LAST_NAME]" maxlength="50" value="<?=$arResult["VALUES"]["LAST_NAME"]?>" class="form-control form-control-local" />
                        </div>

                        <div class="form-group">
                            <label><?=GetMessage("AUTH_SECOND_NAME")?></label>
                            <input type="text" name="REGISTER[SECOND_NAME]" maxlength="50" value="<?=$arResult["VALUES"]["SECOND_NAME"]?>" class="form-control form-control-local" />
                        </div>

                        <?/*
                        <div class="form-group">
                            <label>
                                <span class="starrequired">*</span>
                                <?=GetMessage("AUTH_LOGIN_MIN")?>
                            </label>
                            <input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" class="form-control form-control-local" />
                        </div>
                        */?>

                        <div class="form-group">
                            <label>
                                <span class="starrequired">*</span><?=GetMessage("AUTH_PASSWORD_REQ")?>
                            </label>
                            <input type="password" name="REGISTER[PASSWORD]" maxlength="50" value="<?=$arResult["VALUES"]["PASSWORD"]?>" class="form-control form-control-local" />
                        </div>
                        <div class="form-group">
                            <label>
                                <span class="starrequired">*</span><?=GetMessage("AUTH_CONFIRM")?>
                            </label>
                            <input type="password" name="REGISTER[CONFIRM_PASSWORD]" maxlength="50" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"]?>" class="form-control form-control-local" />
                        </div>
                        <div class="form-group">
                            <label>
                                <span class="starrequired">*</span><?=GetMessage("AUTH_EMAIL")?>
                            </label>
                            <input type="text" name="REGISTER[EMAIL]" maxlength="255" value="<?=$arResult["VALUES"]["EMAIL"]?>" class="form-control form-control-local" />
                        </div>
                        <!-- User properties -->
                        <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
                        <?=strLen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?>
                        <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
                        <?if ($arUserField["MANDATORY"]=="Y"):?><span class="required">*</span><?endif;?>
                        <?=$arUserField["EDIT_FORM_LABEL"]?>:</td><td>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:system.field.edit",
                                $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "bform"), null, array("HIDE_ICONS"=>"Y"));?>
                            <?endforeach;?>
                            <?endif;?>
                            <!-- /User properties -->
                            <!-- CAPTCHA -->
                            <?if ($arResult["USE_CAPTCHA"] == "Y"):?>
                                <div class="captcha form-group">
                                    <?=GetMessage("CAPTCHA_REGF_TITLE")?>
                                    <br>
                                    <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                                    <label>
                                        <span class="starrequired">*</span>
                                        <?=GetMessage("CAPTCHA_REGF_PROMT")?>:</label>
                                    <input type="text" name="captcha_word" maxlength="50" value="" class="form-control form-control-local" />
                                </div>
                            <?endif?>
                            <br>
                            <input type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" class="reg-button intec-button intec-button-s-7 intec-button-cl-common" />
                            <!-- /CAPTCHA -->
                            <?if($personal_data) { ?>
                                <div class="consent">
                                    <div class="intec-contest-checkbox checked"></div>
                                    <?=GetMessage("CONSENT")?>
                                </div>
                            <?}?>
                            <div style="clear:both;"></div>
                    </form>
                    <!--/noindex-->
                <?}?>
            </div>
            <script type="text/javascript">
                document.bform.USER_NAME.focus();
            </script>
            <?}?>
        </div>
    </div>
    </div>


<?/*


<?if($USER->IsAuthorized()){?>
    <p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
<?} else {?>
<div class="registration-block">
    <div class="h2"><?=GetMessage("AUTH_REGISTER");?></div>
    <div class="registration-info">
        <div class="registration-info-text">
            <?=GetMessage("RERISTRATION_DESCRIPTION");?>
        </div>
    </div>
    <div class="registration-form">
        <? if (count($arResult["ERRORS"]) > 0) {

            foreach ($arResult["ERRORS"] as $key => $error) {
                if (intval($key) == 0 && $key !== 0)
                    $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
            }

            ShowError(implode("<br />", $arResult["ERRORS"]));
        } elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y") {	?>
            <p>
                <?=GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?>
            </p>
        <?}?>
        <form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
            <? if($arResult["BACKURL"] <> '') { ?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?}?>
            <?foreach ($arResult["SHOW_FIELDS"] as $FIELD) { ?>
                <?if($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true) { ?>
                    <tr>
                        <td><?echo GetMessage("main_profile_time_zones_auto")?>
                            <?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?>
                                <span class="starrequired">*</span>
                            <?endif?>
                        </td>
                        <td>
                            <select
                                    name="REGISTER[AUTO_TIME_ZONE]"
                                    onchange="this.form.elements['REGISTER[TIME_ZONE]'].disabled=(this.value != 'N')">
                                <option value="">
                                    <?echo GetMessage("main_profile_time_zones_auto_def")?>
                                </option>
                                <option value="Y"<?=$arResult["VALUES"][$FIELD] == "Y" ? " selected=\"selected\"" : ""?>>
                                    <?echo GetMessage("main_profile_time_zones_auto_yes")?>
                                </option>
                                <option value="N"<?=$arResult["VALUES"][$FIELD] == "N" ? " selected=\"selected\"" : ""?>>
                                    <?echo GetMessage("main_profile_time_zones_auto_no")?>
                                </option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?echo GetMessage("main_profile_time_zones_zones")?>
                        </td>
                        <td>
                            <select name="REGISTER[TIME_ZONE]"<?if(!isset($_REQUEST["REGISTER"]["TIME_ZONE"])) echo 'disabled="disabled"'?>>
                                <?foreach($arResult["TIME_ZONE_LIST"] as $tz=>$tz_name){?>
                                    <option
                                            value="<?=htmlspecialcharsbx($tz)?>"
                                        <?=$arResult["VALUES"]["TIME_ZONE"] == $tz ? " selected=\"selected\"" : ""?>>
                                        <?=htmlspecialcharsbx($tz_name)?>
                                    </option>
                                <?}?>
                            </select>
                        </td>
                    </tr>
                <?} else {?>
                <?switch ($FIELD){
                case "PASSWORD":
                ?>
                    <div class="form-group required">
                        <label>
                            <?=GetMessage("REGISTER_FIELD_".$FIELD)?>
                        </label>
                        <br/>
                        <input
                                type="password"
                                name="REGISTER[<?=$FIELD?>]"
                                value="<?=$arResult["VALUES"][$FIELD]?>"
                                autocomplete="off"
                                class="form-control form-control-local"
                                style="height: 45px;"><br/>
                    </div>
                <?if($arResult["SECURE_AUTH"]) { ?>
                    <span class="bx-auth-secure"
                          id="bx_auth_secure"
                          title="<?echo GetMessage("AUTH_SECURE_NOTE")?>"
                          style="display:none">
										<div class="bx-auth-secure-icon"></div>
									</span>
                    <noscript>
										<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
											<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
										</span>
                    </noscript>

                    <script type="text/javascript">
                        document.getElementById('bx_auth_secure').style.display = 'inline-block';
                    </script>
                <?}
                break;

                case "CONFIRM_PASSWORD":?>
                    <div class="form-group <?=$arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == 'Y' ? 'required' : ''?>">
                        <label><?=GetMessage("REGISTER_FIELD_".$FIELD)?></label><br/>
                        <input
                                type="password"
                                name="REGISTER[<?=$FIELD?>]"
                                value="<?=$arResult["VALUES"][$FIELD]?>"
                                autocomplete="off"
                                class="form-control form-control-local"
                                style="height: 45px;">
                        <br/>
                    </div>
                    <?break;

                    case "PERSONAL_GENDER":
                        ?><select name="REGISTER[<?=$FIELD?>]">
                        <option value="">
                            <?=GetMessage("USER_DONT_KNOW")?>
                        </option>
                        <option value="M"<?=$arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : ""?>>
                            <?=GetMessage("USER_MALE")?>
                        </option>
                        <option value="F"<?=$arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : ""?>>
                            <?=GetMessage("USER_FEMALE")?>
                        </option>
                        </select><?
                        break;

                    case "PERSONAL_COUNTRY":
                    case "WORK_COUNTRY":
                        ?><select name="REGISTER[<?=$FIELD?>]"><?
                        foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value)
                        {
                            ?><option value="<?=$value?>"<?if ($value == $arResult["VALUES"][$FIELD]):?> selected="selected"<?endif?>><?=$arResult["COUNTRIES"]["reference"][$key]?></option>
                            <?
                        }
                        ?></select><?
                        break;

                    case "PERSONAL_PHOTO":
                case "WORK_LOGO":
                    ?><input size="30" type="file" name="REGISTER_FILES_<?=$FIELD?>" /><?
                break;

                case "PERSONAL_NOTES":
                case "WORK_NOTES":
                ?>
                    <textarea cols="30" rows="5" name="REGISTER[<?=$FIELD?>]">
                    <?=$arResult["VALUES"][$FIELD]?>
                    </textarea><?
                break;

                case "LOGIN":?>
                    <div class="form-group <?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><?= 'required'?><?endif?>" style="display: none;">
                        <label><?=GetMessage("REGISTER_FIELD_".$FIELD)?></label><br/>
                        <input
                                id="login-registration"
                                type="text"
                                name="REGISTER[<?=$FIELD?>]"
                                value="<?=$arResult["VALUES"][$FIELD]?>"
                                class="form-control form-control-local"
                                style="height: 45px;">
                        <br/>
                    </div><?
                break;

                case "EMAIL":?>
                    <div class="form-group <?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><?= 'required'?><?endif?>">
                        <label><?=GetMessage("REGISTER_FIELD_".$FIELD)?></label><br/>
                        <input
                                id="email-registration"
                                type="text"
                                name="REGISTER[<?=$FIELD?>]"
                                value="<?=$arResult["VALUES"][$FIELD]?>"
                                class="form-control form-control-local" style="height: 45px;">
                        <br/>
                    </div><?
                break;
                break;
                default:?>
                    <div class="form-group <?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><?= 'required'?><?endif?>">
                        <label><?=GetMessage("REGISTER_FIELD_".$FIELD)?></label><br/>
                        <input type="text" name="REGISTER[<?=$FIELD?>]"
                               value="<?=$arResult["VALUES"][$FIELD]?>"
                               class="form-control form-control-local" style="height: 45px;">
                        <br/>
                    </div>
                <?}?>
                <?}?>
            <?}?>
            <?// ********************* User properties ***************************************************?>
            <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"){?>
                <tr>
                    <td colspan="2">
                        <?=strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?>
                    </td>
                </tr>
                <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField){?>
                    <tr>
                        <td>
                            <?=$arUserField["EDIT_FORM_LABEL"]?>:<?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;?>
                        </td>
                        <td>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:system.field.edit",
                                $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                array(
                                    "bVarsFromForm" => $arResult["bVarsFromForm"],
                                    "arUserField" => $arUserField,
                                    "form_name" => "regform"),
                                null,
                                array("HIDE_ICONS"=>"Y")
                            );?>
                        </td>
                    </tr>
                <?}?>
            <?}?>
            <?// ******************** /User properties ***************************************************?>

            <div class="checkbox">
                <input type="checkbox" id="agree-checkbox" checked disabled readonly>
                <label for="agree-checkbox">
                    <?=GetMessage("AGREE")?>
                    <a class="popup_agree">
                        <?=GetMessage("PROCESSING_OF_PERSONAL_DATA")?>
                    </a>
                </label>
            </div>

            <div class="input-submit">
                <input type="submit" id="register_submit" name="register_submit_button" class="intec-button intec-button-s-7 intec-button-cl-common" value="������������������" style="font-weight: 400;">
            </div>
        </form>
        <?}?>
    </div>

    <script>
        $(document).ready(function(){
            $(".close-popup-layer").click(function(){
                $("#popup_agree").hide();
            });
            $(".popup_agree").click(function(){
                $("#popup_agree").show();
            })
        })
    </script>

    <div id="popup_agree" class="overlay" style="display:none;">
        <div class="popup">
            <div class="content">
                <div class="h4"><?=GetMessage("REG_TITLE_POPUP");?></div>
                <hr>
                <p><?=GetMessage("REG_POPUP_MESSAGE");?></p>
            </div>
            <a class="close close-popup-layer">&times;</a>
        </div>
    </div>*/?>