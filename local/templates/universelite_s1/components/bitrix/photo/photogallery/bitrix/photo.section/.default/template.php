<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use intec\core\helpers\ArrayHelper;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewParams = ArrayHelper::getValue($arResult, 'VIEW_PARAMETERS');
$bUserAccess = ArrayHelper::getValue($arResult, 'USER_HAVE_ACCESS');

?>
<div class="photo-section photo-section-default">
    <?php  if ($bUserAccess){ ?>
        <div class="photo-section-wrapper intec-grid intec-grid-wrap intec-grid-a-v-start intec-grid-a-h-start">
            <?php foreach($arResult["ITEMS"] as $arItem){

                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"));

                $arImage = ArrayHelper::getValue($arItem, 'PICTURE');
                ?>
                <?php if (is_array($arImage)){ ?>
                    <div class="intec-grid-item-<?= $arViewParams['LINE_ELEMENT_COUNT'] ?> intec-grid-item-1000-3 intec-grid-item-768-2 intec-grid-item-550-1" data-src="<?= $arImage["SRC"] ?>">
                        <div class="section-item" id="<?= $this->GetEditAreaId($arItem['ID']) ?>">
                            <div class="section-item-wrapper" style="background-image: url('<?= $arImage["SRC"] ?>');">
                                <img class="section-item-img"
                                     src="<?= $arImage["SRC"] ?>"
                                     alt="<?= $arImage["ALT"] ?>"
                                     title="<?= $arImage["TITLE"] ?>"
                                />
                                <span class="section-item-search-plus fa fa-search-plus"></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if (count($arResult["ITEMS"])){ ?>
        <div class="btn-block">
            <a href="<?= $arParams["SECTION_TOP_URL"] ?>"
               class="intec-button intec-button-s-2 intec-button-cl-default intec-button-transparent list-sections">
                <span class="list-sections-read-more-angle fa fa-angle-left"></span>
                <span class="btn-list-sections"><?= GetMessage('CT_BPS_ELEMENT_RETURN_ALBUM');?></span>
            </a>
        </div>
    <?php } ?>
</div>
<script>
    $('.photo-section-wrapper').lightGallery({
        animateThumb: false,
        thumbnail: true
    });
</script>