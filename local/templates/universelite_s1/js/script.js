$(document).ready(function(){

    $('#feedback_form').on('submit',function (e) {
        e.preventDefault();

        var data=$(this).serialize();
        var url=$(this).attr('action');
        var method=$(this).attr('method');

        $('#feedback_form').hide();

        $.ajax({
            url: url,
            data:data,
            method:method,
            success: function(){

            }
        });

        //console.log(data);

        return false;
    });

});