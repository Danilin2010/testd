<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;
?>
<?if($arResult["QUESTIONING"]){?>
<div class="test">
	<h3>
		<?=$arResult["TEST"]["NAME"]?>
	</h3>
	<?if(is_array($arResult["TEST"]["PICTURE"])):?>
		<img class="detail_picture" border="0" src="<?=$arResult["TEST"]["PICTURE"]["SRC"]?>" width="<?=$arResult["TEST"]["PICTURE"]["WIDTH"]?>" height="<?=$arResult["TEST"]["PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["TEST"]["NAME"]?>"  title="<?=$arResult["TEST"]["NAME"]?>" />
	<?endif?>
	<p><?echo $arResult["TEST"]["~DESCRIPTION"];?></p>
    <div class="test_item_maximum_points"><b>ID: <?=$arResult["QUESTIONING"]["ID"];?></b></div>
	<div class="test_item_maximum_points"><b><?=GetMessage("DATE_START")?>: <?=$arResult["QUESTIONING"]["DATE_START"];?></b></div>
	<?if($arResult["QUESTIONING"]["DATE_STOP"]){?>
		<div class="test_item_maximum_points"><b><?=GetMessage("DATE_STOP")?>: <?=$arResult["QUESTIONING"]["DATE_STOP"];?></b></div>
	<?}?>
	<?if($arResult["QUESTIONING"]["DURATION"]){?>
		<div class="test_item_maximum_points"><b><?=GetMessage("DURATION")?>: <?=$arResult["QUESTIONING"]["DURATION"];?></b></div>
	<?}?>
	<div class="test_item_maximum_points"><b><?=GetMessage("MAXIMUM_POINTS")?>: <?=$arResult["QUESTIONING"]["SCORES"];?></b></div>
	<div class="test_item_maximum_points"><b><?=GetMessage("FINAL")?>: <?=GetMessage("AT_ACTIVE_".$arResult["QUESTIONING"]["FINAL"]);?></b></div>
	<?if($arResult["QUESTIONING"]["RESULT_NAME"]){?>
		<div class="test_item_maximum_points"><b><?=GetMessage("RESULT_NAME")?>: <?=$arResult["QUESTIONING"]["RESULT_NAME"];?></b></div>
	<?}?>
</div>
<?}?>
<div class="test_list">
<?/*if(count($arResult["ITEMS"])>0){?>
	<table>
		<tr class="test_item">
			<th><?=GetMessage("TABLE_QUESTION")?></th><th><?=GetMessage("TABLE_SCORE")?></th><th><?=GetMessage("TABLE_ANSWER")?></th>
			<?if($arResult["TEST"]["SHOW_ANSWERS"]=="Y"){?>
				<th><?=GetMessage("SHOW_ANSWERS")?></th>
			<?}?>
		</tr>
	<?foreach($arResult["ITEMS"] as $item){?>
		<tr class="test_item">
			<td><?=$item["QUESTION_NAME"]?></td><td align="center"><?=$item["SCORES"]?></td><td><?=$item["SERIALIZED_RESULT"]?></td>
			<?if($arResult["TEST"]["SHOW_ANSWERS"]=="Y"){?>
				<td><?=$item["ANSWERS"]?></td>
			<?}?>
		</tr>
	<?}?>
	</table>
<?}else{?>
	<div class="test_item">
		<?=GetMessage("NO_TEST")?>
	</div>
<?}*/?>
	<?if($arResult["TEST"]["TEST_URL"]){?>
	<div class="test_repeated">
		<a href="<?=$arResult["TEST"]["TEST_URL"]?>" title="<?=GetMessage("TEST_URL")?>"><?=GetMessage("TEST_URL")?></a>
	</div>
	<?}?>
	<?if($arResult["TEST"]["SHOW_REPEATED"]=="Y"){?>
	<div class="test_repeated">
		<a href="<?=$arResult["TEST"]["REPEATED_URL"]?>" title="<?=GetMessage("TEST_REPEATED")?>"><?=GetMessage("TEST_REPEATED")?></a>
	</div>
	<?}?>
</div>
<?
global $USER;

if($arResult["QUESTIONING"]>0){


    \Bitrix\Main\Loader::includeModule("iblock");

    $arSelect = ["ID", "IBLOCK_ID", "NAME","PROPERTY_QUESTIONING"];
    $arFilter = ["IBLOCK_ID"=>IBLOCK_FEEDBACK,"ACTIVE"=>"Y",'PROPERTY_QUESTIONING_VALUE'=>$arResult["QUESTIONING"]["ID"]];
    $res = CIBlockElement::GetList([], $arFilter, false, Array("nPageSize"=>1), $arSelect);
if(!$ob = $res->GetNext() && $USER->GetID()==$arResult["PROFAIL_ID"]["USER_ID"]) {
    ?>
    <br/><br/>
    <form action="/ajax/feedback.php" method="post" id="feedback_form">

        <h2>Оставить отзыв о тесте</h2>

        <div class="startshop-forms-result-new-wrapper">
            <input type="hidden" name="questioning" value="<?=$arResult["QUESTIONING"]["ID"]?>">
            <input type="hidden" name="questioning_name" value="<?=$arResult["TEST"]["NAME"]?>">

            <div class="submit-block">
                <div class="startshop-forms-result-new-row-name startshop-forms-result-new-table-cell-name">
                    Актуальность
                </div>
                <div class="startshop-forms-result-new-row-control">
                    <div class="consent">
                        <label class="intec-input">
                            <input name="relevance" type="radio" value="1">
                            <label class="intec-input-text">
                                1
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="relevance" type="radio" value="2">
                            <label class="intec-input-text">
                                2
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="relevance" type="radio" value="3">
                            <label class="intec-input-text">
                                3
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="relevance" type="radio" value="4">
                            <label class="intec-input-text">
                                4
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="relevance" type="radio" value="5">
                            <label class="intec-input-text">
                                5
                            </label>
                        </label>
                    </div>
                </div>
            </div>


            <div class="submit-block">
                <div class="startshop-forms-result-new-row-name startshop-forms-result-new-table-cell-name">
                    Качество
                </div>
                <div class="startshop-forms-result-new-row-control">
                    <div class="consent">
                        <label class="intec-input">
                            <input name="quality" type="radio" value="1">
                            <label class="intec-input-text">
                                1
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="quality" type="radio" value="2">
                            <label class="intec-input-text">
                                2
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="quality" type="radio"  value="3">
                            <label class="intec-input-text">
                                3
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="quality" type="radio"  value="4">
                            <label class="intec-input-text">
                                4
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="quality" type="radio" value="5">
                            <label class="intec-input-text">
                                5
                            </label>
                        </label>
                    </div>
                </div>
            </div>

            <div class="submit-block">
                <div class="startshop-forms-result-new-row-name startshop-forms-result-new-table-cell-name">
                    Сложность
                </div>
                <div class="startshop-forms-result-new-row-control">
                    <div class="consent">
                        <label class="intec-input">
                            <input name="complexity" type="radio" value="1">
                            <label class="intec-input-text">
                                1
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="complexity" type="radio" value="2">
                            <label class="intec-input-text">
                                2
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="complexity" type="radio" value="3">
                            <label class="intec-input-text">
                                3
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="complexity" type="radio" value="4">
                            <label class="intec-input-text">
                                4
                            </label>
                        </label>
                        <label class="intec-input">
                            <input name="complexity" type="radio" value="5">
                            <label class="intec-input-text">
                                5
                            </label>
                        </label>
                    </div>
                </div>
            </div>

            <div class="startshop-forms-result-new-row">
                <div class="startshop-forms-result-new-row-name startshop-forms-result-new-table-cell-name">
                    Отзыв
                </div>
                <div class="startshop-forms-result-new-row-control">
                    <textarea class="intec-input intec-input-block" name="detail_text"></textarea>
                </div>
            </div>

            <div class="submit-block">
                <div style="margin-top: 9px;">
                    <input type="submit" class="intec-button intec-button-cl-common intec-button-s-5" value="Оставить отзыв">
                </div>
            </div>
        </div>


    </form>

    <br/><br/><?
}
    ?>


    <?
$ServicesCertificate=new local\services\ServicesCertificate();
$Certificate=$ServicesCertificate->chekResultTest($arResult["PROFAIL_ID"]["USER_ID"],$arResult["QUESTIONING"]["RESULT_DESCRIPTION"],$arResult["TEST"]["NAME"],$arResult["QUESTIONING_ID"]);
if($Certificate){?>
    <?$url=$Certificate->getUrl();?>
    <a href="<?=$url?>" target="_blank">Получить сертификат</a>
<?}else{?>
    <?

    if(strripos($arResult["QUESTIONING"]["RESULT_DESCRIPTION"],"тест пройден")!==FALSE){
        $request = Application::getInstance()->getContext()->getRequest();

        $uriString = $request->getRequestUri();
        $uri = new Uri($uriString);
        $uri->deleteParams(["clear_cache"]);


        if($arResult["QUESTIONING"]["ID"]>0){
            $uri->addParams(["add_questioning" => $arResult["QUESTIONING"]["ID"]]);
        }

        $uriProfile=new Uri("/personal/profile/");
        $uriProfile->addParams(["backurl" => $uri->getUri()]);

        $uriRegister=new Uri("/personal/profile/");
        $uriRegister->addParams(["backurl" => $uri->getUri()]);
        $uriRegister->addParams(["register" => "yes"]);

        ?>
        <a href="<?=$uriProfile->getUri()?>" class="intec-button intec-button-cl-common intec-button-lg registration_button solid_button">Войти</a>
        или
        <a href="<?=$uriRegister->getUri()?>" class="intec-button intec-button-cl-common intec-button-lg registration_button solid_button">Зарегистрироваться</a>
        для получения сертификата
        <?
    }
    ?>
<?}

}else{

    global $USER;


    $request = Application::getInstance()->getContext()->getRequest();

    $uriString = $request->getRequestUri();
    $uri = new Uri($uriString);
    $uri->deleteParams(["clear_cache"]);

    $add_questioning = (int)$request->get("add_questioning");

    if($USER->IsAuthorized() && $add_questioning>0){



        $module_id="aelita.test";
        if(CModule::IncludeModule($module_id)){


            $el=new AelitaTestQuestioning();

            $arFields=array("ID"=>$add_questioning);
            $res = $el->GetList(array("ID"=>"DESC"),$arFields,false,array("nPageSize"=>1),[
                "ID",
                "PROFILE_ID",
                "TEST_ID",
                "RESULT_ID",
                "CLOSED",
                "FINAL",
                "DATE_START",
                "DATE_STOP",
                "DURATION",
                "GLASSES_ID",
                "STEP_MULTIPLE",
                "USER_ID",
            ]);
            if($arr=$res->GetNext())
            {
                if($arr["USER_ID"]<=0){
                    $UserId=$USER->GetID();
                    if($UserId>0){
                        $ProfailId=AelitaTestTools::GetIDProfail();
                        if($ProfailId["ID"]>0 && $arr["PROFILE_ID"]!=$ProfailId["ID"]){
                            $el->Update($arr["ID"],[
                                "PROFILE_ID"=>$ProfailId["ID"],
                            ]);
                            $uri->deleteParams(["add_questioning"]);
                            LocalRedirect($uri->getUri());
                        }
                    }
                }

            }

        }

    }

}?>
