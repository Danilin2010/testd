<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use local\services\typeCommerce;

foreach($arResult["ITEMS"] as &$arItem){
    $arItem["TypeCommerce"]=typeCommerce::getTypeCommerce($arItem["ID"]);
}unset($arItem);
