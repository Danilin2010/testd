<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->IncludeComponent("aelita:test.profile.questioning", "quiz", Array(
	"TEST_GROUP" => "",	// Группа теста
		"TEST_ID" => $arResult["TEST"]["ID"],	// ID теста
		"QUESTIONING_ID" => $arResult["QUESTIONING"]["ID"],	// ID попытки
		"ADD_TEST_CHAIN" => "N",	// Добавлять тест в цепочку навигации
		"SET_TITLE_TEST" => "N",	// Устанавливать заголовок из имени теста
		"ADD_QUESTIONING_CHAIN" => "N",	// Добавлять попытка дата в цепочку навигации
		"SET_TITLE_QUESTIONING" => "N",	// Устанавливать заголовок из попытка дата
		"TEST_URL" => "",	// Адрес теста
		"DETAIL_URL" => $arParams["QUESTIONING_URL"],	// Адрес попытки
		"REPEATED_URL" => "",	// Адрес повторного прохождения теста
		"NON_CHECK_USER" => "Y",	// Показывать не только респонденту
		"SHOW_LINK" => "Y",	// Показывать ссылку на результат
	),
	false
);?>

    <form action="<?//=POST_FORM_ACTION_URI?>?testaction=Y" method="post" enctype="multipart/form-data">
        <input type="hidden" name="reinitquestioning" value="Y">
        <input type="submit" name="testsubmit" value="<?echo GetMessage("INIT_QUESTIONING")?>">
    </form>

<?
AelitaTestTools::CloseQuestioning($arResult["PROFAIL_ID"]["ID"],$arResult["TEST"]["ID"]);
$url="/personal/profile/test/{$arResult["TEST"]["ID"]}/{$arResult["QUESTIONING"]["ID"]}/";
LocalRedirect($url);
?>

