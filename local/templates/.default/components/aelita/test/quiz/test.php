<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use local\services\typeCommerce;
use local\services\testService;
use local\services\subsidiaryCommerce;
use local\services\groupCommerce;
?>

<div class="intec-content">
	<div class="intec-content-wrapper">
        <?
        $typeCommerce=typeCommerce::getTypeCommerceCode($arResult["VARIABLES"]["TEST_CODE"]);
        if($typeCommerce=="chargeable")
        {
            if(groupCommerce::checkGroupTest(typeCommerce::getIdAsCode($arResult["VARIABLES"]["TEST_CODE"])))
            {
                ?>
        <div class="item-info-column">
                <div>
                    <p>
                        Доступ к тесту оплачен
                    </p>
                </div>
        </div>
                <?
                include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/temp.php");
            }else{
                $subsidiaryCommerce=subsidiaryCommerce::getElement(typeCommerce::getIdAsCode($arResult["VARIABLES"]["TEST_CODE"]));

                $module_id="aelita.test";
                CModule::IncludeModule($module_id);



                if(strlen($arResult["VARIABLES"]["GROUP_CODE"])>0){
                    $FilterGroup=[
                        "ACTIVE"=>"Y",
                        "CODE"=>$arResult["VARIABLES"]["GROUP_CODE"],
                    ];
                    $el=new AelitaTestGroup();
                    $res=$el->GetList(array(),$FilterGroup,false,array("nPageSize"=>1),[
                        "ID",
                        "XML_ID",
                        "GROUP_ID",
                        "NAME",
                        "ACTIVE",
                        "PICTURE",
                        "ALT",
                        "DESCRIPTION",
                        "DESCRIPTION_TYPE",
                        "SORT",
                        "CODE",
                        "ACCESS_ALL",
                        "GROUP_NAME",
                        "GROUP_CODE",
                    ]);
                    if($test=$res->GetNext())
                    {

                        $code=$test["CODE"];
                        if(strlen($code)<=0)
                            $code=$test["ID"];
                        $url=$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["group"];
                        $url=str_replace('#GROUP_CODE#',$code,$url);
                        $url=str_replace('#GROUP_ID#',$test["ID"],$url);
                        $test["GROUP_URL"]=$url;

                        if($test["GROUP_ID"]>0){
                            $code=$test["GROUP_CODE"];
                            if(strlen($code)<=0)
                                $code=$test["GROUP_ID"];
                            $url=$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["group"];
                            $url=str_replace('#GROUP_CODE#',$code,$url);
                            $url=str_replace('#GROUP_ID#',$test["GROUP_ID"],$url);
                            $test["GROUP_PARENT_URL"]=$url;
                        }

                        if($test["GROUP_ID"]>0)
                            $APPLICATION->AddChainItem($test["GROUP_NAME"],$test["GROUP_PARENT_URL"]);
                        $APPLICATION->AddChainItem($test["NAME"],$test["GROUP_URL"]);


                    }
                }





                ?>
                <div class="item-info-column">
                    <div>
                        <p>
                            Для прохождения теста необходимо оплатить доступ к нему
                            <span class="item-buttons-counter-block" style="display:inline-block;">
                                    <a class="intec-button intec-button-cl-common intec-button-md intec-button-s-7 intec-button-fs-16 intec-button-block intec-basket-button add"
                                       data-basket-add="<?=$subsidiaryCommerce?>"
                                       data-basket-in="<?= $bInBasket ? 'true' : 'false' ?>"
                                       data-basket-quantity="1">
                                        <span class="intec-button-w-icon intec-basket glyph-icon-cart"></span>
                                        <span class="intec-basket-text">Купить</span>
                                    </a>
                                </span>
                        </p>
                    </div>
                </div>
                <?
            }
        }else{
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/temp.php");
        }?>
    </div>
</div>
<?

$name=testService::getNameAndTypeCode($arResult["VARIABLES"]["TEST_CODE"]);
$APPLICATION->SetTitle($name);
$APPLICATION->SetPageProperty('title',$name);
$APPLICATION->AddChainItem($name,"");

?>

