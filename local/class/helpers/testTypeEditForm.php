<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 19.08.2018
 * Time: 15:19
 */

namespace local\helpers;

use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;
use Bitrix\Main\Loader;
use local\services\typeCommerce;

class testTypeEditForm
{
    const module_id="aelita.test";

    public static function ShowForm(&$form)
    {
        if(
            in_array(self::getStringUri(),self::getUrlForm())
            && $_REQUEST['ID']>0
            && Loader::includeModule(self::module_id)
        ){
            $arr = [
                "REFERENCE" =>["Бесплатный", "Платный", "Бета-тест"],
                "REFERENCE_ID" =>['free', 'chargeable', 'beta_test']
            ];
            $val=typeCommerce::getTypeCommerce($_REQUEST['ID']);
            $Price=typeCommerce::getPriceCommerce($_REQUEST['ID']);
            $CONTENT='';
            $CONTENT.='<tr valign="top"><td>Тип коммерции:</td><td>'.SelectBoxFromArray("TYPE_COMMERCE", $arr,$val).'</td></tr>';
            $CONTENT.='<tr valign="top"><td>Стоимость:</td><td><input type="text" name="TEST_PRICE" size="50" value="'.$Price.'"></td></tr>';
            $form->tabs[]=[
                "DIV"=>"type_commerce",
                "TAB"=>"Тип коммерции",
                "TITLE"=>"Тип коммерции",
                "CONTENT"=>$CONTENT
            ];
        }
    }

    public static function SaveForm()
    {
        if(
            in_array(self::getStringUri(),self::getUrlForm())
            && $_REQUEST['ID']>0
            && Loader::includeModule(self::module_id)
            && self::chekFormRequest()
        ){
            $TypeCommerce=self::getTypeCommerce();
            $Price=self::getPrice();
            $ID=$_REQUEST['ID'];
            if($TypeCommerce)
                typeCommerce::saveTypeCommerce($ID,$TypeCommerce,$Price);
        }
    }

    private static function getUrlForm()
    {
        $UrlEdit=['/bitrix/admin/aelita.test.test.edit.ex.php'];
        return $UrlEdit;
    }

    private static function getStringUri()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $uriString = $request->getRequestUri();
        $uri = new Uri($uriString);
        return $uri->getPath();
    }

    private static function chekFormRequest()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $result=false;
        if($request->isPost())
            $result=true;
        return $result;
    }

    private static function getTypeCommerce()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        if($request->isPost())
        {
            $TypeCommerce=trim($request->getPost('TYPE_COMMERCE'));
            if(in_array($TypeCommerce,['free', 'chargeable', 'beta_test']))
                return $TypeCommerce;
        }
        return false;
    }

    private static function getPrice()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        if($request->isPost())
        {
            $TypeCommerce=trim($request->getPost('TEST_PRICE'));
            return $TypeCommerce;
        }
        return false;
    }

}