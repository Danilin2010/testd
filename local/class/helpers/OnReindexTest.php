<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 02.10.2018
 * Time: 2:52
 */

namespace local\helpers;

class OnReindexTest
{
    function SearchReindex($NS, $oCallback, $callback_method)
    {
        $NS["ID"] = intval($NS["ID"]);
        $module_id="aelita.test";
        if(!\CModule::IncludeModule($module_id))
            return;

        $group_id=2;
        $site_id='s1';

        $Date=ConvertDateTime(GetTime(time(),"FULL"), "YYYY-MM-DD HH:MI:SS");
        $Filter=array(
            "ACTIVE"=>"Y",
            array(
                "LOGIC"=>"OR",
                array(
                    "LOGIC"=>"AND",
                    "DATE_FROM"=>false,
                    "DATE_TO"=>false,
                ),
                array(
                    "LOGIC"=>"AND",
                    "<DATE_FROM"=>$Date,
                    "DATE_TO"=>false,
                ),
                array(
                    "LOGIC"=>"AND",
                    "DATE_FROM"=>false,
                    ">DATE_TO"=>$Date,
                ),
                array(
                    "LOGIC"=>"AND",
                    "<DATE_FROM"=>$Date,
                    ">DATE_TO"=>$Date,
                ),
            ),
        );
        if($NS["ID"] > 0)
            $Filter[">ID"]=$NS["ID"];

        $el=new \AelitaTestTest();
        $res=$el->GetList(array(),$Filter,false,false,array(
            "ID",
            "NAME",
            "DESCRIPTION",
            "DESCRIPTION_TYPE",
            "GROUP_ID",
            "GROUP_CODE",
        ));
        while($row=$res->GetNext())
        {
            $searchData = [
                'ID' => $row['ID'],
                'SITE_ID'=> [$site_id],
                'DATE_CHANGE' => date('d.m.Y H:i:s'),
                'URL' => "/test/{$row["GROUP_CODE"]}/{$row["ID"]}/",
                'PERMISSIONS'=> [$group_id],
                'TITLE' => $row['NAME'],
                'BODY' => $row['DESCRIPTION'],
                'PARAM1' => '',
                'PARAM2' => '',
            ];

            $index_res = call_user_func(
                array($oCallback, $callback_method),
                $searchData
            );

            if (!$index_res) {
                return $searchData['ID'];
            }
        }

        return false;
    }
}