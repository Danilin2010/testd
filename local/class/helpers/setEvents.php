<?php

namespace local\helpers;

use Bitrix\Main\EventManager;
use local\services\groupCommerce;

class setEvents
{

    public static function init()
    {

        EventManager::getInstance()->addEventHandlerCompatible (
            "main",
            "OnAdminTabControlBegin",
            array(
                "local\\helpers\\testTypeEditForm",
                "ShowForm"
            )
        );

        EventManager::getInstance()->addEventHandlerCompatible (
            "main",
            "OnBeforeProlog",
            array(
                "local\\helpers\\testTypeEditForm",
                "SaveForm"
            )
        );

        if(\CModule::IncludeModule('intec.startshop'))
        {
            \CStartShopEvents::Add('OnOrderPayedSuccess',function ($field){
                groupCommerce::callbackOrder($field["ORDER"]);
                //\Bitrix\Main\Diag\Debug::writeToFile($field,"","log.txt");
            });
        }

        EventManager::getInstance()->addEventHandlerCompatible (
            "main",
            "OnBeforeUserRegister",
            array(
                "local\\helpers\\RegisterHelper",
                "OnBeforeUserRegister"
            )
        );

        EventManager::getInstance()->addEventHandlerCompatible (
            "main",
            "OnBeforeUserSimpleRegister",
            array(
                "local\\helpers\\RegisterHelper",
                "OnBeforeUserRegister"
            )
        );

        EventManager::getInstance()->addEventHandlerCompatible (
            "search",
            "OnReindex",
            array(
                "local\\helpers\\OnReindexTest",
                "SearchReindex"
            )
        );

    }

}