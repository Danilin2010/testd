<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 10.09.2018
 * Time: 22:50
 */

namespace local\helpers;


class RegisterHelper
{

    public function OnBeforeUserRegister(&$arFields)
    {
        $arFields["LOGIN"] = $arFields["EMAIL"];
    }

}