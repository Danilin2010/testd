<?php

namespace local\domain\repository;

use local\domain\entity\Import;
use local\domain\factory\FactoryImport;
use Bitrix\Main\Loader;
use InvalidArgumentException;

/**
 * Class RepositoryImport
 * @package domain\repository
 */
class RepositoryImport
{

    public function __construct()
    {
        Loader::includeModule('iblock');
    }

    /**
     * @param Import $Import
     * @return int
     */
    public function add(Import $Import)
    {
        $arField = [];
        if($Import->getName())
            $arField["NAME"]=$Import->getName();
        if($Import->getStatus())
            $arField["PROPERTY_VALUES"]["status"]=$Import->getStatus();
        if($Import->getFileName())
        $arField["PROPERTY_VALUES"]["filename"]=$Import->getFileName();
        if($Import->getNs())
            $arField["PREVIEW_TEXT"]=serialize ($Import->getNs());
        $el = new \CIBlockElement;
        if($PRODUCT_ID = $el->Add($arField))
            return $PRODUCT_ID;
        else
            throw new InvalidArgumentException($el->LAST_ERROR);
    }

    /**
     * @param Import $Import
     * @return bool
     */
    public function update(Import $Import)
    {
        $Id=$Import->getId();
        if ($Id<=0)
            throw new InvalidArgumentException('Неправильный id');
        $el = new \CIBlockElement;
        $arField = [];
        if($Import->getName())
            $arField["NAME"]=$Import->getName();
        if($Import->getStatus())
            $arField["PROPERTY_VALUES"]["status"]=$Import->getStatus();
        if($Import->getFileName())
            $arField["PROPERTY_VALUES"]["filename"]=$Import->getFileName();
        if($Import->getNs())
            $arField["PREVIEW_TEXT"]=serialize ($Import->getNs());
        if(!$el->Update($Id,$arField))
            throw new InvalidArgumentException($el->LAST_ERROR);
        return true;
    }

    /**
     * @param string $Id
     * @return Import
     */
    public function GetById($Id)
    {
        $Id=(int)$Id;
        if ($Id<=0)
            throw new InvalidArgumentException('Неправильный id');
        $arOrder = Array("SORT"=>"ASC");
        $arFilter = Array(
            "ID"=>$Id,
        );
        $res = \CIBlockElement::GetList($arOrder, $arFilter, false, false, [
            "ID",
            "NAME",
            "PREVIEW_TEXT",
            "PROPERTY_status",
            "PROPERTY_archive",
            "PROPERTY_filename",
        ]);
        if($ob = $res->GetNext())
        {
            return FactoryImport::createFromArray(array(
                "id"=>$ob["ID"],
                "fileId"=>$ob["PROPERTY_ARCHIVE_VALUE"],
                "status"=>$ob["PROPERTY_STATUS_VALUE"],
                "fileName"=>$ob["PROPERTY_FILENAME_VALUE"],
                "ns"=>unserialize($ob["~PREVIEW_TEXT"]),
                "name"=>$ob["NAME"],
            ));
        }else{
            throw new InvalidArgumentException("Не найден элемент");
        }
    }

    /**
     * @return Import|bool
     */
    public function GetLast()
    {
        $arOrder = Array("ID"=>"ASC");
        $arFilter = Array(
            "!PROPERTY_archive"=>false,
            "!PROPERTY_status"=>'final',
        );
        $res = \CIBlockElement::GetList($arOrder, $arFilter, false, Array("nTopCount"=>1), [
            "ID",
            "NAME",
            "PREVIEW_TEXT",
            "PROPERTY_status",
            "PROPERTY_archive",
            "PROPERTY_filename",
        ]);
        if($ob = $res->GetNext())
        {
            return FactoryImport::createFromArray(array(
                "id"=>$ob["ID"],
                "fileId"=>$ob["PROPERTY_ARCHIVE_VALUE"],
                "status"=>$ob["PROPERTY_STATUS_VALUE"],
                "fileName"=>$ob["PROPERTY_FILENAME_VALUE"],
                "ns"=>unserialize($ob["~PREVIEW_TEXT"]),
                "name"=>$ob["NAME"],
            ));
        }else
            return false;
    }

    /**
     * @param string $Id
     * @return bool
     */
    public function Delete($Id)
    {
        $Id=(int)$Id;
        if ($Id<=0)
            throw new InvalidArgumentException('Неправильный id');
        if(!\CIBlockElement::Delete($Id))
            throw new InvalidArgumentException('Ошибка удаления.');
        return true;
    }

}