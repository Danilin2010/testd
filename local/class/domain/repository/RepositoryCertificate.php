<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 15.04.2019
 * Time: 21:34
 */

namespace local\domain\repository;

use local\domain\entity\Certificate;
use local\domain\factory\FactoryCertificate;
use Bitrix\Main\Loader;
use InvalidArgumentException;
use Bitrix\Main\Type;
use DateTime;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectException;

class RepositoryCertificate
{
    /**
     * RepositoryCertificate constructor.
     * @throws LoaderException
     */
    public function __construct()
    {
        Loader::includeModule('iblock');
    }

    /**
     * @return int
     */
    public function getSourcePhoto(){
        return (int)\CIBlock::GetArrayByID($this->getIblockId(), "PICTURE");
    }

    /**
     * @param Certificate $Import
     * @return int
     */
    public function add(Certificate $Import)
    {
        $arField=$this->ObjectToArray($Import);
        $el = new \CIBlockElement;
        if($PRODUCT_ID = $el->Add($arField))
            return $PRODUCT_ID;
        else
            throw new InvalidArgumentException($el->LAST_ERROR);
    }

    /**
     * @param Certificate $Import
     * @return bool
     */
    public function update(Certificate $Import)
    {
        $Id=$Import->getId();
        if ($Id<=0)
            throw new InvalidArgumentException('Неправильный id');
        $el = new \CIBlockElement;
        $arField=$this->ObjectToArray($Import);
        if(!$el->Update($Id,$arField))
            throw new InvalidArgumentException($el->LAST_ERROR);
        return true;
    }

    /**
     * @param $Id
     * @return Certificate
     * @throws ObjectException
     */
    public function GetById($Id)
    {
        $Id=(int)$Id;
        if ($Id<=0)
            throw new InvalidArgumentException('Неправильный id');
        $arOrder = Array("SORT"=>"ASC");
        $arFilter = Array(
            "IBLOCK_ID"=>$this->getIblockId(),
            "ID"=>$Id,
        );
        $res = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $this->getSelect());
        if($ob = $res->GetNext())
            return $this->ArrayToObject($ob);
        else
            throw new InvalidArgumentException("Не найден элемент");
    }

    /**
     * @param array $arFilter
     * @return int
     */
    public function GetCount($arFilter=[])
    {
        $arFilter["IBLOCK_ID"]=$this->getIblockId();
        $res = \CIBlockElement::GetList([],$arFilter,[],false);
        return (int)$res;
    }

    /**
     * @param array $arOrder
     * @param array $arFilter
     * @return Certificate[]
     * @throws ObjectException
     */
    public function GetList($arOrder=[],$arFilter=[])
    {
        $arrRes=[];
        $arFilter["IBLOCK_ID"]=$this->getIblockId();
        $res = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $this->getSelect());
        while($ob = $res->GetNext())
            $arrRes[]=$this->ArrayToObject($ob);
        return $arrRes;
    }

    /**
     * @return array
     */
    private function getSelect(){
        return [
            "ID",
            "NAME",
            "DETAIL_PAGE_URL",
            "PROPERTY_user",
            "PROPERTY_year",
            "PROPERTY_yearId",
            "PROPERTY_nameTest",
            "PROPERTY_date",
            "PROPERTY_questioning",
        ];
    }

    /**
     * @param Certificate $Import
     * @return array
     */
    private function ObjectToArray(Certificate $Import){
        $arField = [];
        $arField["IBLOCK_ID"] = $this->getIblockId();
        if($Import->getName())
            $arField["NAME"]=$Import->getName();
        if($Import->getQuestioning())
            $arField["PROPERTY_VALUES"]["questioning"]=$Import->getQuestioning();
        if($Import->getUser())
            $arField["PROPERTY_VALUES"]["user"]=$Import->getUser();
        if($Import->getYear())
            $arField["PROPERTY_VALUES"]["year"]=$Import->getYear();
        if($Import->getYearId())
            $arField["PROPERTY_VALUES"]["yearId"]=$Import->getYearId();
        if($Import->getName())
            $arField["PROPERTY_VALUES"]["nameTest"]=$Import->getName();
        if($Import->getDate())
            $arField["PROPERTY_VALUES"]["date"]=Type\DateTime::createFromPhp($Import->getDate());
        return $arField;
    }

    /**
     * @param $ob
     * @return Certificate
     * @throws ObjectException
     */
    private function ArrayToObject($ob){
        $prop=array(
            "id"=>$ob["ID"],
            "url"=>$ob["DETAIL_PAGE_URL"],
            "user"=>$ob["PROPERTY_USER_VALUE"],
            "year"=>$ob["PROPERTY_YEAR_VALUE"],
            "yearId"=>$ob["PROPERTY_YEARID_VALUE"],
            "nameTest"=>$ob["PROPERTY_NAMETEST_VALUE"],
            "questioning"=>$ob["PROPERTY_QUESTIONING_VALUE"],
            "name"=>$ob["NAME"],
        );
        if($ob["PROPERTY_DATE_VALUE"])
        {
            $DATE=new Type\DateTime($ob["PROPERTY_DATE_VALUE"]);
            $newDATE=new DateTime();
            $newDATE->setTimestamp($DATE->getTimestamp());
            $prop['date']=$newDATE;
        }
        return FactoryCertificate::createFromArray($prop);
    }

    /**
     * @return mixed
     */
    private function getIblockId(){
        return IBLOCK_CERTIFICATE;
    }

    /**
     * @param string $Id
     * @return bool
     */
    public function Delete($Id)
    {
        $Id=(int)$Id;
        if ($Id<=0)
            throw new InvalidArgumentException('Неправильный id');
        if(!\CIBlockElement::Delete($Id))
            throw new InvalidArgumentException('Ошибка удаления.');
        return true;
    }
}