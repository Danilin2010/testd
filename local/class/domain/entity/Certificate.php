<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 15.04.2019
 * Time: 21:17
 */

namespace local\domain\entity;

use DateTime;

class Certificate
{
    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $price
     */
    private $name;

    /**
     * @var string $url
     */
    private $url;

    /**
     * @var int $questioning
     */
    private $questioning;

    /**
     * @var int $user
     */
    private $user;

    /**
     * @var int $year
     */
    private $year;

    /**
     * @var int $yearId
     */
    private $yearId;

    /**
     * @var string $price
     */
    private $nameTest;

    /**
     * @var DateTime $date
     */
    private $date;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getQuestioning()
    {
        return $this->questioning;
    }

    /**
     * @param int $questioning
     */
    public function setQuestioning($questioning)
    {
        $this->questioning = $questioning;
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getYearId()
    {
        return $this->yearId;
    }

    /**
     * @param int $yearId
     */
    public function setYearId($yearId)
    {
        $this->yearId = $yearId;
    }

    /**
     * @return string
     */
    public function getNameTest()
    {
        return $this->nameTest;
    }

    /**
     * @param string $nameTest
     */
    public function setNameTest($nameTest)
    {
        $this->nameTest = $nameTest;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

}