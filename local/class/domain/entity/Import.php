<?

namespace local\domain\entity;

/**
 * Class Import
 * @package domain\entity
 */
class Import
{

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $price
     */
    private $name;

    /**
     * @var [] $ns
     */
    private $ns;

    private $fileId;

    /**
     * @var string $status
     */
    private $status;

    /**
     * @var string $status
     */
    private $fileName;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getFileId()
    {
        return $this->fileId;
    }

    public function setFileId($fileId)
    {
        $this->fileId = $fileId;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return []
     */
    public function getNs()
    {
        return $this->ns;
    }

    /**
     * @param [] $ns
     */
    public function setNs($ns)
    {
        $this->ns = $ns;
    }

}