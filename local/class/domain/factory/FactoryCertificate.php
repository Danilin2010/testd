<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 15.04.2019
 * Time: 21:22
 */

namespace local\domain\factory;

use local\domain\entity\Certificate;
use InvalidArgumentException;

class FactoryCertificate
{
    /**
     * @param array $params
     * @return Certificate
     * @throws InvalidArgumentException
     */
    public static function createFromArray(array $params)
    {
        $good = new Certificate();
        if($params['id']>0)
            $good->setId($params['id']);
        if($params['questioning']>0)
            $good->setQuestioning($params['questioning']);
        if($params['url'])
            $good->setUrl($params['url']);
        if($params['user']>0)
            $good->setUser($params['user']);
        if($params['year']>0)
            $good->setYear($params['year']);
        if($params['yearId']>0)
            $good->setYearId($params['yearId']);
        if(trim($params['name']))
            $good->setName(trim($params['name']));
        if(trim($params['nameTest']))
            $good->setNameTest(trim($params['nameTest']));
        if($params['date'])
            $good->setDate($params['date']);
        return $good;
    }

    /**
     * @param array $records
     * @throws InvalidArgumentException
     * @return Certificate[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}