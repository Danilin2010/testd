<?php

namespace local\domain\factory;

use local\domain\entity\Import;
use InvalidArgumentException;
/**
 * Class FactoryImport
 * @package domain\factory
 */
class FactoryImport
{
    /**
     * @param array $params
     * @return Import
     * @throws InvalidArgumentException
     */
    public static function createFromArray(array $params)
    {
        $good = new Import();
        if($params['id']>0)
            $good->setId($params['id']);
        if($params['name'])
            $good->setName(trim($params['name']));
        if($params['fileId'])
            $good->setFileId($params['fileId']);
        if($params['status'])
            $good->setStatus(trim($params['status']));
        if($params['fileName'])
            $good->setFileName(trim($params['fileName']));
        if($params['ns'])
            $good->setNs($params['ns']);
        return $good;
    }

    /**
     * @param array $records
     * @throws InvalidArgumentException
     * @return Import[]
     */
    public static function createFromCollection(array $records)
    {
        $output = [];
        array_map(function ($item) use (&$output) {
            $output[] = self::createFromArray($item);
        }, $records);
        return $output;
    }
}