<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 15.04.2019
 * Time: 21:50
 */

namespace local\services;

use local\domain\entity\Certificate;
use local\domain\factory\FactoryCertificate;
use local\domain\repository\RepositoryCertificate;
use DateTime;
use CDatabase;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectException;
use local\helpers\ttfTextOnImage;
use Bitrix\Main\Application;

class ServicesCertificate
{

    /**
     * @param int $UserId
     * @param string $ResultDescription
     * @param string $nameTest
     * @param int $QuestioningId
     * @return Certificate|bool
     * @throws LoaderException
     * @throws ObjectException
     */
    public function chekResultTest($UserId,$ResultDescription,$nameTest,$QuestioningId){

        if($UserId<=0)
            return false;

        if($QuestioningId<=0)
            return false;

        if(strripos($ResultDescription,"тест пройден")===FALSE)
            return false;

        $newDate=new DateTime();

        $RepositoryCertificate=new RepositoryCertificate();

        $Year=$newDate->format('Y');

        $minDateCall = "01.01.$Year 00:00:00";
        $maxDateCall = "31.12.$Year 23:59:59";

        $Count=$RepositoryCertificate->GetCount([
            ">=PROPERTY_DATE" => trim(CDatabase::CharToDateFunction($minDateCall),"\'"),
            "<=PROPERTY_DATE" => trim(CDatabase::CharToDateFunction($maxDateCall),"\'"),
        ]);

        $Count++;

        $List=$RepositoryCertificate->GetList([],[
            "PROPERTY_questioning" => $QuestioningId,
            //"PROPERTY_user" => $UserId,
        ]);

        if(count($List)<=0){
            $Certificate=FactoryCertificate::createFromArray(array(
                "name"=>$nameTest,
                "user"=>$UserId,
                "questioning"=>$QuestioningId,
                "year"=>$Year,
                "yearId"=>$Count,
                "nameTest"=>$nameTest,
                "date"=>$newDate,
            ));
            $id=$RepositoryCertificate->add($Certificate);
            $Certificate=$RepositoryCertificate->GetById($id);
        }else{
            $Certificate=$List[0];
        }
        return $Certificate;
    }

    /**
     * @param $id
     * @return bool
     * @throws LoaderException
     * @throws ObjectException
     */
    public function getCertificate($id){
        $RepositoryCertificate=new RepositoryCertificate();

        $Certificate=$RepositoryCertificate->GetById($id);

        global $USER;

        $UserId=$USER->GetID();

        if($UserId>0 && $Certificate->getUser()==$UserId){

            $Photo=$RepositoryCertificate->getSourcePhoto();

            if($Photo>0){

                $rsUser = \CUser::GetByID($Certificate->getUser());
                if($arUser = $rsUser->Fetch()){

                    $arrName=[];
                    if($arUser["LAST_NAME"])
                        $arrName[]=$arUser["LAST_NAME"];

                    if($arUser["NAME"])
                        $arrName[]=$arUser["NAME"];

                    if($arUser["SECOND_NAME"])
                        $arrName[]=$arUser["SECOND_NAME"];

                    if(count($arrName)<=0)
                        $arrName[]=$arUser["LOGIN"];

                    $name=implode(' ',$arrName);

                    $rsFile = \CFile::GetByID($Photo);

                    if($arFile = $rsFile->Fetch()){

                        $CENTER=round($arFile["WIDTH"]/2);

                        $Path=\CFile::GetPath($arFile["ID"]);

                        $ttfImg = new ttfTextOnImage(Application::getDocumentRoot().$Path);

                        $ttfImg->setFont('ArialNarrow.ttf', 64, "#666666", 0);
                        $box=$ttfImg->writeBox($name);
                        $left = $CENTER-round(($box[2]-$box[0])/2);
                        $ttfImg->writeText($left, 465, $name);

                        $ttfImg->setFont('ArialNarrow-Italic.ttf', 32, "#666666", 0);
                        $box=$ttfImg->writeBox($Certificate->getName());
                        $left = $CENTER-round(($box[2]-$box[0])/2);
                        $ttfImg->writeText($left, 630, $Certificate->getName());

                        $ttfImg->setFont('ArialNarrow.ttf', 40, "#666666", 0);
                        $ttfImg->writeText(300, 843, "#".$Certificate->getYear().'/'.$Certificate->getYearId());

                        $ttfImg->setFont('ArialNarrow.ttf', 40, "#666666", 0);
                        $ttfImg->writeText(1400, 843, $Certificate->getDate()->format("Y/m/d"));

                        global $APPLICATION;
                        $APPLICATION->RestartBuffer();

                        $ext = strtolower(substr($arFile["CONTENT_TYPE"], strrpos($arFile["CONTENT_TYPE"], "/") + 1));

                        header("Accept-Ranges: bytes");
                        header("Content-Type: ".$arFile["CONTENT_TYPE"]);
                        header("Content-Disposition: attachment; filename=\"".$Certificate->getName().".".$ext."\"");

                        $ttfImg->outputExt($ext);

                        die();

                    }

                }


            }
        }
        return false;

    }

}