<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 19.08.2018
 * Time: 15:55
 */

namespace local\services;

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Loader;

class typeCommerce
{
    /**
     * @param int $id
     * @param string $Type
     * @param float $Price
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function saveTypeCommerce($id,$Type,$Price)
    {
        $entity_data_class=self::getIblock();
        $rsData = $entity_data_class::getList(["limit"=>1,"filter"=>["UF_TESTID"=>$id]]);
        if($arData = $rsData->Fetch())
            $entity_data_class::update($arData["ID"],["UF_TESTTYPE"=>self::getIdEnum($Type),"UF_TESTPRICE"=>$Price]);
        else
            $entity_data_class::add(["UF_TESTID"=>$id,"UF_TESTTYPE"=>self::getIdEnum($Type),"UF_TESTPRICE"=>$Price]);
        subsidiaryCommerce::updateElement($id,$Price);
    }

    /**
     * @param int $id
     * @return string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getTypeCommerce($id)
    {
        $entity_data_class=self::getIblock();
        $rsData = $entity_data_class::getList(["limit"=>1,"filter"=>["UF_TESTID"=>$id]]);
        if($arData = $rsData->Fetch())
            return self::getXMLIdEnum($arData["UF_TESTTYPE"]);
        else
            return "free";
    }

    /**
     * @param int $id
     * @return float
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getPriceCommerce($id)
    {
        $entity_data_class=self::getIblock();
        $rsData = $entity_data_class::getList(["limit"=>1,"filter"=>["UF_TESTID"=>$id]]);
        if($arData = $rsData->Fetch())
            return $arData["UF_TESTPRICE"];
        else
            return 0;
    }

    /**
     * @param $code
     * @return float
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getPriceCommerceCode($code)
    {
        if(is_numeric($code))
            $filter=array("ID" => $code);
        else
            $filter=array("CODE" => $code);
        if (Loader::includeModule('aelita.test'))
        {
            $elTest=new \AelitaTestTest;
            $resTest = $elTest->GetList(array(), $filter);
            if($Test = $resTest->GetNext())
                return self::getPriceCommerce($Test["ID"]);
        }
        return "free";
    }

    /**
     * @param $code
     * @return string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getTypeCommerceCode($code)
    {
        if(is_numeric($code))
            $filter=array("ID" => $code);
        else
            $filter=array("CODE" => $code);
        if (Loader::includeModule('aelita.test'))
        {
            $elTest=new \AelitaTestTest;
            $resTest = $elTest->GetList(array(), $filter);
            if($Test = $resTest->GetNext())
                return self::getTypeCommerce($Test["ID"]);
        }
        return "free";
    }

    /**
     * @param string $Type
     * @return bool|int
     */
    private static function getIdEnum($Type)
    {
        $resTypeEntity = \CUserTypeEntity::GetList([],["FIELD_NAME"=>"UF_TESTTYPE"]);
        if($TypeEntity = $resTypeEntity->Fetch())
        {
            $UserField = \CUserFieldEnum::GetList([], ['USER_FIELD_ID'=>$TypeEntity["ID"],'XML_ID' => $Type]);
            if($Field = $UserField->Fetch())
                return $Field["ID"];
        }
        return false;
    }

    /**
     * @param int $id
     * @return bool|string
     */
    private static function getXMLIdEnum($id)
    {
        $resTypeEntity = \CUserTypeEntity::GetList([],["FIELD_NAME"=>"UF_TESTTYPE"]);
        if($TypeEntity = $resTypeEntity->Fetch())
        {
            $UserField = \CUserFieldEnum::GetList([], ['USER_FIELD_ID'=>$TypeEntity["ID"],'ID' => $id]);
            if($Field = $UserField->Fetch())
                return $Field["XML_ID"];
        }
        return false;
    }

    /**
     * @return string
     */
    private static function IblockName() {
        return 'TypeOfTest';
    }

    /**
     * @return DataManager
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private static function getIblock()
    {
        if (Loader::includeModule('highloadblock'))
        {
            $hlblock = HL\HighloadBlockTable::getList([
                'filter' => ['=NAME' => self::IblockName()]
            ])->fetch();
            if($hlblock){
                $entity = HL\HighloadBlockTable::compileEntity($hlblock);
                $entity_data_class = $entity->getDataClass();
                return $entity_data_class;
            }
        }
    }

    /**
     * @param string $code
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getIdAsCode($code)
    {
        if(is_numeric($code))
            $filter=array("ID" => $code);
        else
            $filter=array("CODE" => $code);
        if (Loader::includeModule('aelita.test'))
        {
            $elTest=new \AelitaTestTest;
            $resTest = $elTest->GetList(array(), $filter);
            if($Test = $resTest->GetNext())
                return $Test["ID"];
        }
        return 0;
    }

}