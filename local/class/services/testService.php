<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 20.08.2018
 * Time: 1:07
 */

namespace local\services;

use Bitrix\Main\Loader;

class testService
{
    /**
     * @param int $id
     * @param bool $suffix
     * @return string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getNameAndType($id,$suffix=true)
    {
        if (Loader::includeModule('aelita.test'))
        {
            $elTest=new \AelitaTestTest;
            $resTest = $elTest->GetList(array(), array("ID" => $id));
            if($Test = $resTest->GetNext())
            {
                $name=$Test["NAME"];
                if($suffix)
                    $name.=" (".self::getSuffix($Test["ID"]).")";
                return $name;
            }
        }
        return "";
    }

    /**
     * @param string $code
     * @param bool $suffix
     * @return string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getNameAndTypeCode($code,$suffix=true)
    {
        if(is_numeric($code))
            $filter=array("ID" => $code);
        else
            $filter=array("CODE" => $code);
        if (Loader::includeModule('aelita.test'))
        {
            $elTest=new \AelitaTestTest;
            $resTest = $elTest->GetList(array(), $filter);
            if($Test = $resTest->GetNext())
            {
                $name=$Test["NAME"];
                if($suffix)
                    $name.=" (".self::getSuffix($Test["ID"]).")";
                return $name;
            }
        }
        return "";
    }

    /**
     * @param int $id
     * @return string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private static function getSuffix($id)
    {
        $type=typeCommerce::getTypeCommerce($id);
        switch ($type) {
            case "chargeable":
                return "$";
                break;
            case "beta_test":
                return "beta";
                break;
            case "free":
            default:
                return "free";
        }
    }
}