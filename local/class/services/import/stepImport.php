<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 15.11.2018
 * Time: 3:23
 */

namespace local\services\import;

use local\domain\entity\Import;
use local\domain\repository\RepositoryImport;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;
use InvalidArgumentException;

class stepImport
{

    private $RepositoryImport;

    public function __construct()
    {
        $this->RepositoryImport=new RepositoryImport();
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function step()
    {
        $Import= $this->RepositoryImport->GetLast();
        if($Import)
            $this->initStep($Import);

    }

    /**
     * @param Import $Import
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function initStep($Import)
    {
        $patchImport=Application::getDocumentRoot()."/upload/aelita.test.import/".$Import->getId()."/";
        switch ($Import->getStatus()) {
            case 'delOld':
                    $xml = new \CDataXML();
                    $xml->delete_ns=false;
                    $xml->LoadString(File::getFileContents($patchImport.$Import->getFileName()));
                    $uploadTest=new updateOldTest();
                    $uploadTest->delOld($xml);
                    $Import->setStatus('addNew');
                break;
            case "addNew":
                $xml = new \CDataXML();
                $xml->delete_ns=false;
                $xml->LoadString(File::getFileContents($patchImport.$Import->getFileName()));
                $uploadTest=new updateNewTest();
                $uploadTest->update($xml,$patchImport);
                $Import->setStatus('delDir');
                break;
            case "delDir":
                Directory::deleteDirectory($patchImport);
                $Import->setStatus('final');
                break;
            case 'start':
            default:
                $fileName=fileImport::getFileName($Import->getFileId(),$patchImport);
                if($fileName)
                {
                    $Import->setFileName($fileName);
                    $Import->setStatus('delOld');
                }else{
                    $Import->setStatus('final');
                }
                break;
        }
        $this->RepositoryImport->update($Import);
    }

}