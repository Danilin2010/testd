<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 15.11.2018
 * Time: 16:07
 */

namespace local\services\import;

use Bitrix\Main\Loader;
use local\services\typeCommerce;
use Bitrix\Main\Type\DateTime;

class updateNewTest
{
    public function __construct()
    {
        Loader::includeModule('aelita.test');
    }

    /**
     * @param $DataXML
     * @param $patchImport
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function update($DataXML,$patchImport)
    {
        $this->group($DataXML,$patchImport);
        $this->element($DataXML,$patchImport);
    }

    /**
     * @param int $testId
     * @param int $count
     * @return bool|int
     */
    private function getGroupQuestion($testId,$count=0)
    {
        $el=new \AelitaTestQuestionGroup();

        $field=[
            'NAME'=>'По умолчанию',
            'COUNT'=>(int)$count,
            'TEST_ID'=>(int)$testId,
        ];
        $dbResultList=$el->GetList([],[
            'TEST_ID'=>$testId,
            "NAME"=>$field["CODE"]
        ],false,["nTopCount"=>1],[
            "ID",
            "NAME"
        ]);
        if ($arResult = $dbResultList->Fetch())
        {
            $elID=$arResult["ID"];
            $el->Update($elID,$field);
        }else{
            $elID=$el->Add($field);
        }

        return $elID;
    }

    /**
     * @param \CDataXML $DataXML
     * @param string $patchImport
     */
    private function group($DataXML,$patchImport)
    {
        $Allnode = $DataXML->SelectNodes('/testsdata/groups');
        $groups=$this->getCroupCode();
        foreach ($Allnode->children() as $node)
        {
            $field=[
                'CODE'=>$node->elementsByName('code')[0]->textContent(),
                'SORT'=>$node->elementsByName('sort')[0]->textContent(),
                'NAME'=>$node->elementsByName('name')[0]->textContent(),
                'DESCRIPTION'=>$node->elementsByName('description')[0]->textContent(),
            ];

            $this->propertyBool($node->elementsByName('active')[0]->textContent(),'ACTIVE',$field);

            $this->propertyPict($node->elementsByName('picture')[0]->textContent(),"PICTURE",$patchImport,$field);

            $el=new \AelitaTestGroup();

            $parentcode=$node->elementsByName('parentcode');
            if(count($parentcode)>0)
            {
                $group=$parentcode[0]->textContent();
                if($groups[$group])
                    $field["GROUP_ID"]=$groups[$group];
            }

            $dbResultList=$el->GetList([],[
                "CODE"=>$field["CODE"]
            ],false,["nTopCount"=>1],[
                "ID",
                "CODE",
                "NAME"
            ]);

            if ($arResult = $dbResultList->Fetch())
                $el->Update($arResult["ID"],$field);
            else{
                $el->Add($field);
                $groups=$this->getCroupCode();
            }

        }
    }

    private function getCroupCode()
    {
        $group=[];
        $el=new \AelitaTestGroup();
        $dbResultList=$el->GetList([],[
        ],false,false,[
            "ID",
            "CODE",
        ]);
        while ($arResult = $dbResultList->Fetch())
            $group[$arResult["CODE"]]=$arResult["ID"];
        return $group;
    }

    /**
     * @param $date
     * @param $name
     * @param $field
     * @throws \Bitrix\Main\ObjectException
     */
    private function propertyDate($date,$name,&$field)
    {
        $DateFormat="Y-m-d H:i:s";
        $datefrom=$date;
        if($datefrom)
        {
            $datefrom=DateTime::createFromPhp(new \DateTime($datefrom));
            //$datefrom=\date($DateFormat, $datefrom->getTimestamp());
            $datefrom=$datefrom->toString();
            $field[$name]=$datefrom;
        }else{
            $datefrom="";
            $field[$name]=$datefrom;
        }
    }

    private function propertyPict($date,$name,$patchImport,&$field)
    {
        $picture=$date;
        if($picture)
        {
            $picture=$patchImport.$picture;
            if(file_exists($picture))
                $field[$name] = \CFile::MakeFileArray($picture);
        }
    }

    private function propertyBool($date,$name,&$field)
    {
        $active=$date;
        if($active=='true')
            $field[$name]="Y";
        else
            $field[$name]="N";
    }

    /**
     * @param $id
     * @param $typecommerce
     * @param $testprice
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function setCommerce($id,$typecommerce,$testprice)
    {
        if(!in_array($typecommerce,[
            "free",
            "chargeable",
            "beta_test",
        ])){
            $typecommerce="free";
        }
        typeCommerce::saveTypeCommerce($id,$typecommerce,$testprice);
    }

    /**
     * @param \CDataXMLNode $el
     * @param $name
     * @return string
     */
    private function getStringValue($el,$name)
    {
        $val=$el->elementsByName($name);
        if(is_array($val) && count($val)>0)
            return $val[0]->textContent();
        return "";
    }

    /**
     * @param \CDataXMLNode $el
     * @param string $name
     * @param [] $field
     */
    private function getBoolValue($el,$name,$bName,&$field)
    {
        $val=$el->elementsByName($name);
        if(is_array($val) && count($val)>0)
        {
            $active=$val[0]->textContent();
            if($active=='true')
                $field[$bName]="Y";
            else
                $field[$bName]="N";
        }
    }

    /**
     * @param int $id
     * @param \CDataXMLNode $questions
     * @param string $patchImport
     * @param int $GroupQuestionID
     */
    private function addQuestions($id,$questions,$patchImport,$GroupQuestionID)
    {
        $el=new \AelitaTestQuestion();
        $arrQuestion=[];
        $dbResultList=$el->GetList([],[
            "TEST_ID"=>$id,
        ],false,false,[
            "ID",
            "CODE",
            "NAME",
        ]);
        while ($arResult = $dbResultList->Fetch())
        {
            $arrQuestion[$arResult["NAME"]]=$arResult["ID"];
            $el->Update($arResult["ID"],["ACTIVE"=>"N"]);
        }

        foreach ($questions->children() as $question)
        {
            $field=[
                'TEST_ID'=>$id,
                'SORT'=>$question->elementsByName('sort')[0]->textContent(),
                'NAME'=>$question->elementsByName('name')[0]->textContent(),
                'CORRECT_ANSWER'=>$this->getStringValue($question,'correctanswer'),
                'SCORES'=>(int)$this->getStringValue($question,'scores'),
                'DESCRIPTION'=>$question->elementsByName('description')[0]->textContent(),
            ];

            if($GroupQuestionID>0)
                $field['TEST_GROUP_ID']=$GroupQuestionID;

            $this->propertyBool($question->elementsByName('active')[0]->textContent(),'ACTIVE',$field);
            $this->propertyBool($question->elementsByName('showcomments')[0]->textContent(),'SHOW_COMMENTS',$field);
            $this->propertyPict($question->elementsByName('picture')[0]->textContent(),"PICTURE",$patchImport,$field);
            $type=$question->elementsByName('type')[0]->textContent();

            if(in_array($type,[
                "radio",
                "check",
                "input",
            ])){
                $field["TEST_TYPE"]=$type;
            }

            if($arrQuestion[$field["NAME"]])
            {
                $idQ=$arrQuestion[$field["NAME"]];
                $el->Update($idQ,$field);
            }else{
                $idQ=$el->Add($field);
            }
            if($idQ>0)
            {
                $arrAnswers=$this->clearAnswers($idQ);
                $answers=$question->elementsByName('answers');
                if(is_array($answers) && count($answers)>0)
                    $this->addAnswers($idQ,$answers[0],$patchImport,$arrAnswers);
            }
        }
    }

    /**
     * @param int $id
     * @return array
     */
    private function clearAnswers($id)
    {
        $el=new \AelitaTestAnswer();
        $arrQuestion=[];
        $dbResultList=$el->GetList([],[
            "QUESTION_ID"=>$id,
        ],false,false,[
            "ID",
            "CODE",
            "NAME",
        ]);
        while ($arResult = $dbResultList->Fetch())
        {
            $arrQuestion[$arResult["NAME"]]=$arResult["ID"];
            $el->Update($arResult["ID"],["ACTIVE"=>"N"]);
        }
        return $arrQuestion;
    }


    /**
     * @param int $id
     * @param \CDataXMLNode $answers
     * @param string $patchImport
     * @param [] $arrAnswers
     */
    private function addAnswers($id,$answers,$patchImport,$arrAnswers)
    {
        $el=new \AelitaTestAnswer();
        foreach ($answers->children() as $answer)
        {
            $field=[
                'QUESTION_ID'=>$id,
                'SORT'=>$answer->elementsByName('sort')[0]->textContent(),
                'NAME'=>$answer->elementsByName('name')[0]->textContent(),
                'DESCRIPTION'=>$answer->elementsByName('description')[0]->textContent(),
                'SCORES'=>(int)$this->getStringValue($answer,'scores'),
            ];
            $this->propertyPict($answer->elementsByName('picture')[0]->textContent(),"PICTURE",$patchImport,$field);
            $this->propertyBool($answer->elementsByName('active')[0]->textContent(),'ACTIVE',$field);
            $this->getBoolValue($answer,'correct','CORRECT',$field);

            if($arrAnswers[$field["NAME"]])
            {
                $idQ=$arrAnswers[$field["NAME"]];
                $el->Update($idQ,$field);
            }else{
                $idQ=$el->Add($field);
            }
        }
    }

    /**
     * @param int $id
     * @param \CDataXMLNode $results
     * @param string $patchImport
     */
    private function addResults($id,$results,$patchImport)
    {
        $el=new \AelitaTestResult();
        $arrResult=[];
        $dbResultList=$el->GetList([],[
            "TEST_ID"=>$id,
        ],false,false,[
            "ID",
            "CODE",
            "NAME",
        ]);
        while ($arResult = $dbResultList->Fetch())
        {
            $arrResult[$arResult["NAME"]]=$arResult["ID"];
            $el->Update($arResult["ID"],["ACTIVE"=>"N"]);
        }

        foreach ($results->children() as $question)
        {
            $field=[
                'TEST_ID'=>$id,
                'SORT'=>$question->elementsByName('sort')[0]->textContent(),
                'NAME'=>$question->elementsByName('name')[0]->textContent(),
                'MIN_SCORES'=>(int)$this->getStringValue($question,'minscores'),
                'MAX_SCORES'=>(int)$this->getStringValue($question,'maxscores'),
                'DESCRIPTION'=>$question->elementsByName('description')[0]->textContent(),
            ];
            $this->propertyBool($question->elementsByName('active')[0]->textContent(),'ACTIVE',$field);
            $this->propertyPict($question->elementsByName('picture')[0]->textContent(),"PICTURE",$patchImport,$field);
            if($arrResult[$field["NAME"]])
            {
                $idQ=$arrResult[$field["NAME"]];
                $el->Update($idQ,$field);
            }else{
                $idQ=$el->Add($field);
            }
        }
    }

    /**
     * @param $DataXML
     * @param $patchImport
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function element($DataXML,$patchImport)
    {
        $el=new \AelitaTestTest();

        $groups=$this->getCroupCode();
        $Allnode = $DataXML->SelectNodes('/testsdata/tests');
        foreach ($Allnode->children() as $node)
        {

            $field=[
                'CODE'=>$node->elementsByName('code')[0]->textContent(),
                'SORT'=>$node->elementsByName('sort')[0]->textContent(),
                'NAME'=>$node->elementsByName('name')[0]->textContent(),
                'DESCRIPTION'=>$node->elementsByName('description')[0]->textContent(),
                'NUMBER_ATTEMPTS'=>(int)$node->elementsByName('numberattempts')[0]->textContent(),
                'PERIOD_ATTEMPTS'=>(int)$node->elementsByName('periodattempts')[0]->textContent(),
                'TEST_TIME'=>(int)$node->elementsByName('testtime')[0]->textContent(),
                'MULTIPLE_QUESTION_COUNT'=>(int)$node->elementsByName('multiplequestioncount')[0]->textContent(),
            ];

            $this->propertyDate($node->elementsByName('datefrom')[0]->textContent(),'DATE_FROM',$field);
            $this->propertyDate($node->elementsByName('dateto')[0]->textContent(),'DATE_TO',$field);

            $this->propertyPict($node->elementsByName('picture')[0]->textContent(),"PICTURE",$patchImport,$field);

            $this->propertyBool($node->elementsByName('active')[0]->textContent(),'ACTIVE',$field);
            $this->propertyBool($node->elementsByName('showanswers')[0]->textContent(),'SHOW_ANSWERS',$field);
            $this->propertyBool($node->elementsByName('mixquestion')[0]->textContent(),'MIX_QUESTION',$field);
            $this->propertyBool($node->elementsByName('showcomments')[0]->textContent(),'SHOW_COMMENTS',$field);
            $this->propertyBool($node->elementsByName('autostartover')[0]->textContent(),'AUTO_START_OVER',$field);

            $type=$node->elementsByName('type')[0]->textContent();
            if($type=="scoring")
                $field["USE_CORRECT"]="N";
            else
                $field["USE_CORRECT"]="Y";

            $multiplequestion=$node->elementsByName('multiplequestion')[0]->textContent();

            if(in_array($multiplequestion,["none", "anum", "gnum", "allq", "clst"]))
                $field["MULTIPLE_QUESTION"]=$multiplequestion;

            $group=$node->elementsByName('group')[0]->textContent();

            if($groups[$group])
                $field["GROUP_ID"]=$groups[$group];

            $countquestion=0;
            $countquestionNode=$node->elementsByName('countquestion');
            if(is_array($countquestionNode) && count($countquestionNode)==1)
                $countquestion=(int)$countquestionNode[0]->textContent();

            $dbResultList=$el->GetList([],[
                "CODE"=>$field["CODE"]
            ],false,["nTopCount"=>1],[
                "ID",
                "CODE",
                "NAME"
            ]);

            if ($arResult = $dbResultList->Fetch())
            {
                $elID=$arResult["ID"];
                $el->Update($elID,$field);
            }else{
                $elID=$el->Add($field);
            }

            if($elID>0)
            {


                $GroupQuestionID=$this->getGroupQuestion($elID,$countquestion);

                $typecommerce=$node->elementsByName('typecommerce')[0]->textContent();
                $testprice=(int)$node->elementsByName('testprice')[0]->textContent();
                $this->setCommerce($elID,$typecommerce,$testprice);
                $questions=$node->elementsByName('questions');
                if(is_array($questions) && count($questions)>0)
                    $this->addQuestions($elID,$questions[0],$patchImport,$GroupQuestionID);

                $results=$node->elementsByName('results');
                if(is_array($results) && count($results)>0)
                    $this->addResults($elID,$results[0],$patchImport);
            }
        }
    }

}