<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 15.11.2018
 * Time: 11:48
 */

namespace local\services\import;

use Bitrix\Main\FileTable;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;
use Bitrix\Main\Application;

class fileImport
{

    /**
     * @param $fileId
     * @param $patchImport
     * @return bool|mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getFileName($fileId,$patchImport){
        $zip = new \ZipArchive;
        if($fileId<=0)
            return false;
        $resFile=FileTable::getById($fileId);
        if($File=$resFile->fetch())
        {
            $Path=\CFile::GetPath($File["ID"]);
            Directory::createDirectory($patchImport);
            File::putFileContents($patchImport.$File["FILE_NAME"],File::getFileContents(Application::getDocumentRoot().$Path));
            if(is_resource(zip_open($patchImport.$File["FILE_NAME"]))){
                if ($zip->open($patchImport.$File["FILE_NAME"]) === TRUE) {
                    $zip->extractTo($patchImport);
                    $zip->close();
                    File::deleteFile($patchImport.$File["FILE_NAME"]);
                }
            }
            $files=glob($patchImport."*.xml");
            if(count($files)>0)
                return str_replace($patchImport,'',$files[0]);
        }
        return false;
    }

}