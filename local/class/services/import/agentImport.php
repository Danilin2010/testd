<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 16.11.2018
 * Time: 2:25
 */

namespace local\services\import;



class agentImport
{
    const AgentSynchronize='local\services\import\agentImport::Synchronize();';

    /**
     * @return string
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    function Synchronize()
    {
        $stepImport=new stepImport();
        $stepImport->step();
        return self::AgentSynchronize;
    }
}