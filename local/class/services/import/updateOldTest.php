<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 15.11.2018
 * Time: 15:24
 */

namespace local\services\import;

use Bitrix\Main\Loader;

class updateOldTest
{
    public function __construct()
    {
        Loader::includeModule('aelita.test');
    }

    /**
     * @param \CDataXML $DataXML
     */
    public function delOld($DataXML)
    {
        $route=$DataXML->SelectNodes('/testsdata');
        $ContainsOnlyChanges=$route->getAttribute('ContainsOnlyChanges');
        if($ContainsOnlyChanges!='true')
        {
            $this->delOldGroup($DataXML);
            $this->delOldElement($DataXML);
        }
    }

    /**
     * @param \CDataXML $DataXML
     */
    private function delOldGroup($DataXML)
    {
        $code=[];
        $Allnode = $DataXML->SelectNodes('/testsdata/groups');
        foreach ($Allnode->children() as $node)
            foreach ($node->elementsByName('code') as $codeXml)
                $code[]=$codeXml->textContent();
        $el=new \AelitaTestGroup();
        $dbResultList=$el->GetList([],[
            "ACTIVE"=>"Y"
        ],false,false,[
            "ID",
            "CODE",
            "NAME"
        ]);
        while ($arResult = $dbResultList->Fetch())
            if(!in_array($arResult["CODE"],$code))
                $el->Update($arResult["ID"],["ACTIVE"=>"N"]);
    }

    /**
     * @param \CDataXML $DataXML
     */
    private function delOldElement($DataXML)
    {
        $code=[];
        $Allnode = $DataXML->SelectNodes('/testsdata/tests');
        foreach ($Allnode->children() as $node)
            foreach ($node->elementsByName('code') as $codeXml)
                $code[]=$codeXml->textContent();
        $el=new \AelitaTestTest();
        $dbResultList=$el->GetList([],[
            "ACTIVE"=>"Y"
        ],false,false,[
            "ID",
            "CODE",
            "NAME"
        ]);
        while ($arResult = $dbResultList->Fetch())
            if(!in_array($arResult["CODE"],$code))
                $el->Update($arResult["ID"],["ACTIVE"=>"N"]);
    }
}