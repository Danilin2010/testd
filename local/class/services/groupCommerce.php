<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 20.08.2018
 * Time: 1:19
 */

namespace local\services;

use \Bitrix\Main\GroupTable;
use \Bitrix\Main\UserGroupTable;
use \Bitrix\Main\UserTable;

class groupCommerce
{

    public static function callbackOrder($Order)
    {
        $UserId=$Order["USER"];
        foreach ($Order["ITEMS"] as $item)
            self::addTestUser($UserId,subsidiaryCommerce::getTestId($item["ITEM"]));
    }

    public static function checkGroupTest($testId)
    {
        global $USER;
        $ID=$USER->GetID();
        if($ID>0)
            return self::checkGroupTestUserId($ID,$testId);
        return false;
    }

    public static function checkGroupTestUserId($userId,$testId)
    {
        $GroupId=self::getGroupId($testId);
        $getUserGroup=UserTable::getUserGroupIds($userId);
        if(in_array($GroupId,$getUserGroup))
            return true;
        return false;
    }

    public static function addTestUser($userId,$testId)
    {
        $GroupId=self::getGroupId($testId);
        UserGroupTable::add([
            "USER_ID" => $userId,
            "GROUP_ID" => $GroupId,
        ]);
    }

    private static function getGroupId($testId)
    {
        $group = new GroupTable;
        $rsGroups = $group->GetList(['filter'=>["STRING_ID"=>"test".$testId]]);
        if($Groups=$rsGroups->fetch())
        {
            return $Groups["ID"];
        }else{
            $arFields=[
                "ACTIVE"       => "Y",
                "C_SORT"       => 1000,
                "NAME"         => "Доступ к тесту: ".testService::getNameAndType($testId,false),
                "DESCRIPTION"  => "",
                "STRING_ID"      => "test".$testId,
            ];
            $NEW_GROUP_ID = $group->add($arFields);
            return $NEW_GROUP_ID;
        }
    }
}