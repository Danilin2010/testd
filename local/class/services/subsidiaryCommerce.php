<?php
/**
 * Created by PhpStorm.
 * @author Alexander Danilin <danilin2010@yandex.ru>
 * Date: 20.08.2018
 * Time: 2:15
 */

namespace local\services;

use \Bitrix\Main\Loader;

class subsidiaryCommerce
{

    const PriceTypeId=1;

    public static function updateElement($TestId,$Price)
    {
        $ElementId=self::getElement($TestId);
        if($ElementId)
            \CIBlockElement::SetPropertyValues($ElementId, IBLOCK_SUBSIDIARY, $Price, "STARTSHOP_PRICE_".self::PriceTypeId);
    }

    public static function getTestId($elId)
    {
        if(!Loader::includeModule('iblock'))
            return 0;
        $arFilter = [
            "IBLOCK_ID"=>IBLOCK_SUBSIDIARY,
            "ACTIVE"=>"Y",
            "ID"=>$elId,
        ];
        $res = \CIBlockElement::GetList([],$arFilter,false,["nTopCount"=>1],["IBLOCK_TYPE","IBLOCK_ID","ID","PROPERTY_testId"]);
        if($ar_fields = $res->GetNext())
            return $ar_fields["PROPERTY_TESTID_VALUE"];
        return 0;
    }

    /**
     * @param int $TestId
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getElement($TestId){
        if(!Loader::includeModule('iblock'))
            return 0;
        $arFilter = [
            "IBLOCK_ID"=>IBLOCK_SUBSIDIARY,
            "ACTIVE"=>"Y",
            "PROPERTY_testId"=>$TestId,
        ];
        $res = \CIBlockElement::GetList([],$arFilter,false,["nTopCount"=>1]);
        if($ar_fields = $res->GetNext())
        {
            return $ar_fields["ID"];
        }else{
            $el = new \CIBlockElement;
            $arLoadProductArray=[
                "IBLOCK_SECTION_ID"=>false,
                "IBLOCK_ID"=>IBLOCK_SUBSIDIARY,
                "PROPERTY_VALUES"=>[
                    "testId"=>$TestId,
                    "STARTSHOP_QUANTITY"=>1,
                    "STARTSHOP_QUANTITY_RATIO"=>1,
                    "STARTSHOP_CURRENCY_".self::PriceTypeId=>self::getPropertyCurrent("RUB"),
                ],
                "NAME"=>"Доступ к тесту: ".testService::getNameAndType($TestId,false),
                "ACTIVE"=>"Y",
            ];
            if($PRODUCT_ID = $el->Add($arLoadProductArray))
                return $PRODUCT_ID;
            else
                return 0;
        }
    }

    /**
     * @param string $XML
     * @return int
     * @throws \Bitrix\Main\LoaderException
     */
    private static function getPropertyCurrent($XML)
    {
        if(!Loader::includeModule('iblock'))
            return 0;
        $property_enums = \CIBlockPropertyEnum::GetList(
            [], [
                "IBLOCK_ID"=>IBLOCK_SUBSIDIARY,
                "CODE"=>"STARTSHOP_CURRENCY_1",
                "XML_ID"=>$XML,
            ]);
        if($enum_fields = $property_enums->GetNext())
            return $enum_fields["ID"];
        return 0;
    }
}