<?php
use WS\ReduceMigrations\Builder\HighLoadBlockBuilder;
use WS\ReduceMigrations\Builder\Entity\HighLoadBlock;
use WS\ReduceMigrations\Builder\Entity\UserField;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1534679994_sozdanie_highload_infobloka_tipov_testov extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Создание Highload инфоблока типов тестов";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "373b14ca6c759d3c82dec2254b32125120cce6ef";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    static public function IblockName() {
        return 'TypeOfTest';
    }

    static public function IblockTableName() {
        return 'type_of_test';
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        if (!Loader::includeModule('highloadblock'))
            return;
        $hlblock = HL\HighloadBlockTable::getList([
            'filter' => ['=NAME' => self::IblockName()]
        ]);
        if(!$hlres=$hlblock->fetch()){
            $builder = new HighLoadBlockBuilder();
            $builder->addHLBlock(self::IblockName(), self::IblockTableName(), function (HighLoadBlock $block){
                $block
                    ->addField('uf_testId')
                    ->label(['ru' => 'testId'])
                    ->required(true)
                    ->type(UserField::TYPE_INTEGER);
                $Field=$block
                    ->addField('uf_testType')
                    ->label(['ru' => 'Тип'])
                    ->required(true)
                    ->type(UserField::TYPE_ENUMERATION);
                $Field->addEnum("Бесплатный")->xmlId("free");
                $Field->addEnum("Платный")->xmlId("chargeable");
                $Field->addEnum("Бета-тест")->xmlId("beta_test");
                $block
                    ->addField('uf_testPrice')
                    ->label(['ru' => 'testPrice'])
                    ->required(true)
                    ->type(UserField::TYPE_DOUBLE);
            });
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        if (Loader::includeModule('highloadblock'))
        {
            $hlblock = HL\HighloadBlockTable::getList([
                'filter' => ['=NAME' => self::IblockName()]
            ])->fetch();
            if($hlblock){
                HL\HighloadBlockTable::delete($hlblock["ID"]);
            }
        }
    }
}