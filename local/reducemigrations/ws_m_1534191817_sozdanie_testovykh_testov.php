<?php

use \Bitrix\Main\Loader;

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1534191817_sozdanie_testovykh_testov extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Создание тестовых тестов";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "6547beadadc99e0f936909dcc72b0d0e026a2515";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        if (!Loader::includeModule('aelita.test'))
           return;
        $elTestGroup=new AelitaTestGroup;
        $elTest=new AelitaTestTest;
        $elTestQuestion=new AelitaTestQuestion;
        $elTestAnswer=new AelitaTestAnswer;
        $elTestResult=new AelitaTestResult;
        foreach (self::getTestList() as $TestList)
        {
            $arFields = array(
                "NAME"=>$TestList["NAME"],
                "CODE"=>$TestList["CODE"],
                "ACTIVE"=>"Y",
            );
            $TestGroupID = $elTestGroup->Add($arFields);
            if($TestGroupID>0)
            {
                foreach ($TestList["TEST"] as $Test)
                {
                    $arFields = array(
                        "NAME"=>$Test["NAME"],
                        "CODE"=>$Test["CODE"],
                        "ACTIVE"=>"Y",
                        "GROUP_ID"=>$TestGroupID,
                        "USE_CORRECT"=>$Test["USE_CORRECT"],
                    );
                    $IDTest = $elTest->Add($arFields);
                    if($IDTest>0)
                    {
                        foreach ($Test["QUESTION"] as $Question)
                        {
                            $arFields = array(
                                "NAME"=>$Question["NAME"],
                                "ACTIVE"=>"Y",
                                "TEST_ID"=>$IDTest,
                                "TEST_TYPE"=>$Question["TEST_TYPE"],
                            );
                            $IDTestQuestion = $elTestQuestion->Add($arFields);
                            if($IDTestQuestion>0)
                            {
                                foreach ($Question["ANSWER"] as $Answer)
                                {
                                    $arFields = array(
                                        "NAME"=>$Answer["NAME"],
                                        "ACTIVE"=>"Y",
                                        "SCORES"=>$Answer["SCORES"],
                                        "QUESTION_ID"=>$IDTestQuestion,
                                    );
                                    $elTestAnswer->Add($arFields);
                                }
                            }
                        }
                        foreach ($Test["RESULT"] as $Result)
                        {
                            $arFields = array(
                                "NAME"=>$Result["NAME"],
                                "ACTIVE"=>"Y",
                                "MAX_SCORES"=>$Result["MAX_SCORES"],
                                "MIN_SCORES"=>$Result["MIN_SCORES"],
                                "TEST_ID"=>$IDTest,
                            );
                            $elTestResult->Add($arFields);
                        }
                    }
                }
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        if (!Loader::includeModule('aelita.test'))
            return;
        $elTestGroup=new AelitaTestGroup;
        $elTest=new AelitaTestTest;
        $elTestQuestion=new AelitaTestQuestion;
        $elTestAnswer=new AelitaTestAnswer;
        $elTestResult=new AelitaTestResult;
        foreach (self::getTestList() as $TestList)
        {
            $resTestGroup = $elTestGroup->GetList(array(), array("CODE" => $TestList["CODE"]));
            while($TestGroup = $resTestGroup->GetNext())
            {
                $resTest = $elTest->GetList(array(), array("GROUP_ID" => $TestGroup["ID"]));
                while($Test = $resTest->GetNext())
                {
                    $resTestQuestion = $elTestQuestion->GetList(array(), array("TEST_ID" => $Test["ID"]));
                    while($TestQuestion = $resTestQuestion->GetNext())
                    {
                        $resTestAnswer = $elTestAnswer->GetList(array(), array("QUESTION_ID" => $TestQuestion["ID"]));
                        while($TestAnswer = $resTestAnswer->GetNext())
                            $elTestAnswer->Delete($TestAnswer["ID"]);
                        $elTestQuestion->Delete($TestQuestion["ID"]);
                    }
                    $resTestResult = $elTestResult->GetList(array(), array("TEST_ID" => $Test["ID"]));
                    while($TestResult = $resTestResult->GetNext())
                        $elTestResult->Delete($TestResult["ID"]);
                    $elTest->Delete($Test["ID"]);
                }
                $elTestGroup->Delete($TestGroup["ID"]);
            }
        }
    }

    private function getTestList()
    {
        return [
            [
                "NAME"=>"Тестирование",
                "CODE"=>"testing",
                "TEST"=>[
                    [
                        "NAME"=>"Города",
                        "CODE"=>"cities",
                        "USE_CORRECT"=>"N",
                        "QUESTION"=>[
                            [
                                "NAME"=>"Столица России",
                                "TEST_TYPE"=>"radio",
                                "ANSWER"=>[
                                    [
                                        "NAME"=>"Москва",
                                        "SCORES"=>1,
                                    ],
                                    [
                                        "NAME"=>"Ростов",
                                        "SCORES"=>0,
                                    ],
                                    [
                                        "NAME"=>"Саратов",
                                        "SCORES"=>0,
                                    ],
                                ],
                            ],
                        ],
                        "RESULT"=>[
                            [
                                "NAME"=>"Неверно",
                                "MIN_SCORES"=>0,
                                "MAX_SCORES"=>0,
                            ],
                            [
                                "NAME"=>"Верно",
                                "MIN_SCORES"=>1,
                                "MAX_SCORES"=>1,
                            ],
                        ],
                    ],
                    [
                        "NAME"=>"Реки",
                        "CODE"=>"rivers",
                        "USE_CORRECT"=>"N",
                        "QUESTION"=>[
                            [
                                "NAME"=>"Река России",
                                "TEST_TYPE"=>"radio",
                                "ANSWER"=>[
                                    [
                                        "NAME"=>"Дон",
                                        "SCORES"=>1,
                                    ],
                                    [
                                        "NAME"=>"Нил",
                                        "SCORES"=>0,
                                    ],
                                    [
                                        "NAME"=>"Амазонка",
                                        "SCORES"=>0,
                                    ],
                                ],
                            ],
                        ],
                        "RESULT"=>[
                            [
                                "NAME"=>"Неверно",
                                "MIN_SCORES"=>0,
                                "MAX_SCORES"=>0,
                            ],
                            [
                                "NAME"=>"Верно",
                                "MIN_SCORES"=>1,
                                "MAX_SCORES"=>1,
                            ],
                        ],
                    ],
                    [
                        "NAME"=>"Озера",
                        "CODE"=>"lakes",
                        "USE_CORRECT"=>"N",
                        "QUESTION"=>[
                            [
                                "NAME"=>"Озера России",
                                "TEST_TYPE"=>"radio",
                                "ANSWER"=>[
                                    [
                                        "NAME"=>"Байкал",
                                        "SCORES"=>1,
                                    ],
                                    [
                                        "NAME"=>"Лох-Несс",
                                        "SCORES"=>0,
                                    ],
                                    [
                                        "NAME"=>"Виктория",
                                        "SCORES"=>0,
                                    ],
                                ],
                            ],
                        ],
                        "RESULT"=>[
                            [
                                "NAME"=>"Неверно",
                                "MIN_SCORES"=>0,
                                "MAX_SCORES"=>0,
                            ],
                            [
                                "NAME"=>"Верно",
                                "MIN_SCORES"=>1,
                                "MAX_SCORES"=>1,
                            ],
                        ],
                    ],
                ]
            ],
        ];
    }

}