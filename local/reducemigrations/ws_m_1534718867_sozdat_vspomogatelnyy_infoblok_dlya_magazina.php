<?php
use WS\ReduceMigrations\Builder\Entity\Iblock;
use WS\ReduceMigrations\Builder\IblockBuilder;
use WS\ReduceMigrations\Builder\Entity\IblockType;
use \Bitrix\Main\Loader;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1534718867_sozdat_vspomogatelnyy_infoblok_dlya_magazina extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Создать вспомогательный инфоблок для магазина";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "5213bf2401cbc505bf1dff82a9c13f2debc35c7b";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 0;
    }

    static public function newSite() {
        return 's1';
    }

    static public function newCodeType() {
        return 'subsidiary';
    }

    static public function addCode() {
        return 'subsidiary';
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        $builder = new IblockBuilder();
        if (Loader::includeModule('iblock')) {


            $res = \CIBlockType::GetList(
                Array(),
                Array(
                    "ID" => self::newCodeType()
                )
            );
            if (!$ar_res = $res->Fetch()) {
                $builder->createIblockType(self::newCodeType(), function (IblockType $type) {
                    $type
                        ->inRss(false)
                        ->sort(500)
                        ->sections('N')
                        ->lang(
                            [
                                'ru' => [
                                    'NAME' => 'Вспомогательный',
                                ],
                                'en' => [
                                    'NAME' => 'Subsidiary',
                                ],
                            ]
                        );
                });
            }

            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::addCode()
                )
            );
            if(!$ar_res = $res->Fetch())
            {
                $iblock = $builder->createIblock(self::newCodeType(), 'Вспомогательный', function (Iblock $iblock) {
                    $iblock
                        ->siteId(self::newSite())
                        ->code(self::addCode())
                        ->version(2)
                        ->groupId(['2' => 'R']);
                    $iblock->setAttribute('INDEX_ELEMENT', 'N');
                    $iblock->setAttribute('LIST_PAGE_URL', '#SITE_DIR#/catalog/');
                    $iblock->setAttribute('SECTION_PAGE_URL', '#SITE_DIR#/test/0/');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '#SITE_DIR#/test/0/#ELEMENT_CODE#/');

                    $iblock
                        ->addProperty('Id теста')
                        ->code('testId')
                        ->required()
                        ->typeNumber()
                        ->sort(100);
                });
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        if (Loader::includeModule('iblock'))
        {
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::addCode()
                )
            );
            if($ar_res = $res->Fetch())
                \CIBlock::Delete($ar_res["ID"]);
            \CIBlockType::Delete(self::newCodeType());
        }
    }
}