<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск по ID");
?>
	<div class="intec-content">
	<div class="intec-content-wrapper">
<?$APPLICATION->IncludeComponent(
	"local:test.profile",
	".default", 
	array(
		"ADD_QUESTIONING_CHAIN" => "Y",
		"ADD_TEST_CHAIN" => "Y",
		"COUNT_TEST" => "20",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "",
		"REPEATED_URL" => "/test/#GROUP_CODE#/#TEST_CODE#/",
		"SEF_FOLDER" => "/test-search/",
		"SEF_MODE" => "Y",
		"SET_TITLE_QUESTIONING" => "Y",
		"SET_TITLE_TEST" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"SEF_URL_TEMPLATES" => array(
			"tests" => "#USER_ID#/",
			"test" => "#USER_ID#/#TEST_CODE#/",
			"questioning" => "#USER_ID#/#TEST_CODE#/#QUESTIONING_CODE#/",
		)
	),
	false
);?>
	</div></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>