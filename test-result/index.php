<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("test_result");
?><?$APPLICATION->IncludeComponent(
	"aelita:test.profile.questioning", 
	"quiz", 
	array(
		"TEST_GROUP" => "",
		"TEST_ID" => $_REQUEST["test_id"],
		"QUESTIONING_ID" => $_REQUEST["id"],
		"ADD_TEST_CHAIN" => "N",
		"SET_TITLE_TEST" => "N",
		"ADD_QUESTIONING_CHAIN" => "N",
		"SET_TITLE_QUESTIONING" => "N",
		"TEST_URL" => "",
		"DETAIL_URL" => "/test-result/?id=#QUESTIONING_CODE#&test_id=#TEST_CODE#",
		"REPEATED_URL" => "",
		"NON_CHECK_USER" => "Y",
		"SHOW_LINK" => "Y"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>