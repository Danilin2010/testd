<?php

use intec\core\helpers\ArrayHelper;
use intec\core\helpers\FileHelper;
use intec\core\helpers\Html;
use intec\constructor\models\Build;
use intec\core\io\Path;

/**
 * @global CMain $APPLICATION
 */

$properties = Build::getCurrent()->getPage()->getProperties();
$blocks = $properties->get('main_blocks');
$banner = ArrayHelper::getValue($blocks, ['templates', 'main_banner']);

/**
 * @param string $code
 * @param string $template
 */
$widgetInclude = function ($code) use (&$blocks, &$banner) {
    global $APPLICATION;

    $path = Path::from(__DIR__);
    $path = $path
        ->add('wide')
        ->add($code.'.php');

    /**
     * @param string $template
     */
    $templateInclude = function ($template) use (&$code, &$blocks, &$banner) {
        global $APPLICATION;

        $path = Path::from(__DIR__);
        $path = $path
            ->add('wide')
            ->add($code)
            ->add($template.'.php');

        if (FileHelper::isFile($path->value))
            include($path->value);
    };

    if (FileHelper::isFile($path->value))
        include($path->value);
};

$widgetInclude('banner');
$widgetInclude('icons');
$widgetInclude('categories');
$widgetInclude('rubric');
$widgetInclude('photogallery');
$widgetInclude('products');
$widgetInclude('services');
$widgetInclude('stages');
$widgetInclude('advantages');

?>
<?= Html::beginTag('div') ?>
    <?php $APPLICATION->IncludeComponent("intec.universe:main.form", "template.1", array(
	"ID" => "6",
		"NAME" => "Зарегистрируйтесь сейчас",
		"CONSENT" => "/company/consent/",
		"TEMPLATE" => ".default",
		"VIEW" => "left",
		"BG_COLOR" => "",
		"BG_IMAGE_SHOW" => "Y",
		"BG_IMAGE" => "#TEMPLATE_PATH#/images/bg.png",
		"BG_IMAGE_POSITION_V" => "center",
		"BG_IMAGE_POSITION_H" => "left",
		"BG_IMAGE_SIZE" => "cover",
		"BG_IMAGE_REPEAT" => "no-repeat",
		"TITLE" => "Сертификаты TESTD используются 15 000 ведущих работодателей по всей России! Благодаря партнерству с TESTD, специалисты по всей стране продвинулись в своей карьере, найдя лучшие возможности для карьерного роста, более интересную работу и возможности зарабатывать больше. Вы можете сделать то же самое.",
		"DESCRIPTION_SHOW" => "Y",
		"DESCRIPTION" => "Наши консультанта ответят на все ваши вопросы, дадут оценку и рекомендации вашему проекту",
		"TEXT_THEME" => "dark",
		"BUTTON_TEXT" => "Заказать",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"COMPONENT_TEMPLATE" => "template.1",
		"BG_IMAGE_PARALLAX_USE" => "N"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
); ?>
<?= Html::endTag('div') ?>
<?php

$widgetInclude('video');
$widgetInclude('rates');
$widgetInclude('staff');
$widgetInclude('certificates');

?>
<?= Html::beginTag('div') ?>
    <?php $APPLICATION->IncludeComponent("intec.universe:main.widget", "form.1", array(
	"WEB_FORM_ID" => "2",
		"WEB_FORM_TITLE_SHOW" => "Y",
		"WEB_FORM_DESCRIPTION_SHOW" => "Y",
		"WEB_FORM_BACKGROUND" => "theme",
		"WEB_FORM_BACKGROUND_OPACITY" => "",
		"WEB_FORM_TEXT_COLOR" => "light",
		"WEB_FORM_POSITION" => "right",
		"WEB_FORM_ADDITIONAL_PICTURE_SHOW" => "Y",
		"WEB_FORM_ADDITIONAL_PICTURE" => "#TEMPLATE_PATH#/images/picture.png",
		"WEB_FORM_ADDITIONAL_PICTURE_HORIZONTAL" => "center",
		"WEB_FORM_ADDITIONAL_PICTURE_VERTICAL" => "center",
		"WEB_FORM_ADDITIONAL_PICTURE_SIZE" => "contain",
		"BLOCK_BACKGROUND" => "#TEMPLATE_PATH#/images/bg.jpg",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
<?= Html::endTag('div') ?>
<?php

$widgetInclude('faq');
$widgetInclude('videos');
$widgetInclude('shares');
$widgetInclude('news');
$widgetInclude('articles');
$widgetInclude('reviews');
$widgetInclude('about');
$widgetInclude('brands');
$widgetInclude('contact');

?>
<?= Html::tag('div', null, ['style' => [
    'margin-bottom' => '-30px'
]]) ?>
