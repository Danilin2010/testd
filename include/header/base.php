<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

use intec\core\helpers\ArrayHelper;
use intec\constructor\models\Build;

/**
 * @global CMain $APPLICATION
 */

$properties = Build::getCurrent()->getPage()->getProperties();
$blocks = $properties->get('main_blocks');

if ($APPLICATION->GetCurPage(false) !== SITE_DIR || ArrayHelper::getValue($blocks, ['active', 'main_banner'], 'Y') !== 'Y')
    $blocks['templates']['main_banner'] = 1;

$blocks = $properties->set('main_blocks', $blocks);

?>
<?php $APPLICATION->IncludeComponent(
    "intec.universe:widget",
    "buttontop",
    array(
        "RADIUS" => "10",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "0"
    ),
    false
); ?>
<?php $APPLICATION->IncludeComponent(
    "intec.universe:main.header",
    "template.1",
    array(
        "SETTINGS_USE" => "Y",
        "SEARCH_NUM_CATEGORIES" => "1",
        "SEARCH_TOP_COUNT" => "5",
        "SEARCH_ORDER" => "date",
        "SEARCH_USE_LANGUAGE_GUESS" => "Y",
        "SEARCH_CHECK_DATES" => "N",
        "SEARCH_SHOW_OTHERS" => "N",
        "SEARCH_CATEGORY_0_TITLE" => "",
        "SEARCH_CATEGORY_0" => array(
            0 => "no",
        ),
        "SEARCH_INPUT_ID" => "",
        "SEARCH_TIPS_USE" => "Y",
        "SEARCH_PRICE_CODE" => array(
            0 => "BASE",
        ),
        "SEARCH_PRICE_VAT_INCLUDE" => "Y",
        "SEARCH_CURRENCY_CONVERT" => "Y",
        "SEARCH_CURRENCY_ID" => "RUB",
        "COMPARE_IBLOCK_TYPE" => "catalogs",
        "COMPARE_IBLOCK_ID" => "16",
        "COMPARE_CODE" => "compare",
        "MENU_MAIN_ROOT" => "top",
        "MENU_MAIN_CHILD" => "left",
        "MENU_MAIN_LEVEL" => "4",
        "MENU_MAIN_IBLOCK_TYPE" => "catalogs",
        "MENU_MAIN_IBLOCK_ID" => "16",
        "MENU_INFO_ROOT" => "footer",
        "MENU_INFO_CHILD" => "left",
        "MENU_INFO_LEVEL" => "4",
        "BANNER_IBLOCK_TYPE" => "content",
        "BANNER_IBLOCK_ID" => "4",
        "BANNER_ELEMENTS_COUNT" => "",
        "BANNER_SORT_BY" => "SORT",
        "BANNER_ORDER_BY" => "ASC",
        "LOGOTYPE" => "/include/logotype.php",
        "PHONES" => array(
            0 => "+7 916 375 00 71"
        ),
        "ADDRESS" => "г. Москва",
        "EMAIL" => "admin.elena@testd.ru",
        "TAGLINE" => "НЕЗАВИСИМЫЙ ЦЕНТР ОЦЕНКИ КВАЛИФИКАЦИЙ",
        "MENU_MAIN_PROPERTY_IMAGE" => "UF_IMAGE_MENU",
        "FORMS_CALL_ID" => "1",
        "FORMS_CALL_TEMPLATE" => ".default",
        "SOCIAL_VK" => "https://vk.com",
        "SOCIAL_INSTAGRAM" => "https://instagram.com",
        "SOCIAL_FACEBOOK" => "https://facebook.com",
        "SOCIAL_TWITTER" => "https://twitter.com",
        "BANNER_PROPERTY_HEADER" => "HEADER",
        "BANNER_PROPERTY_DESCRIPTION" => "DESCRIPTION",
        "BANNER_PROPERTY_HEADER_COLOR" => "TITLE_TEXT_COLOR",
        "BANNER_PROPERTY_DESCRIPTION_COLOR" => "DESCRIPTION_TEXT_COLOR",
        "BANNER_PROPERTY_TEXT_POSITION" => "POSITION",
        "BANNER_PROPERTY_LINK" => "LINK",
        "BANNER_PROPERTY_LINK_BLANK" => "NEW_TAB",
        "BANNER_PROPERTY_BUTTON_SHOW" => "BUTTON_SHOW",
        "BANNER_PROPERTY_BUTTON_TEXT" => "BUTTON_TEXT",
        "BANNER_PROPERTY_BUTTON_TEXT_COLOR" => "BUTTON_TEXT_COLOR",
        "BANNER_PROPERTY_BUTTON_COLOR" => "BUTTON_COLOR",
        "BANNER_PROPERTY_IMAGE" => "BANNER_IMG",
        "BANNER_PROPERTY_IMAGE_VERTICAL_POSITION" => "BANNER_IMG_POSITION",
        "BANNER_PROPERTY_BANNER_COLOR" => "BANNER_COLOR",
        "BANNER_PROPERTY_INSERTIONS" => "INSERTIONS",
        "BANNER_PROPERTY_VIDEO_URL" => "VIDEO_LINK",
        "LOGOTYPE_SHOW" => "Y",
        "PHONES_SHOW" => "Y",
        "TRANSPARENCY" => "Y",
        "LOGOTYPE_SHOW_FIXED" => "Y",
        "LOGOTYPE_SHOW_MOBILE" => "Y",
        "ADDRESS_SHOW" => "Y",
        "EMAIL_SHOW" => "Y",
        "AUTHORIZATION_SHOW" => "Y",
        "AUTHORIZATION_SHOW_FIXED" => "Y",
        "AUTHORIZATION_SHOW_MOBILE" => "Y",
        "TAGLINE_SHOW" => "Y",
        "SEARCH_SHOW" => "Y",
        "SEARCH_SHOW_FIXED" => "Y",
        "SEARCH_SHOW_MOBILE" => "Y",
        "BASKET_SHOW" => "Y",
        "BASKET_SHOW_FIXED" => "Y",
        "BASKET_SHOW_MOBILE" => "Y",
        "DELAY_SHOW" => "Y",
        "DELAY_SHOW_FIXED" => "Y",
        "DELAY_SHOW_MOBILE" => "Y",
        "COMPARE_SHOW" => "Y",
        "COMPARE_SHOW_FIXED" => "Y",
        "COMPARE_SHOW_MOBILE" => "Y",
        "MENU_MAIN_SHOW" => "Y",
        "MENU_MAIN_SHOW_FIXED" => "Y",
        "MENU_MAIN_SHOW_MOBILE" => "Y",
        "MENU_MAIN_DELIMITERS" => "Y",
        "MENU_MAIN_SECTION_VIEW" => "images",
        "MENU_MAIN_SECTION_ITEMS_COUNT" => "3",
        "MENU_MAIN_CATALOG_LINKS" => array(
            0 => "/catalog/"
        ),
        "FORMS_CALL_SHOW" => "Y",
        "FORMS_CALL_TITLE" => "Заказать звонок",
        "DESKTOP" => "template.1",
        "PHONES_POSITION" => "bottom",
        "MENU_MAIN_POSITION" => "bottom",
        "MENU_MAIN_TRANSPARENT" => "N",
        "MENU_INFO_SHOW" => "Y",
        "SOCIAL_SHOW" => "Y",
        "SOCIAL_POSITION" => "left",
        "MOBILE" => "template.1",
        "MOBILE_FIXED" => "N",
        "MOBILE_FILLED" => "N",
        "FIXED" => "template.1",
        "BANNER" => "template.3",
        "BANNER_HEADER_SHOW" => "Y",
        "BANNER_DESCRIPTION_SHOW" => "Y",
        "BANNER_HEIGHT" => "500",
        "BANNER_INSERTIONS_SHOW" => "Y",
        "BANNER_INSERTIONS_COUNT" => "3",
        "BANNER_VIDEO_SHADOW_USE" => "N",
        "BANNER_SLIDER_DOTS" => "Y",
        "BANNER_SLIDER_LOOP" => "N",
        "BANNER_SLIDER_SPEED" => "500",
        "BANNER_SLIDER_AUTO_USE" => "N",
        "LOGIN_URL" => "/personal/profile/",
        "PROFILE_URL" => "/personal/profile/",
        "PASSWORD_URL" => "/personal/profile/?forgot_pass=yes",
        "REGISTER_URL" => "/personal/profile/?register=yes",
        "SEARCH_URL" => "/search/",
        "BASKET_URL" => "/personal/basket/",
        "COMPARE_URL" => "/catalog/compare.php",
        "CONSENT_URL" => "/company/consent/",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO"
    ),
    false
) ?>