<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
/**
 * @global CMain $APPLICATION
 */
?>
<?php $APPLICATION->IncludeComponent(
	"intec.universe:widget", 
	"basket.fly", 
	array(
		"OPEN_AFTER_ADD" => "settings",
		"COMPARE_CODE" => "compare",
		"COMPARE_IBLOCK_TYPE" => "catalogs",
		"COMPARE_IBLOCK_ID" => "16",
		"WEB_FORM_ID" => "1",
		"SHOW_BASKET" => "Y",
		"SHOW_DELAYED" => "Y",
		"SHOW_FORM" => "Y",
		"SHOW_AUTH" => "Y",
		"SHOW_COMPARE" => "N",
		"URL_CATALOG" => "/test/",
		"URL_BASKET" => "/personal/basket/",
		"URL_ORDER" => "/personal/basket/?page=order",
		"URL_COMPARE" => "/catalog/compare.php",
		"URL_CABINET" => "/personal/profile/",
		"URL_CONSENT" => "/company/consent/",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "",
		"CACHE_NOTES" => "",
		"COMPONENT_TEMPLATE" => "basket.fly",
		"CURRENCY" => "RUB"
	),
	false
); ?>