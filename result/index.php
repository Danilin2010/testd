<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

header('Content-Type:application/xml;charset=utf-8');

use Bitrix\Main\Application;
use \bupy7\xml\constructor\XmlConstructor;

$module_id="aelita.test";
if(!CModule::IncludeModule($module_id))die();

$request = Application::getInstance()->getContext()->getRequest();
$from = $request->get("from");
$to = $request->get("to");

if(strlen($from)<=0)
    $from=date('01.m.Y')." 00:00:00";
else
    $from=$from." 00:00:00";

if(strlen($to)<=0)
    $to=date('t.m.Y')." 23:59:59";
else
    $to=$to." 23:59:59";

$arFilter = array(
    "LOGIC"=>"OR",
    array(
        "LOGIC"=>"AND",
        ">=DATE_START" => trim(\CDatabase::CharToDateFunction($from),"\'"),
        "<=DATE_START" => trim(\CDatabase::CharToDateFunction($to),"\'"),
    ),
    array(
        "LOGIC"=>"AND",
        ">=DATE_STOP" => trim(\CDatabase::CharToDateFunction($from),"\'"),
        "<=DATE_STOP" => trim(\CDatabase::CharToDateFunction($to),"\'"),
    ),
);

//echo "<pre>";print_r($arFilter);echo "</pre>";

$el=new AelitaTestQuestioning();
$elGlass=new AelitaTestGlasses();

$arSelect=array(
    "ID",
    "PROFILE_ID",
    "TEST_ID",
    "TEST_CODE",
    "RESULT_ID",
    "CLOSED",
    "TEST_NAME",
    "RESULT_NAME",
    "SCORES",
    "USER_ID",
    "DATE_START",
    "DATE_STOP",
    "FINAL",
    "DURATION",
);
$arGroup=array(
    "ID",
);
$dbResultList = $el->GetList(array(),$arFilter,$arGroup,false,$arSelect);

$result=[
    'tag' => 'results',
    'elements' => []
];

function addTag(&$newArr,$tag,$content){
    $new=["tag"=>$tag];
    if(strlen($content)>0)
        $new["content"]=$content;
    else
        $new["content"]=false;
    $newArr[]=$new;
}

while ($arExtra = $dbResultList->GetNext())
{
    $newArr=[];

    addTag($newArr,"id",$arExtra["ID"]);
    addTag($newArr,"profile_id",$arExtra["PROFILE_ID"]);
    addTag($newArr,"test_id",$arExtra["TEST_ID"]);
    addTag($newArr,"test_code",$arExtra["TEST_CODE"]);
    addTag($newArr,"result_id",$arExtra["RESULT_ID"]);
    addTag($newArr,"test_name",$arExtra["TEST_NAME"]);
    addTag($newArr,"result_name",$arExtra["RESULT_NAME"]);
    addTag($newArr,"scores",$arExtra["SCORES"]);
    addTag($newArr,"user_id",$arExtra["USER_ID"]);
    addTag($newArr,"date_start",$arExtra["DATE_START"]);
    addTag($newArr,"date_stop",$arExtra["DATE_STOP"]);
    addTag($newArr,"final",$arExtra["FINAL"]);
    addTag($newArr,"id",$arExtra["ID"]);

    if($arExtra["DURATION"]>0)
        addTag($newArr,"duration",AelitaTestTools::GetTxtTime($arExtra["DURATION"]));

    if($arExtra["USER_ID"]>0) {
        $rsUser = CUser::GetByID($arExtra["USER_ID"]);
        $arUser = $rsUser->Fetch();
        $arrU=array();
        if($arUser["LAST_NAME"])
            addTag($newArr,"user_last_name",$arUser["LAST_NAME"]);
        if($arUser["NAME"])
            addTag($newArr,"user_name",$arUser["NAME"]);
        if($arUser["SECOND_NAME"])
            addTag($newArr,"user_second_name",$arUser["SECOND_NAME"]);
        if($arUser["LOGIN"])
            addTag($newArr,"user_login",$arUser["LOGIN"]);
    }

    $answers=[];

    if ($arExtra["ID"] > 0)
    {
        $arFilterGlass=array(
            "QUESTIONING_ID"=>$arExtra["ID"],
        );
        $arSelectGlass=array(
            "ID",
            "QUESTION_ID",
            "QUESTION_NAME",
            "SCORES",
            "SERIALIZED_RESULT",
            "SERIALIZED_RESULT_TEXT",
            "COMMENTS",
        );
        $arGroupGlass=array(
            "ID",
        );
        $dbResultListGlass = $elGlass->GetList(array(),$arFilterGlass,$arGroupGlass,false,$arSelectGlass);
        while ($arElementGlass = $dbResultListGlass->GetNext())
        {
            $newAnswer=[];
            addTag($newAnswer,"id",$arElementGlass["ID"]);
            addTag($newAnswer,"question_id",$arElementGlass["QUESTION_ID"]);
            addTag($newAnswer,"question_name",$arElementGlass["QUESTION_NAME"]);
            addTag($newAnswer,"scores",$arElementGlass["SCORES"]);
            addTag($newAnswer,"comments",$arElementGlass["COMMENTS"]);

            if($arElementGlass["SERIALIZED_RESULT"])
            {
                $arElementGlass["SERIALIZED_RESULT"]=unserialize(base64_decode($arElementGlass["SERIALIZED_RESULT"]));
                $arElementGlass["SERIALIZED_RESULT"]=implode("<br />",$arElementGlass["SERIALIZED_RESULT"]);
                addTag($newAnswer,"serialized_result",$arElementGlass["SERIALIZED_RESULT"]);
            }
            if($arElementGlass["SERIALIZED_RESULT_TEXT"])
            {
                $arElementGlass["SERIALIZED_RESULT_TEXT"]=unserialize(base64_decode($arElementGlass["SERIALIZED_RESULT_TEXT"]));
                $arElementGlass["SERIALIZED_RESULT_TEXT"]=implode("<br />",$arElementGlass["SERIALIZED_RESULT_TEXT"]);
                addTag($newAnswer,"serialized_result_text",$arElementGlass["SERIALIZED_RESULT_TEXT"]);
            }

            $answers[]=[
                'tag' => 'answer',
                'elements' => $newAnswer,
            ];
        }
    }

    $newArr[]=[
        'tag' => 'answers',
        'elements' => $answers,
    ];



    $result["elements"][]=
        [
            'tag' => 'result',
            'elements' => $newArr
        ]
    ;
}

$xml = new XmlConstructor([
    //'indentString' => '****',
    //'startDocument' => false,
]);
echo $xml->fromArray([$result])->toOutput();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
