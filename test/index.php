<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тестирование");
?><?$APPLICATION->IncludeComponent(
	"aelita:test", 
	"quiz", 
	array(
		"ADD_GROUP_CHAIN" => "Y",
		"ADD_TEST_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "quiz",
		"COUNT_TEST" => "20",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "",
		"PROFILE_DETAIL_URL" => "",
		"QUESTIONING_URL" => "/test-result/?id=#QUESTIONING_CODE#&test_id=#TEST_CODE#",
		"SEF_FOLDER" => "/test/",
		"SEF_MODE" => "Y",
		"SET_TITLE_GROUP" => "Y",
		"SET_TITLE_TEST" => "Y",
		"SHOW_ALL" => "N",
		"SHOW_NO_GROUP" => "N",
		"TOP_TESTS" => "N",
		"SEF_URL_TEMPLATES" => array(
			"groups" => "",
			"group" => "#GROUP_CODE#/",
			"test" => "#GROUP_CODE#/#TEST_CODE#/",
		)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>